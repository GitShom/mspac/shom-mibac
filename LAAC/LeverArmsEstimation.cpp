/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run LAAC (Lever Arms Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

/*/-------------------------------------------------------------------------------------/*/
// LAAC (Lever Arms Automatic Calibration)
//
// Class                  :  .cpp (LeverArmsEstimation)
// Authors                :  Rabine Keyetieu, Ga�l Rou�                
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/08/12
//--------------------------------------------------------------------------------------/*/


// --------------------------------------------------------------------------->>>>>>>>>>>>>>>> LIBS:
// Class
#include <direct.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <math.h> 
#include <fstream>
#include <sstream>
#include <Eigen/Dense>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/inverse_gaussian.hpp>
#include <boost/math/distributions/students_t.hpp>
#include "LeverArmsEstimation.h"


// Using name space
using namespace std;

// using particular methods
using boost::math::chi_squared;
using boost::math::quantile;
using boost::math::complement;
using boost::math::normal;
using namespace boost::math;
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>


// --------------------------------------------------------------------------->>>>>> Methods definition
// --------------------------------------------------------------------------->>>>>>
// Conctructor
LeverArmsEstimation::LeverArmsEstimation(void)
{
	m_LAAC_Estimation = "Lever arms angles estimation";
} // end constructor
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Raw data reading
MatrixXd LeverArmsEstimation::readTextFileRawdata(string & dirString3rd, string & folderData)
{
	/* This function is used to read raw data coming
	from data selection (Quadtree process)

	Input data: 
	- folderData,  Folder data

	Output data:
	- RawData_Out, [TpsPerBeams, ping, Index_Beam, SSS_long, Roll,
	Pitch, Yaw, Pn_t1 , Pn_t2 , Pn_t3 , Roll_recep , Pitch_recep , Yaw_recep,
	Alpha , Beta , TravelTime , A , B , C,
	Cax , Cay , Cs , FinalCri , indFile , indPatch]
	*/

	// Name project
	string NameProject;
	cout << "Please type the project name (Please each time AVOID SPACE IN NAMES): " << endl;
	cin >> NameProject; cout << endl;

	// folder data
	cout << "Please enter the data folder link: " << endl;
	cin >> folderData; cout << endl;

	// Provide current directory
	string curDir =  current_working_directory();

	// folder project and folder data
	string dirString = curDir + "\\" + NameProject;
	string folderDataIn = dirString + "\\" + "Export2ndPart_" + NameProject;

	// Create Third part output folder
	dirString3rd = dirString + "\\" + "LAAC_Result_" + NameProject;
	const char* dirChar3rd = (char*)dirString3rd.c_str(); 	// Name of created directory in char
	mkdir(dirChar3rd); 	                                    // Create directory in the current one

	// Initialization
	MatrixXd RawData_Out(1000,25);

	// Open file
	ifstream file;
	string line;
	file.open(folderDataIn + "/Raw_Data_LeverArms.txt");   
	int i = 0;

	if (file)
	{

		//// Reading
		//read stream line by line
		for(string line; getline(file, line); )   
		{

			//now read the whitespace-separated floats
			double TpsPerBeams, SSS_long, Roll, Pitch, Yaw, Pn_t1, Pn_t2, Pn_t3;
			double Roll_recep, Pitch_recep, Yaw_recep, Alpha, Beta, TravelTime;
			double A, B, C, Cphi, Cthe, Cpsi, FinalCri;
			double ping, Index_Beam, indFile, indPatch;

			//make a stream for the line itself
			istringstream in(line);				  

			// Extraction 
			in >> TpsPerBeams >> ping >> Index_Beam >> SSS_long >> Roll >> Pitch >> Yaw
				>> Pn_t1 >> Pn_t2 >> Pn_t3 >> Roll_recep >> Pitch_recep >> Yaw_recep
				>> Alpha >> Beta >> TravelTime >> A >> B >> C
				>> Cphi >> Cthe >> Cpsi >> FinalCri >> indFile >> indPatch;	

			// fill data
			RawData_Out.row(i) << TpsPerBeams, ping, Index_Beam, SSS_long, Roll,
				Pitch, Yaw, Pn_t1 , Pn_t2 , Pn_t3 , Roll_recep , Pitch_recep , Yaw_recep,
				Alpha , Beta , TravelTime , A , B , C,
				Cphi , Cthe , Cpsi , FinalCri , indFile , indPatch;
			i++;

			// increase the data size
			if (i%1000 == 0)
			{
				RawData_Out.conservativeResize(i + 1000,25);
			}

		} // end for

	}
	else
	{
		cout << "File reading error !" << endl;
	}

	// keep just non empty elements
	RawData_Out.conservativeResize(i,25);

	// Output
	return RawData_Out;

} // end function readTextFileRawdata
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Saving index for each patch
void LeverArmsEstimation::indexPerPatch(vector<vector<int>> & indexPerPatch_Out, int indPatch, int i, int & indPrevious,
	vector<double> & A_all, vector<double> & B_all, vector<double> & C_all,
	double A, double B, double C)
{
	/* This function is used to extract index per patch

	Input:
	- indPatch, current patch index
	- i, file line index
	- indPrevious, previous patch index

	Input data/Output data:
	- indexPerPatch_Out, vector of index per patch
	*/

	// if new patch add a vector of index
	if (i==0 || indPrevious + 1 == indPatch)
	{
		A_all.push_back(A);
		B_all.push_back(B);
		C_all.push_back(C);
		indexPerPatch_Out.push_back(vector<int>(1,i));
		indPrevious = indPatch;
	}

	// if patch already exist, add index to associated patch
	else if (indPrevious == indPatch)
	{
		indexPerPatch_Out[indexPerPatch_Out.size()-1].push_back(i);
	}

} // end function indexPerPatch
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// deltaX Least Square adjustment
VectorXd LeverArmsEstimation::LS_Adjustement(MatrixXd const& AbW_LS)
{
	/* This function is used to compute Least square solution as follows:
	deltaX = inv(A^t.W.A)*A^t.W.b

	Input data:
	- AbW_LS, ALS, bLS and WLS matrix

	Output data:
	- LS_Adjustement_Out, Least square solution
	*/

	// Data extraction 
	int N_unknows = (AbW_LS.cols() - 2);
	MatrixXd ALS = AbW_LS.block(0, 0, AbW_LS.rows(), N_unknows);
	VectorXd bLS = AbW_LS.block(0, AbW_LS.cols()-2, AbW_LS.rows(), 1);
	VectorXd diagWeightMat = AbW_LS.block(0, AbW_LS.cols()-1, AbW_LS.rows(), 1);

	// MatWeight = diagWeightMat*ones(1, N_unknows);
	MatrixXd MatWeight = diagWeightMat*(MatrixXd::Ones(1, N_unknows));  

	// AALS = ( (ALS.*MatWeight)' )*ALS;
	MatrixXd AALS_int = (ALS.array()*MatWeight.array()).transpose();                     
	MatrixXd AALS = AALS_int*ALS;                                       

	// bbLS = (ALS')*(diagWeightMat.*bLS);
	MatrixXd bbLS_int = ALS.array().transpose();
	VectorXd bbLS_int2 = diagWeightMat.array()*bLS.array();
	VectorXd bbLS = bbLS_int*bbLS_int2; 

	// Output: // deltaEpsi = AALS\bbLS;
	VectorXd LS_Adjustement_Out = AALS.colPivHouseholderQr().solve(bbLS);

	return LS_Adjustement_Out;

}// end function LS_Adjustment
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Least squares adjustment statistic
VectorXd LeverArmsEstimation::LS_Adjust_Statistic(MatrixXd const& AbW_LS, VectorXd DeltaEpsi, double probability)
{
	/* This function is used to compute least squares adjustment statistic

	Input data:
	- AbW_LS, ALS, bLS and WLS matrix
	- DeltaEpsi, Least squares solution
	- Probability, statistic probability 

	NB:
	- Calculate confidence intervals for a value.
	- For example if we set the confidence limit to
	0.95, we know that if we repeat the sampling
	100 times, then we expect that the true value
	will be between out limits on 95 occations.

	- Note: this is not the same as saying a 95%
	confidence interval means that there is a 95%
	probability that the interval contains the true value.

	- The interval computed from a given sample either
	contains the true value or it does not.

	Output data:
	- LS_Adjust_Statistic_Out:
	* so2, Unit variance factor
	* MNR, Maximal Normalized Residual, 
	* Zint, �Internal reliability 
	* stdax, stday, respectively precision of forward and starboard component of lever arms  
	* chi2Test, Test chi 2 result, 1 if success and 0 if not.
	*/

	// Initialization
	double so2, MNR, Zint, chi2Test;
	double stdax, stday;
	MatrixXd CovXest_a;
	VectorXd diagCovObs_a; 
	VectorXd LS_Adjust_Statistic_Out(6);

	// ALS, bLS and diagWeightMat extraction
	int N_unknows = (AbW_LS.cols() - 2);
	MatrixXd ALS = AbW_LS.block(0, 0, AbW_LS.rows(), N_unknows);
	VectorXd bLS = AbW_LS.block(0, AbW_LS.cols()-2, AbW_LS.rows(), 1);
	VectorXd diagWeightMat = AbW_LS.block(0, AbW_LS.cols()-1, AbW_LS.rows(), 1);

	//// Residual computation
	VectorXd residual = bLS - ALS*DeltaEpsi;

	//// degree of freedom
	int df = ALS.rows() - ALS.cols();

	//// Unbiased estimator of the variance factor sigma^2
	MatrixXd residualT = residual.array().transpose();
	MatrixXd diagResidual = diagWeightMat.array()*residual.array();
	VectorXd so2Vect = (1.0/df)*residualT*diagResidual;
	so2 = so2Vect(0);

	// A priori variances associated to estimated parameters: AALS = ( (ALS.*MatWeight)' )*ALS : [invCovXest = AALS];
	MatrixXd MatWeight = diagWeightMat*(MatrixXd::Ones(1, N_unknows));  
	MatrixXd AALS_int = (ALS.array()*MatWeight.array()).transpose();                     
	MatrixXd AALS = AALS_int*ALS;                  
	MatrixXd CovXest = AALS.inverse();

	// Variances associated to observations
	VectorXd diagCovObs = diagWeightMat.array().inverse(); 

	// for chi2 test:
	double alpha = (1 - probability);
	chi_squared ChiDist(df);
	double g2 = quantile(complement(ChiDist, alpha/2));
	double g1 = quantile(ChiDist, alpha/2);  

	// If successfull chi2 test
	if (df*so2/g2 <= 1 && 1 <= df*so2/g1)
	{
		chi2Test = 1.0;

		// Gamma for normalized residual (student's distribution)
		students_t StudentDist(df);
		double Gamma = quantile(complement(StudentDist, alpha / 2));

		// Covariance matrices a posteriori computation
		CovXest_a = so2*CovXest;
		diagCovObs_a = so2*diagCovObs;
	}

	// If chi2 test failed
	else
	{
		chi2Test = 0.0;

		// Gamma for normalized residual (normal distribution)
		int MeanNorm = 0;
		int StdNorm = 1;
		normal s(MeanNorm, StdNorm);
		double Gamma = quantile(complement(s,  alpha/2));

		// Covariance matrices a posteriori computation
		CovXest_a = CovXest;
		diagCovObs_a = diagCovObs; 
	}

	// Residual covariance matrix
	// NB : J2 = inv(AALS)*ALS' = CovXest*ALS'
	MatrixXd J2 = CovXest*(ALS.transpose()); 

	// NB: diag(ALS*CovXest_a*ALS') = wr
	VectorXd diagACovA = (ALS*J2).diagonal();

	// CovResidual = CovObs_a - ALS*CovXest*ALS'
	VectorXd diagCovResidual =  diagCovObs_a - diagACovA;

	// Maximal Normalized residual computation: MNR = max( residual ./ sqrt(diag(CovResidual)) );
	VectorXd SqrtDiagCovResidual = diagCovResidual.array().sqrt();
	VectorXd MNRVect = residual.array()/SqrtDiagCovResidual.array();
	MNR = MNRVect.maxCoeff();

	// Internal reliability computation : Z, matrix (One column))
	// Zint = sqrt(diag(CovResidual)./diagCovObs_a);
	VectorXd ZintVect = (diagCovResidual.array()/diagCovObs_a.array()).sqrt();
	Zint = ZintVect.minCoeff()*100;

	// standard deviation associated to roll, pitch and yaw estimation
	stdax =  sqrt(CovXest_a(0, 0));      
	stday = sqrt(CovXest_a(1, 1)); 

	// Output
	LS_Adjust_Statistic_Out << stdax, stday, so2, Zint, MNR, chi2Test;
	return LS_Adjust_Statistic_Out;

} // end function LS_adjustment statistic
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Matrix needed for LS adjustment
MatrixXd LeverArmsEstimation::Matrix_ALS_bLS_WLS(string folderData, MatrixXd const& RawData, VectorXd a_bi, 
	VectorXd & ABC, int count, double so2)
{
	/* This function is used to construct matrix needed for least squares estimation (1st option)

	Input data: 
	- FolderData, data folder
	- RawData, raw data:
	[ TpsPerBeams, ping, Index_Beam, SSS_long, Roll,
	Pitch, Yaw, Pn_t1 , Pn_t2 , Pn_t3 , Roll_recep , 
	Pitch_recep , Yaw_recep,  Alpha , Beta , TravelTime ,
	A , B , C, Cax , Cay , Cs , FinalCri , indFile , indPatch ]

	Output data:
	- Matrix_ALS_bLS_WLS_Out, ALS, bLS and WLS matrices
	*/

	// Reading metadata file
	string fileMetadata(folderData + "/MetaData.txt");
	Metadata MetaData_Out = readTextFileMetadata(fileMetadata);

	// Reading SVP file
	double draft = MetaData_Out.Draft;
	string fileSVP(folderData + "/SVP.txt");
	MatrixXd ProfilC = readTextFileSVP(fileSVP, draft);

	// Lever arms and boresight angles
	VectorXd True_a_bi = MetaData_Out.CordTx - MetaData_Out.CordGNSS;
	a_bi(2) = True_a_bi(2);
	VectorXd Boresight = MetaData_Out.PatchBoresight;

	// Uncertainties on raw data
	VectorXd SigRawData = RawDataUncertainties(MetaData_Out);

	// Initialization
	MatrixXd XYZn_all(RawData.rows(), 3);
	MatrixXd sigXYZn_all(RawData.rows(), 3);

	vector<vector<int>> indPerPatch;
	vector<double> A_all, B_all, C_all;
	int indPrevious(0);

	int N_soundings = RawData.rows();
	MatrixXd derXYZ_dax(N_soundings, 3), derXYZ_day(N_soundings, 3);
	double dphi = Boresight(0), dtheta = Boresight(1), dpsi = Boresight(2);

	VectorXd derAbi_dax(3), derAbi_day(3);
	derAbi_dax << 1, 0, 0; derAbi_day << 0, 1, 0;

	//// Loop on each sounding : Georeferencing and Xn derivative wrt lever arms
	for(int j=0; j<RawData.rows(); j++)
	{
		//// Georeferencing
		// Unit vector computation
		double Beta = RawData(j, 14), Alpha = RawData(j, 13);
		VectorXd attitude(3), attitude_Recp(3);
		attitude << RawData(j, 4), RawData(j, 5)  , RawData(j, 6);
		attitude_Recp << RawData(j, 10), RawData(j, 11)  , RawData(j, 12);
		VectorXd sinAzCosAzBetan = UnitVectorComputation(attitude, attitude_Recp, Boresight, Beta, Alpha);

		// Ray Tracing
		double TravelTime = RawData(j, 15);
		VectorXd SLGF = RayTracing(sinAzCosAzBetan, ProfilC,  TravelTime);

		// Data merging
		VectorXd Pn(3);
		Pn << RawData(j, 7), RawData(j, 8), RawData(j, 9);
		VectorXd XYZn = MergingData(Pn, attitude, SLGF, a_bi);
		XYZn_all.block(j, 0, 1, 3) = XYZn.array().transpose();


		////// derXn_Lever arms
		// roll, pitch, heading, rbs
		double phi = attitude(0), theta = attitude(1), psi = attitude(2);
		MatrixXd C_bI_LGF;
		VectorXd r_bS;

		// Cbin
		C_bI_LGF = DCM12(phi,theta,psi);

		// derivative of Xn wrt lever arms
		derXYZ_dax.block(j, 0, 1, 3) = (C_bI_LGF*derAbi_dax).array().transpose();
		derXYZ_day.block(j, 0, 1, 3) = (C_bI_LGF*derAbi_day).array().transpose();

		//// Uncertainties computation
		VectorXd sigXYZn = SoundingsUncertainties(attitude, attitude_Recp, SLGF, Boresight, a_bi, SigRawData);
		sigXYZn_all.block(j, 0, 1, 3) = sigXYZn.array().transpose();


		//// Sort patching index
		double indPatch = RawData(j, 24);
		int indPatchInt = static_cast<int>(indPatch);
		double A = RawData(j, 16), B = RawData(j, 17), C = RawData(j, 18);
		indexPerPatch(indPerPatch, indPatchInt, j, indPrevious,
			A_all, B_all, C_all, A, B, C);
	}


	//// Loop on each surface element: ALS, bLS and WLS computation
	// Initialization
	int N_unknows = indPerPatch.size()*3 + 2;
	MatrixXd ALS = MatrixXd::Zero(N_soundings, N_unknows), bLS(N_soundings, 1), diagWLS(N_soundings, 1);
	MatrixXd Matrix_ALS_bLS_WLS_Out(N_soundings, N_unknows+2);

	int k = 0;
	int ind_abc = 0;
	VectorXd Xn, Yn, Zn;
	VectorXd sigXn, sigYn, sigZn;
	VectorXd derX_dax, derX_day;
	VectorXd derY_dax, derY_day;
	VectorXd derZ_dax, derZ_day;
	for(int i=0; i<indPerPatch.size(); i++)
	{
		// Resize to have current patch soundings and associated derivative
		Xn.resize(indPerPatch[i].size()); Yn.resize(indPerPatch[i].size());	Zn.resize(indPerPatch[i].size());
		sigXn.resize(indPerPatch[i].size()); sigYn.resize(indPerPatch[i].size()); sigZn.resize(indPerPatch[i].size());
		derX_dax.resize(indPerPatch[i].size()); derX_day.resize(indPerPatch[i].size());
		derY_dax.resize(indPerPatch[i].size()); derY_day.resize(indPerPatch[i].size());
		derZ_dax.resize(indPerPatch[i].size()); derZ_day.resize(indPerPatch[i].size());

		// XYZn
		Xn = XYZn_all.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1);
		Yn = XYZn_all.block(indPerPatch[i][0], 1, indPerPatch[i].size(), 1);
		Zn = XYZn_all.block(indPerPatch[i][0], 2, indPerPatch[i].size(), 1);

		// derivative XYZn%dax
		derX_dax = derXYZ_dax.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1);
		derY_dax = derXYZ_dax.block(indPerPatch[i][0], 1, indPerPatch[i].size(), 1);
		derZ_dax = derXYZ_dax.block(indPerPatch[i][0], 2, indPerPatch[i].size(), 1);

		// derivative XYZn%day
		derX_day = derXYZ_day.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1);
		derY_day = derXYZ_day.block(indPerPatch[i][0], 1, indPerPatch[i].size(), 1);
		derZ_day = derXYZ_day.block(indPerPatch[i][0], 2, indPerPatch[i].size(), 1);

		// ABC construction
		double A_i, B_i, C_i;
		if (count == 0)
		{	A_i = A_all[i]; B_i = B_all[i]; C_i = C_all[i];
		ABC.resize(N_unknows-2,1);
		ABC(k) = A_i; ABC(k+1) = B_i; ABC(k+2) = C_i;
		}
		else
		{ A_i = ABC(k); B_i = ABC(k+1); C_i = ABC(k+2); k += 3;}

		// derf_dax, derf_day (f(chi) = Z - AX - BY - C)
		VectorXd derf_dax    = derZ_dax - A_i*derX_dax- B_i*derY_dax;
		VectorXd derf_day  = derZ_day - A_i*derX_day - B_i*derY_day;

		// derf_ABC
		MatrixXd derf_ABC = MatrixXd::Zero(indPerPatch[i].size(), indPerPatch.size()*3);
		derf_ABC.block(0, ind_abc    , indPerPatch[i].size(), 1) = -Xn;
		derf_ABC.block(0, ind_abc + 1, indPerPatch[i].size(), 1) = -Yn;
		derf_ABC.block(0, ind_abc + 2, indPerPatch[i].size(), 1) = -MatrixXd::Ones(indPerPatch[i].size(),1);

		// ALS
		ALS.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1) = derf_dax;
		ALS.block(indPerPatch[i][0], 1, indPerPatch[i].size(), 1) = derf_day;
		ALS.block(indPerPatch[i][0], 2, indPerPatch[i].size(), indPerPatch.size()*3) = derf_ABC;
		ind_abc += 3;

		// bLS: -f(chi0) = bLS = AX + BY + C - Z 
		bLS.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1) = A_i*Xn + B_i*Yn + 
			C_i*(MatrixXd::Ones(indPerPatch[i].size(),1)) - Zn;

		// sig2XYZn
		sigXn = sigXYZn_all.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1);
		sigYn = sigXYZn_all.block(indPerPatch[i][0], 1, indPerPatch[i].size(), 1);
		sigZn = sigXYZn_all.block(indPerPatch[i][0], 2, indPerPatch[i].size(), 1);

		// sig_b_^2 =  variance of bLS
		diagWLS.block(indPerPatch[i][0], 0, indPerPatch[i].size(), 1) = (1/so2)*(A_i*A_i*(sigXn.array().square()) + 
			B_i*B_i*(sigYn.array().square()) + 
			sigZn.array().square()).array().inverse();
	}

	// Output
	Matrix_ALS_bLS_WLS_Out << ALS, bLS, diagWLS;
	return Matrix_ALS_bLS_WLS_Out;

} // end function Matrix_ALS_bLS_WLS
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// LAAC : Iterative least square adjustment and statistics
VectorXd LeverArmsEstimation::LAAC_results(MatrixXd const& RawData, double probability, 
	                                        string fileName, string dirString3rd, string folderData, VectorXd a_bi)
{
	/* This function is used to estimate lever arms by 
	iterative least square and also provides results statistics

	Input data:
	- floderData, folder data
	- RawData, data folder
	- probability, probability for LS adjustment statistic

	Output data:
	- LAAC_results_Out, containing, ax and ay, 
	associated precision and statistics parameters:
	[delax, delay, 
	 stdax, stday,
	 so2, Zint, MNR, chi2Test]
	*/
	// 
	cout << "Least squares ------------------>:" << endl;

	// Epsilon for stopping the Least squares and counter max (if no convergence)
	double eps = 0.001;
	int IterCounterMax = 50;

	// Initialization: loop counter, Norm, plan parameters initialization
	int IterCounter = 0;
	double normDelta = 1;
	int chi2Test = 0;
	double so2 = 1;
	VectorXd ABC;
	VectorXd LSStat;

	// ALS, bLS, WLS and Delta initialization
	MatrixXd AbW_LS_1st;
	VectorXd Delta;

	// Convergence of chi2Test
	while (chi2Test == 0)
	{
		cout << "New set of terations:---->" << endl;

		// Loop: Iterative least squares
		// Stop condition : the LS adjustment norm less than "eps" and the counter reaches "IterCounterMax"
		while (normDelta >= eps && IterCounter < IterCounterMax)
		{

			// ALS, bLS, WLS matrix (1st option)
			AbW_LS_1st = Matrix_ALS_bLS_WLS(folderData, RawData, a_bi, ABC, IterCounter, so2);

			//Least square solution
			Delta  =  LS_Adjustement(AbW_LS_1st);

			// Update boresight and surface parameters
			a_bi(0) = a_bi(0) + Delta(0);
			a_bi(1) = a_bi(1) + Delta(1);
			ABC = ABC + Delta.block(2, 0, Delta.rows()-2, 1);

			// update norm and counter
			normDelta = Delta.norm();
			IterCounter ++;

			// iteration counter
			cout << "Iteration: " << IterCounter << endl;
		}

		// Statistical least square
		LSStat = LS_Adjust_Statistic(AbW_LS_1st, Delta, probability);

		// Update for the new set of iterations
		so2 = LSStat(2);
		chi2Test = LSStat(5);
		IterCounter = 0;
		normDelta = 1;

	}// end first while - chi2Test
	cout << endl;

	// Output
	VectorXd LAAC_results_Out(8);
	LAAC_results_Out << a_bi(0), a_bi(1), LSStat;

	// Save results
	int numberPatch = (Delta.rows()-2)/3;
	Writing_LAAC_Estimation(dirString3rd, fileName, LAAC_results_Out,  IterCounter, numberPatch);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Test: Write data 
	//string const nomFichierOut("ALS_bLS_WLS.txt");
	//ofstream FileOut(nomFichierOut.c_str());

	//// header
	//FileOut << "derf_ax  derf_ay  derf_A1  derf_B1  derf_C1  derf_A2  derf_B2  derf_C2  derf_A3  derf_B3  derf_C3 "
	//		<< "derf_A4  derf_B4  derf_C4  derf_A5  derf_B5  derf_C5  derf_A6  derf_B6  derf_C6  bLS  diagWLS" << endl;

	//// synchronized data
	//for (int il =0; il < AbW_LS_1st.rows(); il++)
	//{
	//	FileOut << std::fixed << setprecision (12)  << AbW_LS_1st(il, 0)   << "\t"  << AbW_LS_1st(il, 1) 
	//	<< "\t" << AbW_LS_1st(il, 2)  << "\t"  << AbW_LS_1st(il, 3)   << "\t"  << AbW_LS_1st(il, 4) 
	//	<< "\t" << AbW_LS_1st(il, 5)  << "\t"  << AbW_LS_1st(il, 6)   << "\t"  << AbW_LS_1st(il, 7) 
	//	<< "\t" << AbW_LS_1st(il, 8)  << "\t"  << AbW_LS_1st(il, 9)   << "\t"  << AbW_LS_1st(il, 10) 
	//	<< "\t" << AbW_LS_1st(il, 11) << "\t"  << AbW_LS_1st(il, 12)  << "\t"  << AbW_LS_1st(il, 13) 
	//	<< "\t" << AbW_LS_1st(il, 14) << "\t"  << AbW_LS_1st(il, 15)  << "\t"  << AbW_LS_1st(il, 16) 
	//	<< "\t" << AbW_LS_1st(il, 17) << "\t"  << AbW_LS_1st(il, 18)  << "\t"  << AbW_LS_1st(il, 19) 
	//	<< "\t" << AbW_LS_1st(il, 20) << "\t"  << AbW_LS_1st(il, 21)  << endl;
	//}
	//FileOut.close();

	//cout << "ABC: " << endl << ABC << endl;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return LAAC_results_Out;

}// end function LAAC results
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// writing LAAC results in files
void LeverArmsEstimation::Writing_LAAC_Estimation(string dirString3rd, string fileName, VectorXd LAAC_results_Out, 
	                                                  int IterCounter, int numberPatch)
{
	/* This function is used to write lever arms estimation results

	Input data:
	- fileName, output file name
	- LAAC_results_Out, Containting estimation and results and associated statistics
	- IterCounter, number of iteration for converging
	- numberPatch, number of surface element used to estimate lever arms
	*/
	
	// const variable
	string FileName = dirString3rd + "\\" + fileName + ".txt";

	// writing
	ofstream FileOut(FileName.c_str());
	FileOut << "LAAC RESULTS WITH ASSOCIATED STANDARD DEVIATION ----->>>:"       << "\n"  
		<< "===================================================================" << "\n" << "\n"

		<< "Number of used surface elements:" << "\t" << numberPatch             << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "Number of iterations:" << "\t" << IterCounter                        << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "ax (forward) in metre :"   << "\t" << LAAC_results_Out(0)    << "\n"
		<< "ay (starboard) in metre :" << "\t" << LAAC_results_Out(1)    << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "STD of ax in metre :" << "\t" << LAAC_results_Out(2) << "\n"
		<< "STD of ay in metre :" << "\t" << LAAC_results_Out(3) << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "Unit variance factor:"					<< "\t" << LAAC_results_Out(4)    << "\n"
		<< "Internal reliability:"					<< "\t" << LAAC_results_Out(5)    << "\n"
		<< "Maximal normalized residual:"			<< "\t" << LAAC_results_Out(6)    << "\n"
		<< "Chi2 test (1 is True and 0 is False):"	<< "\t" << LAAC_results_Out(7)    << "\n"
		<< "===================================================================" << endl;

}// end function writing LAAC results
// --------------------------------------------------------------------------->>>>>>

