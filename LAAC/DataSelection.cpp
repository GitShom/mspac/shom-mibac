/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run LAAC (Lever Arms Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

/*/-------------------------------------------------------------------------------------/*/
// LAAC (Lever Arms Automatic Calibration)
//
// Class                  :  .cpp (DataSelection)
// Authors                :  Rabine Keyetieu, Julian Le Deunf, Gaël Roué              
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/08/12
//--------------------------------------------------------------------------------------/*/

// --------------------------------------------------------------------------->>>>>>>>>>>>>>>> LIBS:
// Class
#include <direct.h>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <algorithm>  
#include <math.h>
#include <cmath>
#include <set>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <stdio.h>
#include <ctime>
#include <pcl/segmentation/sac_segmentation.h>
#include <Engine.h>
#include "DataSelection.h"

// Using name space
using namespace std;
using namespace pcl;
using namespace Eigen;

// Structure sort
struct IdxCompare
{
    const std::vector<double>& target;
    IdxCompare(const std::vector<double>& target): target(target) {}
    bool operator()(double a, double b) const { return target[a] < target[b]; }
};
// ------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>


// ------------------------------------------------------------------------>>>>>> Methods definition
// --------------------------------------------------------------------------->>>>>>
// Conctructor
DataSelection::DataSelection(void)
{
	m_LAAC_DataSelection = "Data selection";
}// end of function constructor
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Get current directory
string DataSelection::current_working_directory()
{
	/* This function is used to get current directory
	*/

	char* cwd = _getcwd( 0, 0 ) ; // **** microsoft specific ****
	string working_directory(cwd) ;
	free(cwd) ;
	return working_directory ;

}// end function current_working_directory
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Density
vector<int> DataSelection::density(double Emin, double Emax, double Nmin, double Nmax, MatrixXd const& D, int Nbpoint)
{
	/* This function subdivides the current zone in 4 and identifies if each quadrant
	has a significant density.

	Input data: 
	- Emin, Emax, Nmin, Nmax, boundaries of the current zone

	Output data:
	- density_Out, results of density test
	*/

	// Initialization
	double disteast = Emax - Emin;
	double distnorth = Nmax - Nmin;
	vector <double> D1;
	vector <double> D2;
	vector <double> D3;
	vector <double> D4;
	bool Criteria;
	vector <int> density_Out;

	// Loop on each line of D (each sounding)
	for (int i = 0; i < D.col(0).size(); i++)
	{
		// Quadrant 1
		if ( D(i,3) >= Emin && D(i,3) <= Emin+disteast/2 && D(i,4) >= Nmin && D(i,4) <= Nmin+distnorth/2) 
		{
			D1.push_back(D(i,4));
		}

		// Quadrant 2
		if ( D(i,3) >= Emin+disteast/2 && D(i,3) <= Emax && D(i,4) >= Nmin && D(i,4) <= Nmin+distnorth/2)
		{
			D2.push_back(D(i,4));
		}

		// Quadrant 3
		if ( D(i,3) >= Emin && D(i,3) <= Emin+disteast/2 && D(i,4) >= Nmin+distnorth/2 && D(i,4) <= Nmax )
		{
			D3.push_back(D(i,4));
		}

		// Quadrant 4
		if ( D(i,3) >= Emin+disteast/2 && D(i,3) <= Emax && D(i,4) >= Nmin+distnorth/2 && D(i,4) <= Nmax)
		{
			D4.push_back(D(i,4)); 
		}

	}// end for

	// point number respectively in quadrant 1, 2, 3, and 4
	int Len1 = D1.size();
	int Len2 = D2.size();
	int Len3 = D3.size();
	int Len4 = D4.size();	

	// Criteria = True if we have enough soundings on each next quadrant and false if not
	if (Len1 > (Nbpoint/5) && Len2 > (Nbpoint/5) && Len3 > (Nbpoint/5) && Len4 > (Nbpoint/5))
		Criteria = true;
	else
		Criteria = false;

	// Output
	density_Out.push_back(Criteria);
	density_Out.push_back(Len1);
	density_Out.push_back(Len2);
	density_Out.push_back(Len3);
	density_Out.push_back(Len4);
	return density_Out;

}// end function density
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Extract soundings from a defined zone
vector <int> DataSelection::SoundingsInPatchQuad(double Emin, double Emax, double Nmin, double Nmax, 
												 MatrixXd const& Data, int i, vector <int> indice)
{
	/* This function is used to extract current file soundings
	from a defined zone

	Input data: 
	- Emin, Emax, Nmin, Nmax, boundaries of the current zone
	- Data, soundings of the entire zone
	- i, file index
	- indice, representing index associated to each file

	Output data:
	- SoundingsInPatchQuad_Out, extract associated soundings index.
	*/

	// Initialization
	vector <int> SoundingsInPatchQuad_Out;

	// Loop on soundings of the current file
	for (int i_b = indice[i]; i_b < indice[i+1]; i_b++)
	{
		if (Data(i_b,3) > Emin && Data(i_b,3) < Emax && Data(i_b,4) > Nmin && Data(i_b,4) < Nmax) 
		{
			SoundingsInPatchQuad_Out.push_back(i_b);
		}	
	}
	return SoundingsInPatchQuad_Out;

}// end of function SoundingsInPatchQuad
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Total Least Squares
vector <double> DataSelection::TotalLS_Refinement(double A,double B,double C, VectorXd const& X, VectorXd const& Y, VectorXd const& Z,
	 VectorXd const& sigmaX,  VectorXd const& sigmaY,  VectorXd const& sigmaZ, double eps)
{
	/* This function is for Deming least square refinement

	Input data: 
	- A, B, C : initialization of the unknows (plan equation Parameters)
	- X, Y, Z : the points required to satisfy the plan equation, Z = AX + BY + C
	- sigmaX, sigmaY, sigmaZ : The points associated variances
	- eps : The threshold for stopping the iteration

	Output data:
	- TotalLS_Refinement_Out, containing plan parameters and associated covariances
	*/

	// Initialization
	double normABC = 1, sumW, sumWX, sumWY, sumWZ;
	VectorXd W(X.size());

	// Loop for Deming least square
	while(normABC > eps)
	{
		// Weighted matrix Computation:
		// W = 1./(A*A*sigX*sigX + B*B*sigY*sigY + sigZ*sigZ)
		for (int i = 0; i < X.size(); i++)
		{
			W[i] = 1/(A*A*sigmaX[i]*sigmaX[i] + B*B*sigmaY[i]*sigmaY[i] + sigmaZ[i]*sigmaZ[i]);
		}
		sumW = W.sum();

		// Xbar, Ybar, Zbar (mean of points) computation
		VectorXd wx(X.size());
		VectorXd wy(X.size());
		VectorXd wz(X.size());

		for (int i = 0; i < X.size(); i++)
		{
			wx[i] = (W[i]*X[i]);
			wy[i] = (W[i]*Y[i]);
			wz[i] = (W[i]*Z[i]);
		}

		sumWX = wx.sum();
		sumWY = wy.sum();
		sumWZ = wz.sum();

		double Xbar = sumWX/sumW;
		double Ybar = sumWY/sumW;
		double Zbar = sumWZ/sumW;

		// Estimation of C, (Z = AX + BY + C)
		double C_final = Zbar - A*Xbar - B*Ybar;

		VectorXd Xc(X.size());
		VectorXd Yc(X.size());
		VectorXd Zc(X.size());

		VectorXd WXc(X.size());
		VectorXd WXcYc(X.size());
		VectorXd WYc(X.size());
		VectorXd WXcZc(X.size());
		VectorXd WYcZc(X.size());

		for (int i = 0; i < X.size(); i++)
		{
			Xc[i] = (X[i] - Xbar);
			Yc[i] = (Y[i] - Ybar);
			Zc[i] = (Z[i] - Zbar);
			WXc[i] = (W[i]*Xc[i]*Xc[i]);
			WYc[i] = (W[i]*Yc[i]*Yc[i]);
			WXcYc[i] = (W[i]*Xc[i]*Yc[i]);
			WXcZc[i] = (W[i]*Xc[i]*Zc[i]);
			WYcZc[i] = (W[i]*Yc[i]*Zc[i]);
		}

		double sumWXc = WXc.sum();
		double sumWYc = WYc.sum();
		double sumWXcYc = WXcYc.sum();
		double sumWXcZc = WXcZc.sum();
		double sumWYcZc = WYcZc.sum();

		// To solve the problem MatA*unknown = Matb, With unknown = (A, B) 
		// Computation of Matrice MatA
		Matrix2f MatA(2, 2);
		MatA << sumWXc, sumWXcYc, sumWXcYc, sumWYc;

		// Computation of Matrice Matb 
		Vector2f Matb;
		Matb << sumWXcZc, sumWYcZc;

		// Linear Solve
		Vector2f x = MatA.colPivHouseholderQr().solve(Matb);
		double A_final = x(0);
		double B_final = x(1);

		Vector3f NORM;
		NORM << A-A_final, B-B_final, C-C_final;
		normABC = NORM.norm();

		// Update result
		A = A_final;
		B = B_final;
		C = C_final;
	}

	// Update of the weighted matrix
	VectorXd WXX(X.size());
	VectorXd WXY(X.size());
	VectorXd WX(X.size());
	VectorXd WYY(X.size());
	VectorXd WY(X.size());
	VectorXd W_final(X.size());

	for (int i = 0; i < X.size(); i++)
	{
		W_final[i] = 1/(A*A*sigmaX[i]*sigmaX[i] + B*B*sigmaY[i]*sigmaY[i] + sigmaZ[i]*sigmaZ[i]);
		WXX[i] = W_final[i]*X[i]*X[i];
		WXY[i] = W_final[i]*X[i]*Y[i];
		WX[i] = W_final[i]*X[i];
		WYY[i] = W_final[i]*Y[i]*Y[i];
		WY[i] = W_final[i]*Y[i];
	}


	// Computation of the variances of parameters
	double sumW_final = W_final.sum();
	double sumWXX2 = WXX.sum();
	double sumWXY2 = WXY.sum();
	double sumWX2 = WX.sum();
	double sumWYY2 = W_final.sum()*Y.sum()*Y.sum();
	double sumWY2 = W_final.sum()*Y.sum();

	Matrix3d M;
	Matrix3d CovMat;

	M(0,0) = WXX.sum();
	M(1,0) = WXY.sum();
	M(2,0) = WX.sum();
	M(0,1) = WXY.sum();
	M(1,1) = WYY.sum();
	M(2,1) = WY.sum();
	M(0,2) = WX.sum();
	M(1,2) = WY.sum();
	M(2,2) = W_final.sum();

	CovMat = M.inverse();

	// Extraction of variances and covariances of the parameters
	double sigA = sqrt(CovMat(0,0));
	double sigB = sqrt(CovMat(1,1));
	double sigC = sqrt(CovMat(2,2));
	double varAB = CovMat(0,1);
	double varAC = CovMat(0,2);
	double varBC = CovMat(1,2);

	// Output
	vector <double> result;
	result.push_back(A);
	result.push_back(B); 
	result.push_back(C);
	result.push_back(sigA);
	result.push_back(sigB);
	result.push_back(sigC);
	result.push_back(varAB);
	result.push_back(varAC);
	result.push_back(varBC);
	return result;

}// end of function TotalLS_Refinement
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Rotation Matrix ENU convention
MatrixXd DataSelection::RotationMatrix(double Roll, double Pitch, double Yaw)
{
	/* This function is used to compute the derivative of the DirectionCosine Matrix (DCM)
	from frame 1 to frame 2 (ENU convention) wrt phi, theta, psi.

	Input data: 
	- Roll, Pitch, Yaw, attitude angle of 1 wrt 2

	Output data:
	- RotationMatrix_Out, rotation matrix and derivative in ENU configuration
	*/

	// Initialization
	double cR = cos(Roll);
	double sR = sin(Roll);
	double cP = cos(Pitch);
	double sP = sin(Pitch);
	double cY = cos(Yaw);
	double sY = sin(Yaw);

	Matrix3d R(3,3), dR_dRoll(3,3), dR_dPitch(3,3), dR_dYaw(3,3);

	// DCM - ENU convention
	R << cY*cR+sY*sR*sP,sY*cP,cY*sR-sY*cR*sP,
		cY*sR*sP-sY*cR,cY*cP,-sY*sR-cY*cR*sP,
		-sR*cP,sP,cR*cP;

	// derivative wrt roll - ENU convention
	dR_dRoll << sY*sP*cR-cY*sR,0,cY*cR+sY*sR*sP,
		sY*sR+cY*sP*cR,0,cY*sR*sP-sY*cR,
		-cR*cP,0,-cP*sR;

	// derivative wrt pitch - ENU convention
	dR_dPitch << sY*cP*sR,-sP*sY,-cP*cR*sY,
		cY*cP*sR,-cY*sP,-cP*cR*cY,
		sP*sR,cP,-cR*sP;

	// derivative wrt yaw - ENU convention
	dR_dYaw << cY*sR*sP-sY*cR,cP*cY,-sY*sR-cY*cR*sP,
		-cY*cR-sY*sR*sP,-cP*sY,sY*cR*sP-cY*sR,
		0,0,0;

	// Output
	MatrixXd RotationMatrix_Out(3,12);
	RotationMatrix_Out << R, dR_dRoll, dR_dPitch, dR_dYaw;
	return RotationMatrix_Out;

} // end function RotationMatrix
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Inverse Rotation  Matrix
MatrixXd DataSelection::RotationMatrixInv(double Roll, double Pitch, double Yaw)
{
	/* This function is used to compute the inverse of the DirectionCosine Matrix (DCM) 
	from frame 1 to frame 2, so to compute the DCM from frame 2 to frame 1 (ENU convention).

	Input data: 
	- Roll, Pitch, Yaw, attitude angle of 1 wrt 2

	Output data:
	- RotationMatrixInv_Out
	*/

	// Initialization
	double cR = cos(Roll);
	double sR = sin(Roll);
	double cP = cos(Pitch);
	double sP = sin(Pitch);
	double cH = cos(Yaw);
	double sH = sin(Yaw);
	Matrix3d RotationMatrixInv_Out;

	// Inverse DCM
	RotationMatrixInv_Out << sR*sH*sP+cR*cH, sR*cH*sP-cR*sH, -sR*cP,
		sH*cP, cH*cP, sP,
		sR*cH-cR*sH*sP, -cR*cH*sP-sR*sH, cR*cP;

	// Output
	return RotationMatrixInv_Out;

}// end function RotationMatrixInv
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// ObservCrtieria function
vector <double> DataSelection::observCriteria(double Xc,double Yc,VectorXd Zc, VectorXd Xb,VectorXd Yb,VectorXd Zb,
	VectorXd Roll,VectorXd Pitch,VectorXd Yaw,double a_g,double b_g)
{
	/* This function is used to compute the criterion sensitivity for the horizontal coordinates (ax and ay) 
	of lever arms associated to each surface element

	NB: * Criteria = (-ag, -bg, 1)*(xc, yc, zcj)^T - cg
	    * with (xc, yc, zc)^T = (xb, yb, zb)^T + Cbin.Cbsbi.rbs + Cbin.a_bi
		Where:  * rbs = Cbibs.Cnbi.rn 
				* rn = [(xc, yc, zc)^T - (xb, yb, zb)^T] 
				* a_bi = Cnbi.[(xc, yc, zc)^T - (xb, yb, zb)^T - (rn1, rn2, rn3)^T]

	Input data: 
	- Xc, Yc, Zc, Coordinates of the surface element center for one Line
	- Xb, Yb, Zb, Average of soundings positioning inside the current zone
	- Roll, Pitch, Yaw, Average of soundings attitude inside the current zone
	- ag, bg, global parameters from the entire lines inside the current zone

	Output data:
	- observCriteria_Out, Cax, Cay, Caz, respectively ax, ay and az sensitivity criteria
	*/

	// Initialization
	// Cnbi, Cbin and derivative of Cbin wrt attitude (roll, pitch and heading)
	MatrixXd C_n_bI, C_bI_nn(3,3), dC_bI_n_dRoll(3,3), dC_bI_n_dPitch(3,3), dC_bI_n_dYaw(3,3);

	// derivative of (xc, yc, zc) wrt ax, ay and az
	VectorXd dX_dax(3), dX_day(3), dX_daz(3);

	// derivative of Criteria wrt ax, ay, az
	VectorXd dcritere_dax(Roll.size()), dcritere_day(Roll.size()), dcritere_daz(Roll.size());

	// jacobian of dcritere_dax, dcritere_day and dcritere_daz wrt roll, pitch and heading
	VectorXd J_ax(3), J_ay(3), J_az(3);

	// uncertainties associated to dcritere_dax, dcritere_day, dcritere_daz
	double S_ax, S_ay, S_az;

	// Derivative of abI with respect to a1, a2 and a3
	VectorXd derAbI_ax(3), derAbI_ay(3), derAbI_az(3);
    derAbI_ax << 1, 0, 0;
    derAbI_ay << 0, 1, 0;   
    derAbI_az << 0, 0, 1;   

	// Uncertainties on data to compute the standard deviation of criterion derivative
	double Pi = PI;
	double d2r = Pi/180;
	double Sigma_Roll = 0.05*d2r, Sigma_Pitch = 0.05*d2r, Sigma_Yaw = 0.05*d2r;
	VectorXd Sigma(3);   Sigma << Sigma_Roll*Sigma_Roll, Sigma_Pitch*Sigma_Pitch,  Sigma_Yaw*Sigma_Yaw;
	MatrixXd SIGMA(3,3); SIGMA = Sigma.asDiagonal();

	// Loop on each file inside the current zone
	for (int i = 0; i < Roll.size(); i++)
	{
		//// Rbs modelization
		// modelization rn = (sounder acoustic center to seabed)
		VectorXd R(3);
		R[0] = Xc - Xb[i];
		R[1] = Yc - Yb[i];
		R[2] = Zc[i] - Zb[i];

		// Extraction of Rho from RbS modeliszation computation
		C_n_bI = RotationMatrixInv(Roll[i],Pitch[i],Yaw[i]);
		VectorXd R_bS = C_n_bI*R;
		double Rho = R_bS.norm();
		R.resize(0);

		//// Computation of der(xc, yc, zc) wrt lever arms (ax, ay, az)
		// Calculation of the transformation matrices for the attitude
		MatrixXd Mat_transfo2 = RotationMatrix(Roll[i],Pitch[i],Yaw[i]);
		C_bI_nn = Mat_transfo2.block(0,0,3,3);
		dC_bI_n_dRoll = Mat_transfo2.block(0,3,3,3);
		dC_bI_n_dPitch = Mat_transfo2.block(0,6,3,3);
		dC_bI_n_dYaw = Mat_transfo2.block(0,9,3,3); 

		// derivative of Xn wrt attitude
		dX_dax = C_bI_nn*derAbI_ax;
		dX_day = C_bI_nn*derAbI_ay;
		dX_daz = C_bI_nn*derAbI_az;


		// Criteria = (-ag, -bg, 1)*(xc, yc, zc)^T - cg, so
		// derCriteria = (-ag, -bg, 1)*der(xc, yc, zc)^T and
		// der(xc, yc, zc)^T = Cbin.der(a_bi)
		// therefore : derCriteria = (-ag, -bg, 1)*Cbin.der(a_bi)
		// So, der(derCriteria)_{attitude} = J_{attitude}

		// plan parameters
		VectorXd AA(3); AA << -a_g, -b_g, 1;

		// Jacobian associated to (derCriteria ->(wrt ax, ay, az))
		J_ax	<< AA.dot(dC_bI_n_dRoll*derAbI_ax),
			AA.dot(dC_bI_n_dPitch*derAbI_ax),
			AA.dot(dC_bI_n_dYaw*derAbI_ax);

		J_ay << AA.dot(dC_bI_n_dRoll*derAbI_ay),
			AA.dot(dC_bI_n_dPitch*derAbI_ay),
			AA.dot(dC_bI_n_dYaw*derAbI_ay);

		J_az << AA.dot(dC_bI_n_dRoll*derAbI_az),
			AA.dot(dC_bI_n_dPitch*derAbI_az),
			AA.dot(dC_bI_n_dYaw*derAbI_az);


		//// uncertainties associated to criteria derivative, by TPU
		S_ax = sqrt(J_ax.dot(SIGMA*J_ax));
		S_ay = sqrt(J_ay.dot(SIGMA*J_ay));
		S_az = sqrt(J_az.dot(SIGMA*J_az));


		//// derCriteria = (-ag, -bg, 1)*der(xc, yc, zc)^T: 
		// Solution normalizing by the range(Rho) and associated criteria derivative standard deviation (S_Roll,...)
		dcritere_dax[i] = (dX_dax[2] - a_g*dX_dax[0] - b_g*dX_dax[1])/(S_ax*Rho);
		dcritere_day[i] = (dX_day[2] - a_g*dX_day[0] - b_g*dX_day[1])/(S_ay*Rho);
		dcritere_daz[i] = (dX_daz[2] - a_g*dX_daz[0] - b_g*dX_daz[1])/(S_az*Rho);

	} // end for

	// Computation of roll, pitch and yaw criteria
	double Cax = abs(dcritere_dax.maxCoeff() - dcritere_dax.minCoeff());
	double Cay = abs(dcritere_day.maxCoeff() - dcritere_day.minCoeff());
	double Caz = abs(dcritere_daz.maxCoeff() - dcritere_daz.minCoeff());

	// Computation of slope
	double Cs = acos(1/sqrt((a_g*a_g + b_g*b_g + 1*1)));

	// Output 
	vector <double> observCriteria_Out;

	observCriteria_Out.push_back(Cax);
	observCriteria_Out.push_back(Cay);
	observCriteria_Out.push_back(Cs);
	return observCriteria_Out;

} // end function observCriteria
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
//QuadTree function
vector<double> DataSelection::QuadTree(double Emin, double Emax, double Nmin, double Nmax, int Nbpoint,  MatrixXd const& Data, 
	vector <int> indice, double StatisticThreshold, vector <double> Resultat)
{
	/* This function is used to process quadtree on data, it is a recursive function.

	Input data: 
	- Emin, Emax, Nmin, Nmax, boundaries of the current zone
	- Nbpoint, minimal number of point to stop the process, also used to see if soundings 
	is fairly widespread on the current zone otherwise we subdivide again
	- Data, raw data, positioning and attitude
	- indice, index corresponding to soundings of each file
	- StatisticThreshold, Threshold for plan adjustment
	- Resultat, save results

	Output data:
	- Resultat, containing surface element criteria, boundaries and plan parameters
	*/

	// floating-point values are written using fixed-point notation: 
	// the value is represented with exactly as many digits in the 
	// decimal part as specified by the precision field (precision) and with no exponent part.
	std::fixed;

	//// Initialization
	vector <double> Criteria_R;
	vector <double> varAB;
	vector <double> varAC;
	vector <double> varBC;
	VectorXd sigmaX;
	VectorXd sigmaY;
	VectorXd sigmaZ;
	vector <double> Residu;
	vector <int> Indice_Res;
	Indice_Res.push_back(0);
	VectorXd Jacr(6);
	VectorXd Mdiag0(6);
	MatrixXd Mdiag(6,6);
	double Resnorm0;
	vector <double> MaxResnorm;
	vector <double> NbrpntPatch;
	vector <int> IndMaxResNormOk;
	VectorXd X;
	VectorXd Y;
	VectorXd Z;
	MatrixXd Data_I;
	vector <int> IndDensity;
	vector <double> TotalLeastSquares;
	vector <int> result_WorkZone;
	vector <int> I;
	vector <int> Sizedata; Sizedata.push_back(0);
	vector <double> I2;
	vector <int> WorkBlock; WorkBlock.push_back(0);

	// Extraction of soundings index corresponding to the current zone for each line
	for (int i = 0; i < indice.size()-1; i++)
	{
		result_WorkZone = SoundingsInPatchQuad(Emin, Emax, Nmin, Nmax, Data, i, indice);
		for (int j = 0; j < result_WorkZone.size(); j++)
		{
			I.push_back(result_WorkZone[j]);
			I2.push_back(result_WorkZone[j]);
		}

		Sizedata.push_back(I.size());
		WorkBlock.push_back(I2.size());
		I.clear();
	}

	// Extraction of data corresponding
	MatrixXd DataWork (I2.size(), 15);
	for (int k = 0; k < I2.size(); k++)
	{
		DataWork(k,0) = Data(I2[k],0); // Tps
		DataWork(k,1) = Data(I2[k],1); // Ping index
		DataWork(k,2) = Data(I2[k],2); // Beam index

		DataWork(k,3) = Data(I2[k],3);   // X
		DataWork(k,4) = Data(I2[k],4);   // Y
		DataWork(k,5) = Data(I2[k],5);   // Z

		DataWork(k,6) = Data(I2[k],6);   // sigX
		DataWork(k,7) = Data(I2[k],7);   // sigY
		DataWork(k,8) = Data(I2[k],8);   // sigZ

		DataWork(k,9) = Data(I2[k],9);   // Roll
		DataWork(k,10) = Data(I2[k],10); // Pitch
		DataWork(k,11) = Data(I2[k],11); // Heading

		DataWork(k,12) = Data(I2[k],12); // Easting
		DataWork(k,13) = Data(I2[k],13); // Northing
		DataWork(k,14) = Data(I2[k],14); // Height
	}


	//// To know if the number of soundings per line in the current zone is greater than Nbpoint 
	// This is the -->> (STOP CONDITION)
	vector <int> IndSize;
	for (int i = 0; i < Sizedata.size();i++)
	{
		if (Sizedata[i] > Nbpoint)
		{
			IndSize.push_back(i);
		}
	}


	// Start process and STOP if we do not have: IndSize.size() >=2
	MatrixXd Density(IndSize.size(), 5);
	if (IndSize.size() >=2)
	{   
		//// To see if for each line, soundings is fairly widespread - CONDITION 1 (to subdivide)
		for(int j = 0; j < IndSize.size(); j++)
		{
			// Extract data corresponding to the current zone
			Data_I = DataWork.block(WorkBlock[IndSize[j]-1],0,WorkBlock[IndSize[j]]-WorkBlock[IndSize[j]-1],14);

			// Criteria (false or true) - density function call
			vector <int> D = density(Emin, Emax, Nmin, Nmax, Data_I, Nbpoint);
			Density(j,0) = D[0]; 

			// filtering patch
			if (Density(j,0) == 1)
			{
				IndDensity.push_back(IndSize[j]);
			}
		}



		//// TotalLeastSquares function - CONDITION 2 (to subdivide)
		vector <double> a;
		vector <double> b;
		vector <double> c;
		vector <double> sigA;
		vector <double> sigB;
		vector <double> sigC;
		vector <double> MaxResNorm;
		if (IndDensity.size()  >= 2)
		{
			// to adjust the current zone assimilated to a plan
			for (int i = 0; i < IndDensity.size(); i++)
			{
				// Extraction of coordinated corresponding
				X =  DataWork.block(WorkBlock[IndDensity[i]-1],3,WorkBlock[IndDensity[i]]-WorkBlock[IndDensity[i]-1],1);
				Y =  DataWork.block(WorkBlock[IndDensity[i]-1],4,WorkBlock[IndDensity[i]]-WorkBlock[IndDensity[i]-1],1);
				Z =  DataWork.block(WorkBlock[IndDensity[i]-1],5,WorkBlock[IndDensity[i]]-WorkBlock[IndDensity[i]-1],1);

				// Extraction of uncertainties coordiantes corresponding
				sigmaX = DataWork.block(WorkBlock[IndDensity[i]-1],6,WorkBlock[IndDensity[i]]-WorkBlock[IndDensity[i]-1],1);
				sigmaY = DataWork.block(WorkBlock[IndDensity[i]-1],7,WorkBlock[IndDensity[i]]-WorkBlock[IndDensity[i]-1],1);
				sigmaZ = DataWork.block(WorkBlock[IndDensity[i]-1],8,WorkBlock[IndDensity[i]]-WorkBlock[IndDensity[i]-1],1);

				// We compute plan parameters for each survey strip satisfying the density Test (1st step)
				TotalLeastSquares = TotalLS_Refinement(1,1,1, X,Y,Z,sigmaX,sigmaY,sigmaZ,0.0001);

				a.push_back(TotalLeastSquares[0]);
				b.push_back(TotalLeastSquares[1]);
				c.push_back(TotalLeastSquares[2]);

				sigA.push_back(TotalLeastSquares[3]);
				sigB.push_back(TotalLeastSquares[4]);
				sigC.push_back(TotalLeastSquares[5]);

				varAB.push_back(TotalLeastSquares[6]);
				varAC.push_back(TotalLeastSquares[7]);
				varBC.push_back(TotalLeastSquares[8]);

				// Residual Computation
				VectorXd Resnorm(X.size());
				for (int k = 0; k < X.size() ; k++)
				{
					Residu.push_back(Z[k]-a[i]*X[k]-b[i]*Y[k]-c[i]);

					// Normalized residual computation
					Jacr[0] = -a[i]; 
					Jacr[1] = -b[i]; 
					Jacr[2] = 1;
					Jacr[3] = -X[k]; 
					Jacr[4] = -Y[k]; 
					Jacr[5] = -1;
					Mdiag0 << sigmaX[k]*sigmaX[k], sigmaY[k]*sigmaY[k], sigmaZ[k]*sigmaZ[k],
						sigA[i]*sigA[i], sigB[i]*sigB[i], sigC[i]*sigC[i];
					Mdiag = Mdiag0.asDiagonal();

					Resnorm0 = std::sqrt((Jacr.transpose()*Mdiag*Jacr));
					Resnorm[k] = std::abs(Residu[k] / Resnorm0);
				}
				MaxResnorm.push_back(Resnorm.maxCoeff());
				Residu.resize(0);


				// Number of points of a strip survey "i" in the studied patch
				NbrpntPatch.push_back(X.size());

				// Statistical test to know if the studied patch is planar
				if (MaxResnorm[i] < StatisticThreshold)
				{ 
					IndMaxResNormOk.push_back(IndDensity[i]);
					MaxResNorm.push_back(MaxResnorm[i]);
				}
			}

			// Test on planar adjustment - save the zone if planar (IndMaxResNormOk.size() > 2)
			VectorXd Roll(IndMaxResNormOk.size());
			VectorXd Pitch(IndMaxResNormOk.size());
			VectorXd Yaw(IndMaxResNormOk.size());
			VectorXd index(IndMaxResNormOk.size());

			VectorXd Xb(IndMaxResNormOk.size());
			VectorXd Yb(IndMaxResNormOk.size());
			VectorXd Zb(IndMaxResNormOk.size());

			VectorXd Zc(IndMaxResNormOk.size());

			VectorXd a_mean(IndMaxResNormOk.size());
			VectorXd b_mean(IndMaxResNormOk.size());
			VectorXd c_mean(IndMaxResNormOk.size());

			VectorXd sigA_mean(IndMaxResNormOk.size());
			VectorXd sigB_mean(IndMaxResNormOk.size());
			VectorXd sigC_mean(IndMaxResNormOk.size());
			if (IndMaxResNormOk.size() >= 2)
			{
				for (int i = 0; i < IndMaxResNormOk.size(); i++)
				{
					Roll[i] =  (DataWork.block(WorkBlock[IndMaxResNormOk[i]-1],9,WorkBlock[IndMaxResNormOk[i]]-WorkBlock[IndMaxResNormOk[i]-1],1)).mean();
					Pitch[i] = (DataWork.block(WorkBlock[IndMaxResNormOk[i]-1],10,WorkBlock[IndMaxResNormOk[i]]-WorkBlock[IndMaxResNormOk[i]-1],1)).mean();
					Yaw[i] = (DataWork.block(WorkBlock[IndMaxResNormOk[i]-1],11,WorkBlock[IndMaxResNormOk[i]]-WorkBlock[IndMaxResNormOk[i]-1],1)).mean();
					Xb[i] = (DataWork.block(WorkBlock[IndMaxResNormOk[i]-1],12,WorkBlock[IndMaxResNormOk[i]]-WorkBlock[IndMaxResNormOk[i]-1],1)).mean();
					Yb[i] = (DataWork.block(WorkBlock[IndMaxResNormOk[i]-1],13,WorkBlock[IndMaxResNormOk[i]]-WorkBlock[IndMaxResNormOk[i]-1],1)).mean();
					Zb[i] = (DataWork.block(WorkBlock[IndMaxResNormOk[i]-1],14,WorkBlock[IndMaxResNormOk[i]]-WorkBlock[IndMaxResNormOk[i]-1],1)).mean();

					Zc[i] = a[i]*(Emax+Emin)/2 + b[i]*(Nmax+Nmin)/2 + c[i];

					a_mean[i] = a[i];
					b_mean[i] = b[i];
					c_mean[i] = c[i];

					sigA_mean[i] = sigA[i];
					sigB_mean[i] = sigB[i];
					sigC_mean[i] = sigC[i];
				}
				Criteria_R = observCriteria((Emax+Emin)/2,(Nmax+Nmin)/2,Zc,Xb,Yb,Zb, Roll,Pitch,Yaw, a_mean.mean(), b_mean.mean());

				// We save the result
				Resultat.push_back(false);
				Resultat.push_back(Criteria_R[0]);
				Resultat.push_back(Criteria_R[1]);
				Resultat.push_back(Criteria_R[2]);

				Resultat.push_back(Emin);
				Resultat.push_back(Emax);
				Resultat.push_back(Nmin);
				Resultat.push_back(Nmax);
				Resultat.push_back(abs(Emax-Emin)*abs(Nmax-Nmin));
				Resultat.push_back(a_mean.mean());
				Resultat.push_back(b_mean.mean());
				Resultat.push_back(c_mean.mean());
				Resultat.push_back(sigA_mean.mean());
				Resultat.push_back(sigB_mean.mean());
				Resultat.push_back(sigC_mean.mean());
			}


			// If there is not at least two survey strip satisfying a plan, 
			// so the patch is not considered as planar, we subdivide again (If IndMaxResNormOk is inferior at 2, we subdivide directly)
			// CONDITION 1 TO SUBDIVIDE
			else
			{
				double disteast=Emax-Emin;
				double distnorth=Nmax-Nmin;
				Resultat = QuadTree(Emin,Emin+disteast/2,Nmin,Nmin+distnorth/2, Nbpoint, Data, indice , StatisticThreshold, Resultat);
				Resultat = QuadTree(Emin+disteast/2,Emax,Nmin,Nmin+distnorth/2, Nbpoint, Data, indice , StatisticThreshold, Resultat);
				Resultat = QuadTree(Emin,Emin+disteast/2,Nmin+distnorth/2,Nmax,Nbpoint, Data, indice , StatisticThreshold, Resultat);
				Resultat = QuadTree(Emin+disteast/2,Emax,Nmin+distnorth/2,Nmax,Nbpoint, Data, indice , StatisticThreshold, Resultat);
			}

		}



		// If density of survey strip is not sufficient we subdivide directly (not fairly widespread for at least 2 lines)
		// CONDITION 2 TO SUBDIVIDE 
		else
		{
			double disteast=Emax-Emin;
			double distnorth=Nmax-Nmin;
			Resultat = QuadTree(Emin,Emin+disteast/2,Nmin,Nmin+distnorth/2, Nbpoint, Data, indice , StatisticThreshold, Resultat);
			Resultat = QuadTree(Emin+disteast/2,Emax,Nmin,Nmin+distnorth/2, Nbpoint, Data, indice , StatisticThreshold, Resultat);
			Resultat = QuadTree(Emin,Emin+disteast/2,Nmin+distnorth/2,Nmax,Nbpoint, Data, indice , StatisticThreshold, Resultat);
			Resultat = QuadTree(Emin+disteast/2,Emax,Nmin+distnorth/2,Nmax,Nbpoint, Data, indice , StatisticThreshold, Resultat);
		}
	}



	// If we dont have enough soundings for at least 2 survey strips, we do nothing ( we abandon the patch)
	// STOP CONDITION
	else
	{
		// do nothing
	}

	// Output
	return Resultat;

} // end of function QuadTree
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Reading of data
MatrixXd DataSelection::ReadingData_DataSelection(std::vector<int> & indice, string & folderData, 
	                                              string & dirString2nd, string & dirString3rd)
{
	/* This function is used to read data coming from the georeferencing module

	Input data: 
	- fichier, files name of data
	- indice, index corresponding to soundings for each file

	Output data:
	- Data, raw data coming from georeferencing module
	*/

	// Name project
	string NameProject;
	cout << "Please type the project name (Please each time AVOID SPACE IN NAMES): " << endl;
	cin >> NameProject; cout << endl;

	cout << "Please enter the data folder link: " << endl;
	folderData; 	cin >> folderData; cout << endl;

	// Number of used lines
	int nbrLines;
	cout << "Please enter the number of lines to use: " << endl;
	cin >> nbrLines; cout << endl;

	// Provide current directory
	string curDir =  current_working_directory();

	// folder project
	string dirString = curDir + "\\" + NameProject;

	// Create second part output folder
	dirString2nd = dirString + "\\" + "Export2ndPart_" + NameProject;
	const char* dirChar1st = (char*)dirString2nd.c_str(); 	// Name of created directory in char
	mkdir(dirChar1st); 	                                    // Create directory in the current one

	// Create third part output folder
	dirString3rd = dirString + "\\" + "LAAC_Result_" + NameProject;
	const char* dirChar3rd = (char*)dirString3rd.c_str(); 	// Name of created directory in char
	mkdir(dirChar3rd); 	                                    // Create directory in the current one

	// Lines name
	string dirString1st = dirString + "\\" + "Export1stPart_" + NameProject;
	vector<string> fichier; 
	vector<string> JustNamefile;
	for(int i = 0; i<nbrLines; i++)
	{
		cout << "Please enter the name of line: " << i << endl;
		string fileName_i; cin >> fileName_i; 
		fichier.push_back(dirString1st + "\\" + fileName_i + ".txt");
		JustNamefile.push_back(fileName_i);
	}
	cout << endl;

	//Initialisation
	std::deque<double> Time;
	std::deque<double> X0, Y0, Z0;
	std::deque<double> sX, sY, sZ;
	std::deque<double> PingNum, BeamNum;
	std::deque<double> roll, pitch, heading;
	std::deque<double> easting, northing, height;

	double Timei, PingNumi, BeamNumi, X_Geo, Y_Geo, Z_Geo, sigX, sigY, sigZ, k1, Roll, Pitch, Heading;
	double Easting, Northing, Height, k2, k3, k4, k5, k6, k7;
	int k = 0;

	// Information that we dont need for the quadtree
	std::vector <double> k1_vec;
	std::vector <double> k2_vec; std::vector <double> k5_vec;
	std::vector <double> k3_vec; std::vector <double> k6_vec;  
	std::vector <double> k4_vec; std::vector <double> k7_vec;  

	// Reading
	for (int i = 0; i < fichier.size(); i++)
	{
		// Open the file
		std::ifstream fichier_data(fichier[i]);
		std::cout<<"- Load Data from file "<< JustNamefile[i] << "..." << endl;

		if (fichier_data)
		{

			// Reading file in ENU configuration
			while(fichier_data >> Timei >> PingNumi  >> BeamNumi >> Y_Geo >> X_Geo >> Z_Geo >> 
				sigY >> sigX >> sigZ >> k1 >> Roll >> Pitch >> Heading >> 
				Northing >> Easting >> Height >> k2 >> k3 >> k4 >> k5 >> 
				k6 >> k7)
			{

				// Extract data
				Time.push_back(Timei);                 k1_vec.push_back(k1);
				PingNum.push_back(PingNumi);           k2_vec.push_back(k2);
				BeamNum.push_back(BeamNumi);           k3_vec.push_back(k3);

				X0.push_back(X_Geo);                   k4_vec.push_back(k4);
				Y0.push_back(Y_Geo);                   k5_vec.push_back(k5);
				Z0.push_back(-Z_Geo);                  k6_vec.push_back(k6);   k7_vec.push_back(k7);

				sX.push_back(sigX);                   
				sY.push_back(sigY);                    
				sZ.push_back(sigZ);                    

				roll.push_back(Roll);                  
				pitch.push_back(Pitch);                
				heading.push_back(Heading);            

				easting.push_back(Easting);
				northing.push_back(Northing); 
				height.push_back(-Height);
				k++;
			}

			// extremal index of soundings corresponding to the current file
			indice.push_back(k);

			// Close files
			fichier_data.close();

		} // end if

		else
		{
			cout << "File reading error !" << endl;
		}

	}// end loop for

	// Harmonize data
	MatrixXd Data(X0.size(), 22);
	int i = 0;
	while ( i < X0.size())
	{
		Data(i,0) = Time[i];

		Data(i,1) = PingNum[i];
		Data(i,2) = BeamNum[i];

		Data(i,3) = X0[i];
		Data(i,4) = Y0[i];
		Data(i,5) = Z0[i];

		Data(i,6) = sX[i];
		Data(i,7) = sY[i];
		Data(i,8) = sZ[i];

		Data(i,9)  = roll[i];
		Data(i,10) = pitch[i];
		Data(i,11) = heading[i];

		Data(i,12) = easting[i];
		Data(i,13) = northing[i];
		Data(i,14) = height[i];

		Data(i,15) = k1_vec[i]; Data(i,16) = k2_vec[i]; Data(i,17) = k3_vec[i]; Data(i,18) = k4_vec[i];
		Data(i,19) = k5_vec[i]; Data(i,20) = k6_vec[i]; Data(i,21) = k7_vec[i];
		i++;
	}

	// Output
	return Data;

}// end function readingData_DataSelection
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Criteria extraction
MatrixXd DataSelection::CriteriaExtraction(string fichierSave, string & FileName, 
	                                        vector<double> const& Result_quadtree, 
											vector <double>& indice_kdtree, MatrixXd const& Data,
						                    vector<int> indice)
{
	/* This function is used to extract specific characteristics
	of Quadtree results

	Input data:
	- fichierSave, name of output file
	- FileName, name of output folder
	- Result_quadtree, Quadtree results
	- indice_kdtree, index correspondings to surfaces elements from Quadtree results
	- indice, index representing soundings for each survey line

	Output data:
	- Current surface element associated criteria 
	*/ 

	// Initialization
	vector <double> Cax, Cay, Cs, Emin_f, Nmax_f, b_f, sigC_f, Emax_f;
	vector <double> Nmin_f, a_f, sigB_f, EmNm_f, c_f, sigA_f;

	// Extract Criteria and index
	double indexXd = 1;
	int f = 0;
	while (f < Result_quadtree.size())
	{
		// New surface element if Result_quadtree[f] == false (see Quadtree function)
		if ( Result_quadtree[f] == false)
		{
			indice_kdtree.push_back(indexXd);
			Cax.push_back(Result_quadtree[f+1]);
			Cay.push_back(Result_quadtree[f+2]);
			Cs.push_back(Result_quadtree[f+3]);

			Emin_f.push_back(Result_quadtree[f+4]);
			Emax_f.push_back(Result_quadtree[f+5]);
			Nmin_f.push_back(Result_quadtree[f+6]);
			Nmax_f.push_back(Result_quadtree[f+7]);
			EmNm_f.push_back(Result_quadtree[f+8]);

			a_f.push_back(Result_quadtree[f+9]);
			b_f.push_back(Result_quadtree[f+10]);
			c_f.push_back(Result_quadtree[f+11]);

			sigA_f.push_back(Result_quadtree[f+12]);
			sigB_f.push_back(Result_quadtree[f+13]);
			sigC_f.push_back(Result_quadtree[f+14]);

			indexXd = indexXd + 1;
		}
		f++;
	}

	// Put Criteria in a matrix
	VectorXd El(Cax.size()), Nl(Cax.size()), Surf(Cax.size());
	MatrixXd Criteria_final(Cax.size(), 14);
	for (int i = 0; i < Cax.size(); i++)
	{
		El(i) = Emax_f[i] - Emin_f[i];
		Nl(i) = Nmax_f[i] - Nmin_f[i];
		Surf(i) = El(i)*Nl(i);

		Criteria_final(i,0) = Cax[i];
		Criteria_final(i,1) = Cay[i];
		Criteria_final(i,2) = Cs[i];

		Criteria_final(i,3) = Emin_f[i];
		Criteria_final(i,4) = Emax_f[i];
		Criteria_final(i,5) = Nmin_f[i];
		Criteria_final(i,6) = Nmax_f[i];
		Criteria_final(i,7) = EmNm_f[i];

		Criteria_final(i,8)  = a_f[i];
		Criteria_final(i,9)  = b_f[i];
		Criteria_final(i,10) = c_f[i];

		Criteria_final(i,11) = sigA_f[i];
		Criteria_final(i,12) = sigB_f[i];
		Criteria_final(i,13) = sigC_f[i];
	}


	//// Save file
	cout << "DATA SAVING ----------------->:" << endl << endl;
	vector<double> FinalCriForImages;
	MatrixXd SelectedData = SaveFile(fichierSave, Data, Criteria_final, indice,
									 FileName, FinalCriForImages);

	// Display images using engine from matlab
	vector <double> FinalCri;
	for (int i = 0; i < Criteria_final.rows(); i++)
	{
		FinalCri.push_back((Criteria_final(i,0)/Criteria_final.col(0).maxCoeff()) + 
		(Criteria_final(i,1)/Criteria_final.col(1).maxCoeff()));
	}
	int Image = ImagesDataSelection(FinalCri, FinalCriForImages, a_f,  b_f, c_f, Emin_f, Emax_f,  Nmin_f,  Nmax_f, Data, indice);

	// Output
	return SelectedData;

}// end function CriteriaExtraction
// --------------------------------------------------------------------------->>>>>>



// --------------------------------------------------------------------------->>>>>>
// Extract patches corresponding to high roll and pitch
vector<int> DataSelection::PingsWith_HighAttitude(vector<int> const& final_Indice, MatrixXd const & Data, int nbrPing)
{
	/* This function is used to select pings with attitude. 
	It is done for each survey line from the current patch. 
	Pings are selected for pitch and another group selected for roll

	Input data:
	- final_Indice, index corresponding to soundings of one survey 
	  line from the current patch
	- Data, Raw data, the same as defined for data selection step output
	- nbrPing (2*nbrPing for roll and pitch), number of ping to choose per line
	
	Output data:
	- FinalIndiceOut index corresponding to selected data.
	*/

	// Initialization
	vector<int> FinalIndiceOut, FinalIndiceOutRoll, FinalIndiceOutPitch;
	vector<double> Ping;
	vector<vector<int>> IndexPerPing;
	vector<double> RollPerPing, PitchPerPing;
	double PreviousPing(-1);

	if (final_Indice.size()>0)
	{
		// Extraction
		for (int i=0; i<final_Indice.size(); i++)
		{
			// filling data
			Ping.push_back(Data(final_Indice[i], 1));

			// Test if different ping
			if (PreviousPing == Ping[Ping.size()-1])
			{
				// Add index of the current ping
				IndexPerPing[IndexPerPing.size()-1].push_back(final_Indice[i]);
			}
			else
			{
				// Extract roll and pitch per ping
				RollPerPing.push_back(Data(final_Indice[i], 9));
				PitchPerPing.push_back(Data(final_Indice[i], 10));

				// Construct vector of vector of index for each ping of the line
				IndexPerPing.push_back(vector<int>(1, final_Indice[i]));
				PreviousPing = Ping[Ping.size()-1];
			}
		}


		// Ensure to have emough ping for the current line
		if (nbrPing < RollPerPing.size())
		{
			// Sort pings relative to roll and extract associated index
			std::vector<int> IndexSortRoll(RollPerPing.size());
			std::iota(IndexSortRoll.begin(), IndexSortRoll.end(), 0);
			auto comparatorRoll = [&RollPerPing](int a, int b){ return RollPerPing[a] > RollPerPing[b]; };
			std::sort(IndexSortRoll.begin(), IndexSortRoll.end(), comparatorRoll);

			// Sort pings relative to pitch and extract associated index
			std::vector<int> IndexSortPitch(PitchPerPing.size());
			std::iota(IndexSortPitch.begin(), IndexSortPitch.end(), 0);
			auto comparatorPitch = [&PitchPerPing](int a, int b){ return PitchPerPing[a] > PitchPerPing[b]; };
			std::sort(IndexSortPitch.begin(), IndexSortPitch.end(), comparatorPitch);

			// Loop on roll sorted data
			for (int i=0; i<nbrPing; i++)
			{
				// Extract group of index
				vector <int> GroupIndex = IndexPerPing[ IndexSortRoll[i] ];
				FinalIndiceOutRoll.insert( FinalIndiceOutRoll.end(), GroupIndex.begin(), GroupIndex.end() );
			}

			// Loop on pitch sorted data
			for (int i=0; i<nbrPing; i++)
			{
				// Extract group of index
				vector <int> GroupIndex = IndexPerPing[ IndexSortPitch[i] ];
				FinalIndiceOutPitch.insert( FinalIndiceOutPitch.end(), GroupIndex.begin(), GroupIndex.end() );
			}

			// union of FinalIndiceOutRoll and FinalIndiceOutPitch
			vector<int> FinalIndiceOutInt(FinalIndiceOutRoll.size() + FinalIndiceOutPitch.size());
			sort(FinalIndiceOutRoll.begin(),FinalIndiceOutRoll.end());
			sort(FinalIndiceOutPitch.begin(),FinalIndiceOutPitch.end()); 
			vector<int>::iterator it= set_union(FinalIndiceOutRoll.begin(),FinalIndiceOutRoll.end(),
				                                FinalIndiceOutPitch.begin(),FinalIndiceOutPitch.end(),FinalIndiceOutInt.begin());
			FinalIndiceOutInt.resize(it-FinalIndiceOutInt.begin());

			// Output
			FinalIndiceOut = FinalIndiceOutInt;

		}// end second if

	}// end first if

	// Output
	return FinalIndiceOut;


} // end of function PingsWith_HighAttitude
// --------------------------------------------------------------------------->>>>>>



// --------------------------------------------------------------------------->>>>>>
// Save file with defined number of surface elements
MatrixXd DataSelection::SaveFile(string fichierSave,  MatrixXd const& Data,  MatrixXd const& Criteria_final, 
	vector<int> indice, string & FileName, vector<double> & FinalCriForImages)
{
	/* This function is used to save selected data that will be used
	to estimate Lever arms

	Input data:
	- fichierSave, output file name
	- Data, data to save
	- Criteria_final, final criteria for lever arms
	- indice, index representing soundings for each survey line
	- FinalCriForImages, finalcri associated to selected surface elements
	
	Output data:
	- DataForBorEst, selected soundings with associated raw data
	*/

	// Open file
	string FileNameWrite = FileName + "\\" + fichierSave + ".txt";
	ofstream fichier_saving(FileNameWrite, ios::out | ios::trunc);

	// saving data in a text file
	double IndFile = 1, IndPatch = 1;

	// Mean of slope
	double MeanSlope = Criteria_final.col(2).sum()/Criteria_final.rows();

	// FinalCri computation
	vector <double> FinalCri, FinalCriM, FinalCriL;
	vector <int> indMore, indLess;
	for (int i = 0; i < Criteria_final.rows(); i++)
	{
		// Extract index and final criterion of surface elements with slope greater or less than meanSlope
		if (Criteria_final(i,2) > MeanSlope) {
			indMore.push_back(i);
			FinalCriM.push_back((Criteria_final(i,0)/Criteria_final.col(0).maxCoeff()) + 
		(Criteria_final(i,1)/Criteria_final.col(1).maxCoeff()));	
		}
		else {
			indLess.push_back(i);
			FinalCriL.push_back((Criteria_final(i,0)/Criteria_final.col(0).maxCoeff()) + 
		(Criteria_final(i,1)/Criteria_final.col(1).maxCoeff()));
		}

		// Final cri related to ax, ay computation
		FinalCri.push_back((Criteria_final(i,0)/Criteria_final.col(0).maxCoeff()) + 
		(Criteria_final(i,1)/Criteria_final.col(1).maxCoeff()));
	}

	// Select the number of surface element to use 
	// (should be less than min of size(indMore) and size(indLess))
	int NumberPatch;
	do
	{
		cout << "Number of surface elements. The maximum is: " << min(indLess.size(), indMore.size()) << endl;
		cin >> NumberPatch;
	}while(NumberPatch>min(indLess.size(), indMore.size())); cout << endl;

	// Select the number of ping (for high pitch and roll) per line
	int nbrPing;
	do 
	{
	cout << "Number of used pings per line for each patch: " << endl;
	cin >> nbrPing;
	}while(nbrPing <= 0); cout << endl;

	// Sort FinalCriM and extract index associated
	std::vector<int> IndexSelectedFinalCriMore(FinalCriM.size());
	std::iota(IndexSelectedFinalCriMore.begin(), IndexSelectedFinalCriMore.end(), 0);
	auto comparatorMore = [&FinalCriM](int a, int b){ return FinalCriM[a] > FinalCriM[b]; };
	std::sort(IndexSelectedFinalCriMore.begin(), IndexSelectedFinalCriMore.end(), comparatorMore);

	// Sort FinalCriL and extract index associated
	std::vector<int> IndexSelectedFinalCriLess(FinalCriL.size());
	std::iota(IndexSelectedFinalCriLess.begin(), IndexSelectedFinalCriLess.end(), 0);
	auto comparatorLess = [&FinalCriL](int a, int b){ return FinalCriL[a] > FinalCriL[b]; };
	std::sort(IndexSelectedFinalCriLess.begin(), IndexSelectedFinalCriLess.end(), comparatorLess);

	// Construct initial index corresponding sorted FinalCriM
	vector<int> IniIndFinalCriM;
	for(int i =0; i<FinalCriM.size(); i++)
	{
		int ind = IndexSelectedFinalCriMore[i];
		IniIndFinalCriM.push_back(indMore[ind]);
	}

	// Construct initial index corresponding sorted FinalCriL
	vector<int> IniIndFinalCriL;
	for(int i =0; i<FinalCriL.size(); i++)
	{
		int ind = IndexSelectedFinalCriLess[i];
		IniIndFinalCriL.push_back(indLess[ind]);
	}

	// Concatenate indMore and indLess
	vector<vector<int>> indSelectedMoreLess;
	indSelectedMoreLess.push_back(IniIndFinalCriM); indSelectedMoreLess.push_back(IniIndFinalCriL);

	// Initialization
	// Data for lever arms estimation
	int counterSounding = 0;
	MatrixXd DataForBorEst(1000, 25);

	// Initial index corresponding to sort FinalCri
	vector<int> IndexSelectedFinalCri;

	// Loop on each group of orientation (less and more than meanSlope)
	for (int typeSlope = 0; typeSlope < 2; typeSlope++)
	{
		// Extract index for meanSlope More or less slope patches
		IndexSelectedFinalCri = indSelectedMoreLess[typeSlope];

		// Loop on the number of selected patch
		for(int sel=0; sel<NumberPatch; sel++)
		{
			int i = IndexSelectedFinalCri[sel];

			// Specific surface element parameters extraction
			double Caxi = Criteria_final(i,0), Cayi = Criteria_final(i,1), Cazi = Criteria_final(i,2);
			double FinalCrii = FinalCri[i];
			double Emin_fi = Criteria_final(i,3), Emax_fi = Criteria_final(i,4);
			double Nmin_fi = Criteria_final(i,5), Nmax_fi = Criteria_final(i,6);
			double EmNm_fi = Criteria_final(i,7);
			double a_fi = -Criteria_final(i,9), b_fi = -Criteria_final(i,8), c_fi = -Criteria_final(i,10); // Adapt a, b, c in NED.

			// Loop on the number of survey lines
			for (int h = 0; h < indice.size() - 1; h++)
			{
				// index of Entire soundings
				vector <int> final_IndiceEntire = SoundingsInPatchQuad(Emin_fi, Emax_fi, Nmin_fi, Nmax_fi, Data, h, indice);

				// index of soundings with high roll and high pitch
				vector <int> final_Indice = PingsWith_HighAttitude(final_IndiceEntire, Data, nbrPing);

				// Save data
				if ( final_Indice.size() > 0)
				{
					// Loop on the number of soundings of survey line h from the patch sel
					for (int j = 0; j < final_Indice.size(); j++)
					{
						// save data in file in NED configuration
						fichier_saving  << std::fixed << std::setprecision(8) << Data(final_Indice[j],0)  
							<< "   " <<  Data(final_Indice[j],1)   << "   " <<  Data(final_Indice[j],2) 
							<< "   " <<  Data(final_Indice[j],15) 
							<< "   " <<  Data(final_Indice[j],9)   << "   " <<  Data(final_Indice[j],10) 
							<< "   " <<  Data(final_Indice[j],11)
							<< "   " <<  Data(final_Indice[j],13)  << "   " <<  Data(final_Indice[j],12) 
							<< "   " << -Data(final_Indice[j],14)  << "   " <<  Data(final_Indice[j],16) 
							<< "   " <<  Data(final_Indice[j],17)
							<< "   " <<  Data(final_Indice[j],18)  << "   " <<  Data(final_Indice[j],19)
							<< "   " <<  Data(final_Indice[j],20)  << "   " <<  Data(final_Indice[j],21) 
							<< "   " <<  a_fi  << "   "          << b_fi  << "   " << c_fi      << "   " << Caxi
							<< "   " <<  Cayi << "   "          << Cazi << "   " << FinalCrii << "   " << IndFile 
							<< "   " <<  IndPatch << endl;

						// Save data in matrix
						DataForBorEst.block(counterSounding, 0, 1, 3) = Data.block(final_Indice[j], 0, 1, 3); 

						DataForBorEst(counterSounding,3) =  Data(final_Indice[j],15);

						DataForBorEst.block(counterSounding, 4, 1, 3) = Data.block(final_Indice[j], 9, 1, 3);

						// NED configuration
						DataForBorEst(counterSounding,7) =  Data(final_Indice[j],13);
						DataForBorEst(counterSounding,8) =  Data(final_Indice[j],12);
						DataForBorEst(counterSounding,9) = -Data(final_Indice[j],14);

						DataForBorEst.block(counterSounding, 10, 1, 6) = Data.block(final_Indice[j], 16, 1, 6);

						DataForBorEst(counterSounding,16) = a_fi; 
						DataForBorEst(counterSounding,17) = b_fi; 
						DataForBorEst(counterSounding,18) = c_fi;
						DataForBorEst(counterSounding,19) = Caxi; 
						DataForBorEst(counterSounding,20) = Cayi; 
						DataForBorEst(counterSounding,21) = Cazi;
						DataForBorEst(counterSounding,22) = FinalCrii; 
						DataForBorEst(counterSounding,23) = IndFile; 
						DataForBorEst(counterSounding,24) = IndPatch;

						// Save finalCri for images
						FinalCriForImages.push_back(FinalCrii);

						// update size
						counterSounding++;
						if (counterSounding%1000 == 0) 
						{DataForBorEst.conservativeResize(counterSounding+1000, 25);}

					}// end for j

				}// end if
				IndFile = IndFile + 1;

			}// end for h
			IndFile = 1;
			IndPatch++;

		}// end for sel

	}// end for typeSlope

	// close file
	fichier_saving.close(); 

	// Output
	DataForBorEst.conservativeResize(counterSounding, 25);
	return DataForBorEst;

}// end function SaveFile
// --------------------------------------------------------------------------->>>>>>



// --------------------------------------------------------------------------->>>>>>
// Images using matlab engine
int DataSelection::ImagesDataSelection(vector<double> FinalCri, vector<double> const& FinalCriForImages,
	                                    vector<double> a_f, vector<double> b_f, vector<double> c_f,
										vector<double> Emin_f, vector<double> Emax_f, vector<double> Nmin_f, vector<double> Nmax_f,
										MatrixXd const& Data, vector<int> indice)
{
	/* This function is used to display surface elements the most relevant for 
	lever arms estimation. It uses "Library Engine.h from Matlab". So we need to have a Matlab 
	licence to use this function. In this case it is run in 64 bits platform. 

	Input data:
	- FinalCri  sensitivity criteria associated lever arms in general
	- FinalCriForImages, associated criterion to selected surface elements
	- a_f, b_f, c_f plan parameters estimated for different surface elements
	- Emin_f, Emax_f, Nmin_f, Nmax_f, surface elements boundaries
	*/

	// sentivity criterion Initialization
	vector <double> Sensibility_sort; Sensibility_sort = FinalCri;
	vector <double> index_Sensibility;
	float Threshold = 0.5;

	// initialize indexes for Sensibility
	vector<int> Sensibility_index;
	for(size_t i = 0; i < Sensibility_sort.size(); ++i)
		Sensibility_index.push_back(i);

	std::sort(Sensibility_index.begin(), Sensibility_index.end(), IdxCompare(Sensibility_sort));
	vector <int> RealIndexSensibility;
	for (int i = 0; i < Sensibility_index.size(); i++)
		RealIndexSensibility.push_back(Sensibility_sort[Sensibility_index[i]]);

	std::vector<int> IndexSensibility;
	std::vector<int>::iterator iterSensibility;
	for (int i = 0; i < Sensibility_index.size(); i++)
	{
		iterSensibility = std::find(Sensibility_index.begin(), Sensibility_index.end(), i);
		size_t indSensibility = std::distance(Sensibility_index.begin(), iterSensibility);
		IndexSensibility.push_back(indSensibility);
	}

	// Engine pointer
	Engine *ep;
	mxArray *x = NULL, *result = NULL;
	mxArray *y = NULL;
	mxArray *x_g = NULL;
	mxArray *y_g = NULL;
	mxArray *Cphi_ind = NULL;
	mxArray *a_t = NULL;
	mxArray *b_t = NULL;
	mxArray *c_t = NULL;
	mxArray *Cphi_Size = NULL;

	if (!(ep = engOpen(""))) {
		fprintf(stderr, "\nCan't start MATLAB engine\n");
		return EXIT_FAILURE;
	}

	int wantPlot = 0;
	cout << "Enter 1 if you want data selection plot:  ";
	cin >> wantPlot; cout << endl;

	if (wantPlot == 1)
	{
		////////////////////////////////////////////////////////////////////////////////// Graphic of SENSITIVITY
		engEvalString(ep, "figure (4)");
		mxArray *Sensibility_ind = NULL;
		mxArray *Sensibility_Size = NULL;

		for (int i = 0; i < a_f.size(); i++)
		{
			char buffer[BUFSIZE+1];
			double AI[1] = {a_f[i]}, BI[1] = {b_f[i]}, CI[1] = {c_f[i]};
			double Sensibility_IND[1] = {IndexSensibility[i]}, Sensibility_SIZE[1] = {FinalCri.size()};
			double x1=Emin_f[i], x2=Emax_f[i], y1=Nmin_f[i], y2=Nmax_f[i];

			//MatrixXf X(3,3);
			vector <double> X;
			X.push_back(x1); X.push_back(x1 + (x2 - x1)/2); X.push_back(x2); X.push_back(x1);
			X.push_back(x1 + (x2 - x1)/2); X.push_back(x2); X.push_back(x1); X.push_back(x1 + (x2 - x1)/2); X.push_back(x2);
	
			//MatrixXf Y(3,3);
			vector <double> Y;
			Y.push_back(y1); Y.push_back(y1); Y.push_back(y1); Y.push_back(y1 + (y2 - y1)/2);
			Y.push_back(y1 + (y2 - y1)/2); Y.push_back(y1 + (y2 - y1)/2); Y.push_back(y2); Y.push_back(y2); Y.push_back(y2);

			vector <double> X_E, Y_E;
			X_E.push_back(x1); X_E.push_back(x2); X_E.push_back(x2); X_E.push_back(x1); X_E.push_back(x1);
			Y_E.push_back(y1); Y_E.push_back(y1); Y_E.push_back(y2); Y_E.push_back(y2); Y_E.push_back(y1);

			if (!(ep = engOpen(""))) {
				fprintf(stderr, "\nCan't start MATLAB engine\n");
				return EXIT_FAILURE;
			}
		
			x_g = mxCreateDoubleMatrix(3,3, mxREAL);
			memcpy(mxGetPr(x_g), X.data(), sizeof(double)*X.size());
			engPutVariable(ep, "x_g", x_g);

			y_g = mxCreateDoubleMatrix(3,3, mxREAL);
			memcpy(mxGetPr(y_g), Y.data(), sizeof(double)*Y.size());
			engPutVariable(ep, "y_g", y_g);

			x = mxCreateDoubleMatrix(1,5, mxREAL);
			memcpy(mxGetPr(x), X_E.data(), sizeof(double)*X_E.size());
			engPutVariable(ep, "x", x);

			y = mxCreateDoubleMatrix(1,5, mxREAL);
			memcpy(mxGetPr(y), Y_E.data(), sizeof(double)*Y_E.size());
			engPutVariable(ep, "y", y);

			a_t = mxCreateDoubleMatrix(1, 1, mxREAL);
			memcpy((void *)mxGetPr(a_t), (void *)AI, sizeof(AI));
			engPutVariable(ep, "a_t", a_t);

			b_t = mxCreateDoubleMatrix(1, 1, mxREAL);
			memcpy((void *)mxGetPr(b_t), (void *)BI, sizeof(BI));
			engPutVariable(ep, "b_t", b_t);

			c_t = mxCreateDoubleMatrix(1, 1, mxREAL);
			memcpy((void *)mxGetPr(c_t), (void *)CI, sizeof(CI));
			engPutVariable(ep, "c_t", c_t);

			Sensibility_ind = mxCreateDoubleMatrix(1, 1, mxREAL);
			memcpy((void *)mxGetPr(Sensibility_ind), (void *)Sensibility_IND, sizeof(Sensibility_IND));
			engPutVariable(ep, "Sensibility_ind", Sensibility_ind);

			Sensibility_Size = mxCreateDoubleMatrix(1, 1, mxREAL);
			memcpy((void *)mxGetPr(Sensibility_Size), (void *)Sensibility_SIZE, sizeof(Sensibility_SIZE));
			engPutVariable(ep, "Sensibility_Size", Sensibility_Size);

			engEvalString(ep, "z_g = a_t.*x_g + b_t.*y_g + c_t;");
			engEvalString(ep, "z = 0.*x + 0.*y + 0;");
			engEvalString(ep, "cmap = colormap(parula(Sensibility_Size)) ;");

			// Plot the result FinalCriForImages
			// if (FinalCri[i]>Threshold)
			if ( find(FinalCriForImages.begin(), FinalCriForImages.end(), FinalCri[i]) != FinalCriForImages.end() )
			{
				engEvalString(ep, "surf(x_g, y_g, z_g,'LineWidth',0.001,'FaceColor', cmap(Sensibility_ind+1,:,:), 'EdgeColor',  cmap(Sensibility_ind+1,:,:)), hold on, ;");	
			}
			else
				engEvalString(ep, "surf(x_g, y_g, z_g,'LineWidth',0.001, 'FaceColor','k'), hold on, ;");

			engEvalString(ep, "plot3(x, y, z, 'k') , hold on, ;");
			engEvalString(ep, "view([90,90,90]);");
		}
		engEvalString(ep, "title('Sensitivity criteria');");
		engEvalString(ep, "xlabel('Easting [m]');");
		engEvalString(ep, "ylabel('Northing [m]');");
		engEvalString(ep, "zlabel('Height [m]');");
		engEvalString(ep, "colorbar");
		engEvalString(ep, "colorbar('Ticks',[-15.5,-13,-10.5],'TickLabels',{'0','0.5','1'}),;");


		////////////////////////////////////////////////////////////////////////////////// Graphic of Survey lines
		vector <double> EastingLine, NorthingLine;
		int indi = 1;
		for (int i=0; i<Data.rows();i++)
		{
			EastingLine.push_back(Data(i, 12));
			NorthingLine.push_back(Data(i, 13));

			// if new line
			if (i==(indice[indi]-1))
			{
				x = mxCreateDoubleMatrix(1,EastingLine.size(), mxREAL);
				memcpy(mxGetPr(x), EastingLine.data(), sizeof(double)*EastingLine.size());
				engPutVariable(ep, "x", x);

				y = mxCreateDoubleMatrix(1,NorthingLine.size(), mxREAL);
				memcpy(mxGetPr(y), NorthingLine.data(), sizeof(double)*NorthingLine.size());
				engPutVariable(ep, "y", y);

				engEvalString(ep, "z = 0.*x + 0.*y + 0;");
				engEvalString(ep, "plot3(x, y, z, '.', 'Color', rand(1,3)) , hold on, ;");

				// update
				indi++;
				EastingLine.clear();
				NorthingLine.clear();
			}
		}

	} // END IF WantPlot

}// end function ImagesDataSelection
// --------------------------------------------------------------------------->>>>>> 



// --------------------------------------------------------------------------->>>>>>
// Data selection
MatrixXd DataSelection::DataSelection_Results(MatrixXd const& Data, double PlanThreshold, 
											  vector<int> indice, string fichierSave, string & FileName)
{
	/* This function is used to select soundings that will be used to estimate
	lever arms, it reads data, apply Quadtree process, then Extract surface elements parameters 
	to finish it saves the most relevant patches soundings.

	Input data:
	- Data, raw data coming from reading data function
	- PlanThreshold, Threshold used for plan adjustment
	- Threshold, Threshold used for surface element sentivity criterion
	- NumberPatch, Number of patches selected to estimate lever arms 
	  (*2 for patches with slope greater and less than the slope average)
	- indice, index corresponding to soundings for each file
	*/

	//// Initial Boundaries of DTM:
	double Emin = Data.col(3).minCoeff();
	double Emax = Data.col(3).maxCoeff();
	double Nmin = Data.col(4).minCoeff();
	double Nmax = Data.col(4).maxCoeff();
	vector <double> Resultat;
	// cout << "Emin: " << Emin << " Emax: " << Emax << " Nmin: " << Nmin << " Nmax: " << Nmax;

	// QuadTree function call
	cout << "QUADTREE -------------------->:" << endl << endl;
	int NbrPoints2Valid = 50;
	vector <double> Result_quadtree = QuadTree(Emin, Emax, Nmin, Nmax, NbrPoints2Valid, Data, indice, PlanThreshold, Resultat);


	//// Saved the criteria, the boundaries and the plan parameters.
	cout << "CRITERIA -------------------->:" << endl << endl;
	vector <double> indice_kdtree;
	MatrixXd SelectedData = CriteriaExtraction(fichierSave, FileName, Result_quadtree, indice_kdtree, Data, indice);

	// Output
	return SelectedData;

}// end function DataSelection_Results
// --------------------------------------------------------------------------->>>>>>