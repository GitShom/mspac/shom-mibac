/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run LAAC (Lever Arms Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

/*/-------------------------------------------------------------------------------------/*/
// LAAC (Lever Arms Automatic Calibration)
//
// Class                  :  .cpp (Georeferencing)
// Authors                :  Rabine Keyetieu, Gaël Roué                  
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/08/12
//--------------------------------------------------------------------------------------/*/


// --------------------------------------------------------------------------->>>>>>>>>>>>>>>> LIBS:
// Class
#include <direct.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <math.h> 
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <Eigen/Dense>
#include "spline.h"
#include "Georeferencing.h"


// Using name space
using namespace std;
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>


// --------------------------------------------------------------------------->>>>>> Methods definition
// --------------------------------------------------------------------------->>>>>>
// Conctructor
Georeferencing::Georeferencing(void)
{
	m_LAAC_Georeferencing = "Georeferencing";
} // end constructor
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Get current directory
string Georeferencing::current_working_directory()
{
	/* This function is used to get current directory
	*/

	char* cwd = _getcwd( 0, 0 ) ; // **** microsoft specific ****
	string working_directory(cwd) ;
	free(cwd) ;
	return working_directory ;

}// end function current_working_directory
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Heading file reading
MatrixXd Georeferencing::readTextFileHeading(string fileName)
{
	/* This function is used to read Heading file

	Input data:
	- filename, heading file name

	Output data:
	- HeadingData, heading data
	*/

	// Initialization
	MatrixXd HeadingData(1000,2);
	int i=0;

	// Open file
	ifstream file;
	file.open(fileName);

	if (file)
	{
		// File reading
		for(string line; std::getline(file, line); )
		{
			istringstream in(line);    
			double tps, heading;

			in >> tps >> heading;
			HeadingData(i,0)=tps;
			HeadingData(i,1)=heading;
			i++;

			// Increase data size
			if (i%1000 == 0)
			{
				HeadingData.conservativeResize(i + 1000, 2);
			}

		}// end for
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output
	HeadingData.conservativeResize(i,2);
	return HeadingData;

}// end function readTextFileHeading
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Pitch Roll file reading
MatrixXd Georeferencing::readTextFilePitchRoll(string fileName)
{
	/* This function is used to read pitch roll file

	Input data:
	- filename, pitch/roll file name

	Output data:
	- PitchRollData, Pitch/Roll data
	*/

	// Initialization
	MatrixXd PitchRollData(1000,3);
	int i=0;

	// Open file
	ifstream file;
	file.open(fileName);

	if (file)
	{
		// File reading
		for(string line; std::getline(file, line); )
		{
			istringstream in(line);    
			double tps, pitch, roll;

			in >> tps >> pitch >> roll;
			PitchRollData(i,0)=tps;
			PitchRollData(i,1)=pitch;
			PitchRollData(i,2)=roll;
			i++;

			// Increase data size
			if (i%1000 == 0)
			{
				PitchRollData.conservativeResize(i + 1000, 3);
			}

		}// end for
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output
	PitchRollData.conservativeResize(i,3);
	return PitchRollData;

}// end function readTextFilePitchRoll
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Position file reading
MatrixXd Georeferencing::readTextFilePosition(string fileName)
{
	/* This function is used to read position file

	Input data:
	- filename, position file name

	Output data:
	- PositionData, Position data
	*/

	// Initialization
	MatrixXd PositionData(1000,4);
	int i=0;

	// Open file
	ifstream file;
	file.open(fileName);

	if (file)
	{
		// File reading
		for(string line; std::getline(file, line); )
		{
			istringstream in(line);    
			double tps, lat, lon, elH;

			in >> tps >> lat >> lon >> elH;
			PositionData(i,0)=tps;
			PositionData(i,1)=lat;
			PositionData(i,2)=lon;
			PositionData(i,3)=elH;
			i++;

			// Increase data size
			if (i%1000 == 0)
			{
				PositionData.conservativeResize(i + 1000, 4);
			}

		}// end for
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output
	PositionData.conservativeResize(i,4);
	return PositionData;

}// end function readTextFilePosition
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// MBES data reading
MatrixXd Georeferencing::Reading_MBES_Data(string fileNameMBES)
{
	/* This function is used to read MBES file

	Input data:
	- filename, MBES file name

	Output data:
	- MBESData, MBES data
	*/

	// Initalization
	MatrixXd MBESData;

	// Open files
	ifstream file;
	file.open(fileNameMBES);

	if (file)
	{

		// Extract the maximum of columns (maximum of soundings)
		VectorXd First_3_Col(1000);
		int i=0;
		for(string line; std::getline(file, line); )
		{
			double tps, SSS, NbrBeams;
			istringstream in(line);
			in >> tps >> SSS >> NbrBeams;
			First_3_Col(i) = NbrBeams;
			i++;

			if (i%1000 == 0)
			{
				First_3_Col.conservativeResize(i+1000);
			}
		}
		First_3_Col.conservativeResize(i);

		// Initialization of MBES data
		int MaxNumberBeams = static_cast<int>(First_3_Col.maxCoeff());
		int NumberOfColumns = MaxNumberBeams*3 + 3;
		long NumberOfPings = First_3_Col.rows();
		MBESData = MatrixXd::Constant(NumberOfPings, NumberOfColumns, -10000000);

		// Reading of data line by line
		file.close();
		ifstream file2(fileNameMBES);
		i=0;
		for(string line; std::getline(file2, line); )
		{
			istringstream in(line);
			for (int j = 0; j < (First_3_Col(i)*3+3); j++) 
			{
				double x;
				in >> x;
				MBESData(i,j)= x;
			}
			i++;
		}
	}
	else
	{
		cout << "File reading error" << endl;
	}

	// Output
	return MBESData;

}// end function Reading_MBES_Data
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// SVP reading 
MatrixXd Georeferencing::readTextFileSVP(string fileName, double draft)
{

	/* This function is used to read SVP file and adapt
	with draft

	Input data: 
	- fileName, SVP file name 

	Output data:
	- ProfilC_Out, Sound velocity and depth
	*/

	// Initialization
	MatrixXd ProfilC_Out;
	MatrixXd ProfilC(1000,2);

	// Open file
	ifstream file;
	string line;
	file.open(fileName);

	//// Reading
	//read stream line by line
	int i = 0;
	for(string line; getline(file, line); )   
	{
		//make a stream for the line itself
		istringstream in(line);				  

		//now read the whitespace-separated floats
		double cProfil, zProfil;
		in >> zProfil >> cProfil;					      

		//fill the profil vector
		ProfilC(i,0)=cProfil;      
		ProfilC(i,1)=-zProfil;
		i++;
	}
	// keep just non empty elements
	ProfilC.conservativeResize(i,2);


	//// Adaptation with draft
	// Find index where depth is lower than the draft
	VectorXd ind(ProfilC.rows());
	int compteur = 0;
	for (int i=0 ; i<ProfilC.rows() ; i++)
	{
		if (ProfilC(i,1) <= draft)
		{
			ind(compteur) = i;
			compteur++;
		}
	}
	ind.conservativeResize(compteur);

	// add draft if the case in SVP data
	if (ind.rows()!=0)
	{
		// compute sound velcotity assocuated to draft
		const int last(ind(ind.rows()-1));
		double gi = (ProfilC(last+1,0) - ProfilC(last,0)) / (ProfilC(last+1,1) - ProfilC(last,1));
		double cDraft = ProfilC(last,0) + gi*(draft - ProfilC(last,1));

		// New profil with draft
		MatrixXd ProfilCnew(ProfilC.rows() - last, 2);
		ProfilCnew << cDraft, draft, ProfilC.bottomRows(ProfilC.rows()-(last+1));
		ProfilC_Out = ProfilCnew;
	}
	else
	{
		ProfilC_Out = ProfilC;
	}

	// Output 
	ProfilC_Out.col(1) = ProfilC_Out.col(1).array() - ProfilC_Out(0,1);
	return ProfilC_Out;

} // end function readTextFileSVP
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Metadata reading
Metadata Georeferencing::readTextFileMetadata(string fileName)
{
	/* This function is used to read Metadata file in NED
	convention

	Input data: 
	- fileName, Metadata file name 

	Output data:
	- Metadata_Out, 
	struct Metadata [CordTx, CordGNSS, PosHorAcc, PosVerAcc, PitchRollAcc, HeadingAcc, Draft, Boresight, NameDevice]
	*/

	// initialization
	Metadata Metadata_Out;
	VectorXd CordTx(3), CordGNSS(3), Boresight(3);
	string NameDevice;

	double Patch_Roll, Patch_Pitch, Patch_Heading;
	double PitchRollAcc, HeadingAcc;
	double PosHorAcc, PosVerAcc;
	double MBES_X, MBES_Y, MBES_Z;
	double AntX, AntY, AntZ;
	double Draft;

	const double Pi = PI;
	const double d2r = Pi/180;

	// Open file
	ifstream file;
	string line;
	file.open(fileName);

	/// Reading
	//read stream line by line
	for(string line; getline(file, line); )  
	{
		//make a stream for the line itself
		istringstream in(line);  

		string type;
		in >> type; 

		if (type=="MultibeamModel")
		{
			in >> NameDevice;
		}
		else if (type=="AntPositionOffsetX")
		{
			in >> AntX;
		}
		else if (type=="AntPositionOffsetY")
		{
			in >> AntY;
		}
		else if (type=="AntPositionOffsetZ")
		{
			in >> AntZ;
		}
		else if (type=="MBEOffsetX")
		{
			in >> MBES_X;
		}
		else if (type=="MBEOffsetY")
		{
			in >> MBES_Y;
		}
		else if (type=="MBEOffsetZ")
		{
			in >> MBES_Z;
		}
		else if (type=="MBEDraft")
		{
			in >> Draft;
		}
		else if (type=="PositionAccuracy")
		{
			in >> PosHorAcc;
			PosVerAcc = PosHorAcc*1.5;
		}
		else if (type=="PitchRollAccuracy")
		{
			in >> PitchRollAcc;
		}
		else if (type=="HeadingAccuracy")
		{
			in >> HeadingAcc;
		}
		else if (type=="RollAlignment")
		{
			in >> Patch_Roll;
		}
		else if (type=="PitchAlignment")
		{
			in >> Patch_Pitch;
		}
		else if (type=="HeadingAlignment")
		{
			in >> Patch_Heading;
		}

	} // end For

	// Transmitter, GNSS and boresight
	CordTx << MBES_Y, MBES_X, -MBES_Z;
	CordGNSS << AntY, AntX, -AntZ;
	Boresight << Patch_Roll*d2r, Patch_Pitch*d2r, Patch_Heading*d2r;

	// Output
	Metadata_Out.CordTx = CordTx;
	Metadata_Out.CordGNSS = CordGNSS;
	Metadata_Out.PatchBoresight = Boresight;

	Metadata_Out.PitchRollAcc = PitchRollAcc*d2r;
	Metadata_Out.HeadingAcc = HeadingAcc*d2r;

	Metadata_Out.PosHorAcc = PosHorAcc;
	Metadata_Out.PosVerAcc = PosVerAcc;

	Metadata_Out.Draft = Draft;
	Metadata_Out.ModelMBES = NameDevice;

	return Metadata_Out;

} // end function readTextFileMetadata
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Assembling raw data uncertainties
VectorXd Georeferencing::RawDataUncertainties(Metadata const& MetaData_Out)
{
	/* This function is used to cluster georeferencing soundings uncertainties

	Input data: 
	- MetaData_Out, soundings meta data

	Output data:
	- RawDataUncertainties_Out, raw data uncertainties
	*/

	// Initialization
	VectorXd RawDataUncertainties_Out(15);

	const double Pi = PI;
	const double d2r = Pi/180;

	// uncertainties
	RawDataUncertainties_Out << MetaData_Out.PosHorAcc, MetaData_Out.PosHorAcc, MetaData_Out.PosVerAcc,  // Pn (Pn1, Pn2, Pn3)
		MetaData_Out.PitchRollAcc, MetaData_Out.PitchRollAcc, MetaData_Out.HeadingAcc,                   // Attitude (phi, theta, psi)
		0.01*d2r, 0.01*d2r, 0.01*d2r,                                                                    // Boresight (dphi, dtheta, dpsi)
		0.01, 0.01*d2r, 0.01*d2r,                                                                        // rbs (rho, beta, alpha)
		0.01, 0.01, 0.01;                                                                                // Lever arms (ax, ay, az)

	// Output
	return RawDataUncertainties_Out;

}// end function RawDataUncertainties
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Average of longitude, latitude and ellipsoidal height of all the files for local frame
VectorXd Georeferencing::Mean_LatLonHeight(vector<string> const& fileNames)
{
	/* This function is used to compute average position

	Input data:
	- fileNames, position files name

	Output data:
	- Pos0, Average Position data
	*/

	// Initialization
	int NberOfFiles = fileNames.size();
	VectorXd LatTot(1000);
	VectorXd LonTot(1000);
	VectorXd HeightTot(1000);
	VectorXd Pos0(3);
	int c = 0;

	// Loop on each files
	for (int i=0; i<NberOfFiles; i++)
	{

		// file open
		string tmp = fileNames[i] + + "\\AntPosition.txt";
		ifstream file;
		file.open(tmp);

		if (file)
		{

			for(string line; std::getline(file, line); )
			{
				double tps, lat, lon, elH;
				istringstream in(line);
				in >> tps >> lat >> lon >> elH;
				LonTot(c)= lon;
				LatTot(c)= lat;
				HeightTot(c)= elH;
				c++;

				if (c%1000 == 0)
				{
					LonTot.conservativeResize(c+1000);
					LatTot.conservativeResize(c+1000);
					HeightTot.conservativeResize(c+1000);
				}

			} // end for line
			file.close();

		}// end if (file)
		else
		{
			cout << "File reading error !" << endl;
		}

	}// end for i

	LonTot.conservativeResize(c);
	LatTot.conservativeResize(c);
	HeightTot.conservativeResize(c);

	// Average computation: Pos0 is [Long0, Lat0, Height0]
	double Pi = PI; double d2r = Pi/180;
	Pos0(0) = LonTot.mean()*d2r;
	Pos0(1) = LatTot.mean()*d2r;
	Pos0(2) = HeightTot.mean();

	// Output
	return Pos0;

} // end function Mean_LatLonHeight
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Keep only data collected before the end of "val"
MatrixXd Georeferencing::find_sup(VectorXd const& Time2modify, VectorXd const& data2modify, double val, vector<int>& ind, int taille)
{
	/* This function is used keeping only data collected before a definied time (val)

	Input data:
	- Time2modify, initial time of data to modify
	- data2modify, data collected
	- val, referene time 
	- ind, index of soundings satisfying the condition,
	- taille, to synchronize data index when deleting

	Output data:
	- TimeData, new time and data deleted from soundings not satisfying the condition
	*/

	// Initialization
	VectorXd NewTime(Time2modify.rows());
	VectorXd NewData(Time2modify.rows());
	int c=0;

	// Loop on each value of pitch roll
	for (int i=0; i<Time2modify.rows(); i++)
	{
		if (Time2modify(i) <= val)
		{
			NewTime(c) = Time2modify(i);
			NewData(c) = data2modify(i);
			c++;
		}

		else
		{
			ind.push_back(i+taille);
		}

	}// end for

	// resize data
	NewTime.conservativeResize(c);
	NewData.conservativeResize(c);

	// Output: New values
	MatrixXd TimeData(NewData.rows(), 2);
	TimeData.col(0) = NewTime;
	TimeData.col(1) = NewData;
	return TimeData;

}// end function find_sup
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Keep only data collected after the beginning of "val"
MatrixXd Georeferencing::find_inf(VectorXd const& Time2modify, VectorXd const& data2modify, double val, vector<int>& ind, int taille)
{
	/* This function is used keeping only data collected after a definied time (val)

	Input data:
	- Time2modify, initial time of data to modify
	- data2modify, data collected
	- val, referene time 
	- ind, index of soundings satisfying the condition,
	- taille, to synchronize data index when deleting

	Output data:
	- TimeData, new time and data deleted from soundings not satisfying the condition
	*/

	// Initialization
	VectorXd NewTime(Time2modify.rows());
	VectorXd NewData(Time2modify.rows());
	int c=0;

	// Loop on each value of pitch roll
	for (int i=0; i<Time2modify.rows(); i++)
	{
		if (Time2modify(i) >= val)
		{
			NewTime(c) = Time2modify(i);
			NewData(c) = data2modify(i);
			c++;
		}

		else
		{
			ind.push_back(i+taille);
		}

	}// end for

	// resize data
	NewTime.conservativeResize(c);
	NewData.conservativeResize(c);

	// Output: New values
	MatrixXd TimeData(NewData.rows(), 2);
	TimeData.col(0) = NewTime;
	TimeData.col(1) = NewData;
	return TimeData;

}// end function find_inf
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Linear interpolation one point
double Georeferencing::interpolate_linear(double x, vector<pair<double, double>> & table)
{
	/* This function is used for interpolation

	Input data:
	- x, value from which we need the interpolation
	- table, reference data for interpolation

	Output data:
	- interpolated value
	*/

	// Assumes that "table" is sorted by .first
	// Check if x is out of bound
	if (x > table.back().first) return INF;
	if (x < table[0].first) return -INF;

	vector<pair<double, double> >::iterator it, it2;

	// INFINITY is defined in math.h in the glibc implementation
	it = lower_bound(table.begin(), table.end(), make_pair(x, -INF));

	// Corner case
	if (it == table.begin()) return it->second;
	it2 = it;
	--it2;
	return it2->second + (it->second - it2->second)*(x - it2->first)/(it->first - it2->first);

}// end function interpolate_linear 
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// linear interpolation vector
VectorXd Georeferencing::interpl(VectorXd const& X_input, VectorXd const& Y_input, VectorXd const& X_wanted)
{
	/* This function is used for interpolation

	Input data:
	- x, value from which we need the interpolation
	- X_input, Y_input, reference data for interpolation
	- X_wanted, data we need interpolation

	Output data:
	- Output, interpolated value vector
	*/

	// Initialization
	vector<pair<double, double> > table;
	VectorXd Output(X_wanted.rows());

	for (int i=0; i<X_input.rows(); i++)
	{
		table.push_back(make_pair(X_input(i),Y_input(i)));
	}

	// interpolation point by point
	for (int i=0; i<X_wanted.rows(); i++)
	{
		Output(i) = interpolate_linear(X_wanted(i), table);
	}

	// Output
	return Output;

}// end function interpl
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Remove a row from MatrixXd
void Georeferencing::removeRow(MatrixXd& matrix, long int rowToRemove)
{
	/* This function is used to remove a row from a MAtrixXd

	Input data:
	- matrix, a matrix we want to remove row
	- rowToRemove, row index we want to remove
	*/

	// Intialization
	long int numRows = matrix.rows()-1;
	long int numCols = matrix.cols();

	if( rowToRemove < numRows )
		matrix.block(rowToRemove,0,numRows-rowToRemove,numCols) = matrix.block(rowToRemove+1,0,numRows-rowToRemove,numCols);

	matrix.conservativeResize(numRows,numCols);

}// end function removeRow
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Matching all raw data
Cell_DataReading Georeferencing::MatchingByInterpolation(Cell_DataReading const& Input)
{
	/* This funcion is used to match data (synchronize)

	Input data:
	- Cell_DataReading, constituted of data to synchronize

	Output data:
	- Output, synchronize data
	*/

	//  Data
	// MBES time (Reference time)
	VectorXd TimeMBES2 = Input.TimeMBES;

	// Position
	VectorXd TimePos = Input.TimePos;
	VectorXd latitude = Input.latitude;
	VectorXd longitude = Input.longitude;
	VectorXd height = Input.height;

	// attitude
	VectorXd TimePitchRoll = Input.TimePitchRoll;
	VectorXd pitch = Input.pitch;
	VectorXd roll = Input.roll;
	VectorXd TimeHeading = Input.TimeHeading;
	VectorXd heading = Input.heading;

	// MBES Data
	MatrixXd MBESData = Input.MBES_Data;

	// Keep MBES ping which start after the beginning and end before auxiliary
	// data (position and attitude) to ensure the interpolation
	VectorXd TimeMBES = TimeMBES2.head(TimeMBES2.rows()-1);
	vector<int> ind(0);
	int taille(0);
	double tmp = max(TimePos(0),TimePitchRoll(0));
	MatrixXd TimeMbesModif = find_inf(TimeMBES, TimeMBES, tmp, ind, taille);
	TimeMBES = TimeMbesModif.col(0);

	taille = ind.size();
	tmp = min(TimePos(TimePos.rows()-1),TimePitchRoll(TimePitchRoll.rows()-1));
	TimeMbesModif = find_sup(TimeMBES, TimeMBES, tmp, ind, taille);
	TimeMBES = TimeMbesModif.col(0);

	// Matching position data
	VectorXd latitudeMatch = interpl(TimePos, latitude, TimeMBES);
	VectorXd longitudeMatch = interpl(TimePos, longitude, TimeMBES);
	VectorXd heightMatch = interpl(TimePos, height, TimeMBES);

	// Matching attitude data
	VectorXd pitchMatch = interpl(TimePitchRoll, pitch, TimeMBES);
	VectorXd rollMatch = interpl(TimePitchRoll, roll, TimeMBES);
	VectorXd headingMatch = interpl(TimePitchRoll, heading, TimeMBES);

	// index to delete
	VectorXd ind2(ind.size());
	for (int i=0;i<ind.size();i++) 
	{ 
		ind2(i) = ind[i]; 
	}

	for (int i=0;i<ind.size();i++) 
	{ 
		removeRow( MBESData, ind2(i) );
		ind2.array() = ind2.array() - 1;
	}
	removeRow(MBESData, MBESData.rows()-1);

	// Output
	Cell_DataReading Output;
	Output.latitude = latitudeMatch;
	Output.longitude = longitudeMatch;
	Output.height = heightMatch;
	Output.pitch = pitchMatch;
	Output.roll = rollMatch;
	Output.heading = headingMatch;
	Output.TimeMBES = TimeMBES;
	Output.Ping2Delete = ind2;
	Output.MBES_Data = MBESData;
	return Output;

}// end function MatchingByInterpolation
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Centered positioning data
MatrixXd Georeferencing::CenterGNSSData(VectorXd const& lat, VectorXd const& lon,
	VectorXd const& alt, double lat0, double lon0, double alt0)
{
	/* This function is used to compute centered position in navigation frame

	Input data:
	- lat, lon, alt, latitude, longitude and ellipsoidal height
	- lat0, lon0, alt0, average position data

	Output data:
	- Pn_t123C, centered position data in navigation frame
	*/

	// WGS84 ellipsoid Parameters
	double a = 6378137;
	double e = 0.081819190842622;

	// Positioning data
	// Transformation of lon, lat, alt to TRF
	VectorXd N, PTRF1, PTRF2, PTRF3;
	N = ((1- (e*lat.array().sin()).pow(2)).sqrt()); N = a*( N.array().inverse() );
	PTRF1 = (N.array() + alt.array())*(lat.array().cos())*(lon.array().cos());
	PTRF2 = (N.array() + alt.array())*(lat.array().cos())*(lon.array().sin());
	PTRF3 = (N.array()*(1-pow(e,2)) + alt.array())*lat.array().sin();

	// Transformation of GNSS data from TRF frame to Navigation frame (n)
	VectorXd Pn_t1, Pn_t2, Pn_t3;
	Pn_t1 = cos(lat0)*PTRF3 - sin(lat0)*sin(lon0)*PTRF2 - sin(lat0)*cos(lon0)*PTRF1;
	Pn_t2 = cos(lon0)*PTRF2 - sin(lon0)*PTRF1;
	Pn_t3 = -sin(lat0)*PTRF3 - cos(lat0)*sin(lon0)*PTRF2 - cos(lat0)*cos(lon0)*PTRF1;

	//Local frame center
	//Transformation of lon, lat, alt to TRF
	double N0, PTRF10, PTRF20, PTRF30;
	N0 = a/(sqrt(1 - pow(e*sin(lat0), 2)));
	PTRF10 = (N0+alt0)*cos(lat0)*cos(lon0);
	PTRF20 = (N0+alt0)*cos(lat0)*sin(lon0);
	PTRF30 = (N0*(1-pow(e,2)) + alt0)*sin(lat0);

	// Transformation of GNSS data from TRF frame to Navigation frame (n)
	double Pn_t10, Pn_t20, Pn_t30;
	Pn_t10 = cos(lat0)*PTRF30 - sin(lat0)*sin(lon0)*PTRF20 - sin(lat0)*cos(lon0)*PTRF10;
	Pn_t20 = cos(lon0)*PTRF20 - sin(lon0)*PTRF10;
	Pn_t30= -sin(lat0)*PTRF30 - cos(lat0)*sin(lon0)*PTRF20 - cos(lat0)*cos(lon0)*PTRF10;

	// Centered GNSS data
	VectorXd Pn_t1C, Pn_t2C, Pn_t3C;
	Pn_t1C = Pn_t1.array() - Pn_t10;
	Pn_t2C = Pn_t2.array() - Pn_t20;
	Pn_t3C = Pn_t3.array() - Pn_t30;

	// Output
	MatrixXd Pn_t123C(lat.rows(), 3);
	Pn_t123C << Pn_t1C, Pn_t2C, Pn_t3C;

	return Pn_t123C;

}// end function CenterGNSSData
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Cidco format file reading
MatrixXd Georeferencing::CidcoFormatFileRead(string folderData, VectorXd const& MeanPos)
{
	/*This function is used to read raw data from survey system 
	(formatted in CIDCO configuration)

	Input data:
	- folderData, location of data
	- MeanPos, average position data

	Output data:
	- CidcoFormatData, raw data
	*/

	//---------------------------------> files name
	string fileNameAntPos(folderData + "/AntPosition.txt"); 
	string fileNamePitchRoll(folderData + "/PitchRoll.txt");
	string fileNameHeading(folderData + "/Heading.txt"); 
	string fileNameMBES(folderData + "/Multibeam.txt"); 

	// Degree To Radian
	double Pi = PI; double d2r = Pi/180;

	//---------------------------------> Reading of different files
	// GNSS data
	MatrixXd AntPosData = readTextFilePosition(fileNameAntPos);
	VectorXd TimePos = AntPosData.col(0);
	VectorXd latitude = AntPosData.col(1) * d2r;
	VectorXd longitude = AntPosData.col(2) * d2r;
	VectorXd height = AntPosData.col(3);

	// Pitch roll data
	MatrixXd PitchRollData = readTextFilePitchRoll(fileNamePitchRoll);
	VectorXd TimePitchRoll = PitchRollData.col(0);
	VectorXd pitch = PitchRollData.col(1) * d2r;
	VectorXd roll = PitchRollData.col(2) * d2r;

	// Heading data
	MatrixXd HeadingData = readTextFileHeading(fileNameHeading);
	VectorXd TimeHeading  = HeadingData.col(0);
	VectorXd heading = HeadingData.col(1);
	for (int h=1; h<heading.rows(); h++)
	{
		if (heading(h)-heading(h-1) < -180.)  {heading(h) = heading(h) + 360. ;}
		else if (heading(h)-heading(h-1) > 180.)  { heading(h) = heading(h) - 360. ;}
	}
	heading = heading * d2r ;

	// MBES time is our reference time
	MatrixXd MBESData = Reading_MBES_Data(fileNameMBES);
	VectorXd TimeMBES = MBESData.col(0);

	// ---------------------------------> Synchronization of data
	// Synchronization of attitude data
	vector<int> indSyn; int tailleSyn = 0;
	MatrixXd TimeRoll = find_inf(TimePitchRoll, roll, TimeHeading(0), indSyn, tailleSyn);
	MatrixXd TimePitch = find_inf(TimePitchRoll, pitch, TimeHeading(0), indSyn, tailleSyn);
	MatrixXd TimeRoll2 = find_sup(TimeRoll.col(0), TimeRoll.col(1), TimeHeading(TimeHeading.rows()-1), indSyn, tailleSyn);
	MatrixXd TimePitch2 = find_sup(TimeRoll.col(0), TimePitch.col(1), TimeHeading(TimeHeading.rows()-1), indSyn, tailleSyn);
	roll = TimeRoll2.col(1);
	pitch = TimePitch2.col(1);
	TimePitchRoll = TimeRoll2.col(0);
	heading = interpl(TimeHeading, heading, TimePitchRoll);
	MatrixXd rawAttitude(heading.rows(),3);
	rawAttitude << roll,pitch,heading;

	// Matching of data by interpolation
	// allow to delete MBES data that do not coincide with others data, so not to have bad values when interpolating
	Cell_DataReading Data2Interpolate;
	Data2Interpolate.TimeMBES = TimeMBES;
	Data2Interpolate.TimePos = TimePos;
	Data2Interpolate.latitude = latitude;
	Data2Interpolate.longitude = longitude;
	Data2Interpolate.height = height;
	Data2Interpolate.TimePitchRoll = TimePitchRoll;
	Data2Interpolate.pitch = pitch;
	Data2Interpolate.roll = roll;
	Data2Interpolate.TimeHeading = TimeHeading;
	Data2Interpolate.heading = heading;
	Data2Interpolate.MBES_Data = MBESData;

	Cell_DataReading InterpolatedData = MatchingByInterpolation(Data2Interpolate);

	// matched Attitude data
	VectorXd pitchMatch = InterpolatedData.pitch;
	VectorXd rollMatch = InterpolatedData.roll;
	VectorXd headingMatch = InterpolatedData.heading;
	MatrixXd attitude(headingMatch.rows(),3);
	attitude << rollMatch, pitchMatch, headingMatch;

	//  matched position data
	VectorXd latitudeMatch = InterpolatedData.latitude;
	VectorXd longitudeMatch = InterpolatedData.longitude;
	VectorXd heightMatch = InterpolatedData.height;

	// Transformation from geographic coordinates(lon, lat, h) to local navigation frame NED (Pnt1, Pnt2, Pnt3)
	MatrixXd PGeo = CenterGNSSData(latitudeMatch, longitudeMatch, heightMatch, MeanPos(1), MeanPos(0), MeanPos(2));

	// MBES ref time and Ping to delete
	MBESData = InterpolatedData.MBES_Data;
	VectorXd TimeMBES_ref = InterpolatedData.TimeMBES;
	VectorXd Ping2Delete = InterpolatedData.Ping2Delete; 

	//Ping 
	VectorXd Ping(TimeMBES_ref.rows());
	for (int i=0; i<TimeMBES_ref.rows(); i++)
	{
		Ping(i)=i+1;
	}

	// Adapt data to data per beams
	VectorXd SSS = MBESData.col(1);
	VectorXd NberBeam = MBESData.col(2);

	long nbrElt = NberBeam.sum();

	VectorXd Beta(nbrElt,1);
	VectorXd Alpha(nbrElt,1);
	VectorXd TwoTravelTime(nbrElt,1);

	VectorXd Time_long(nbrElt,1);
	VectorXd Ping_long(nbrElt,1);
	VectorXd Index_Beam(nbrElt,1);
	VectorXd SSS_long(nbrElt,1);
	MatrixXd attitude_long(nbrElt,3);
	MatrixXd PGeo_long(nbrElt,3);

	long indMin;
	long indMax;
	int c = 0;

	for(int i=0; i<MBESData.rows(); i++)
	{
		indMin = NberBeam.topRows(i).sum() ;
		indMax = NberBeam.topRows(i+1).sum() - 1;

		for (long j=0;j<(indMax-indMin+1);j++)
		{
			// Extract MBES data
			TwoTravelTime(c) = MBESData(i,3*j+3);
			Alpha(c) = MBESData(i,3*j+4)*d2r;
			Beta(c) = MBESData(i,3*j+5)*d2r;
			c++;
		}

		int counterBeams = 1;
		for (long j=indMin;j<=indMax;j++)
		{
			// adapt attitude data
			attitude_long(j,0) = attitude(i,0);
			attitude_long(j,1) = attitude(i,1);
			attitude_long(j,2) = attitude(i,2);

			// adapt position data
			PGeo_long(j,0) = PGeo(i,0);
			PGeo_long(j,1) = PGeo(i,1);
			PGeo_long(j,2) = PGeo(i,2);

			// adapt other data
			Ping_long(j) = Ping(i);
			SSS_long(j) = SSS(i);
			Time_long(j) = TimeMBES_ref(i);
			Index_Beam(j)= counterBeams;
			counterBeams++;   
		}

	}// end for i

	// time corresponding to receive beams
	VectorXd TimeBasis = Time_long + TwoTravelTime;

	// Delete beams index whose time is greater for ensuring interpolation
	vector<int> ind2Remove;
	VectorXd TimeBasisInt = Time_long.array() + TwoTravelTime.array();
	MatrixXd TimeBasisNew = find_sup(TimeBasisInt, TimeBasisInt, TimePitchRoll(TimePitchRoll.rows()-1), ind2Remove, tailleSyn);
	TimeBasis = TimeBasisNew.col(0);

	//attitude data at received time:
	// NB: when using linear interpolation results seems ambiguous
	// Linear:
	VectorXd Attitude_Roll_interp = interpl(TimePitchRoll, rawAttitude.col(0), TimeBasis);
	VectorXd Attitude_Pitch_interp = interpl(TimePitchRoll, rawAttitude.col(1), TimeBasis);
	VectorXd Attitude_Yaw_interp = interpl(TimePitchRoll, rawAttitude.col(2), TimeBasis);
	// Spline:
	//VectorXd Attitude_Roll_interp = tk::Interp_spline(TimePitchRoll,rawAttitude.col(0), TimeBasis);
    //VectorXd Attitude_Pitch_interp = tk::Interp_spline(TimePitchRoll,rawAttitude.col(1), TimeBasis);
    //VectorXd Attitude_Yaw_interp = tk::Interp_spline(TimePitchRoll,rawAttitude.col(2), TimeBasis);
	// Storage
	MatrixXd AttitudeRecep(Attitude_Roll_interp.rows(),3);
	AttitudeRecep << Attitude_Roll_interp, Attitude_Pitch_interp, Attitude_Yaw_interp;

	// intermediate data
	MatrixXd CidcoFormatData_int(nbrElt, 7);
	CidcoFormatData_int << Time_long, Ping_long, Index_Beam, SSS_long, 
		TwoTravelTime/2.0, Alpha, Beta;

	// Remove data when appropriate
	if (ind2Remove.size() > 0)
	{
		VectorXd ind2Rem(ind2Remove.size());
		for (int i=0;i<ind2Remove.size();i++) 
		{ 
			ind2Rem(i) = ind2Remove[i]; 
		}

		for(int ii=0; ii<ind2Remove.size(); ii++)
		{
			removeRow( PGeo_long, ind2Rem(ii) );
			removeRow( attitude_long, ind2Rem(ii) );
			removeRow( CidcoFormatData_int, ind2Rem(ii) );
			ind2Rem.array() = ind2Rem.array() - 1;
		}
	}

	// Output
	MatrixXd CidcoFormatData(CidcoFormatData_int.rows(), 16);
	CidcoFormatData << CidcoFormatData_int, PGeo_long, attitude_long, AttitudeRecep;


	// Keep only data with alpha between -40 and 40
	int jj = 0;
	MatrixXd CidcoFormatData_New(CidcoFormatData.rows(), 16);
	for (int ii=0; ii<CidcoFormatData.rows(); ii++)
	{
		double Alpha_ii = CidcoFormatData(ii, 5);
		if (abs(Alpha_ii) < 30*d2r)
		{
			CidcoFormatData_New.row(jj) = CidcoFormatData.row(ii);
			jj++;
		}
	
	}
	CidcoFormatData_New.conservativeResize(jj, 16);


	return CidcoFormatData_New;

}// end function CidcoFormatFileRead
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// C_bi_n
MatrixXd Georeferencing::DCM12(double phi, double theta, double psi)
{
	/* This function is used to compute the DirectionCosine Matrix (DCM)
	from frame 1 to frame 2 (NED convention).

	Input data: 
	- phi, theta, psi, attitude angle of 1 wrt 2

	Output data:
	- DCM12_Out
	*/

	// Initialisation
	MatrixXd R1(3,3), R2(3,3), R3(3,3);
	MatrixXd C_bI_LGF_NED(3,3);

	// Rotation matrix Rx, Ry, Rz
	R1 << 1, 0, 0,
		0, cos(phi), -sin(phi),
		0, sin(phi), cos(phi);

	R2 << cos(theta), 0, sin(theta),
		0, 1, 0,
		-sin(theta), 0, cos(theta);

	R3 << cos(psi), -sin(psi), 0,
		sin(psi), cos(psi), 0,
		0, 0, 1;

	// Frame transformation matrix (1 -> 2) - NED convention
	C_bI_LGF_NED = R3*R2*R1;

	// Output
	return C_bI_LGF_NED;

} // end function DCM12
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// derivative DCM wrt phi, theta, psi : dCbin_(phi, theta, psi)
MatrixXd Georeferencing::derDCM_phiThetaPsi(double phi, double theta, double psi)
{
	/* This function is used to compute the derivative of the DirectionCosine Matrix (DCM)
	from frame 1 to frame 2 (NED convention) wrt phi, theta, psi.

	Input data: 
	- phi, theta, psi, attitude angle of 1 wrt 2

	Output data:
	- derDCM_phiThetaPsi_Out
	*/

	// Initialization
	double sR = sin(phi);
	double cR = cos(phi);
	double sP = sin(theta);
	double cP = cos(theta);
	double sH = sin(psi);
	double cH = cos(psi);

	MatrixXd derDCM_phi(3,3), derDCM_theta(3,3), derDCM_psi(3,3);

	// derivative wrt phi - NED convention
	derDCM_phi   <<    0, cR*cH*sP+sR*sH, -sR*cH*sP+cR*sH,
		0, cR*sH*sP-sR*cH, -sR*sH*sP-cR*cH,
		0, cR*cP, -sR*cP;

	// derivative wrt theta - NED convention
	derDCM_theta << -cH*sP, sR*cH*cP, cR*cH*cP,
		-sH*sP, sR*sH*cP, cR*sH*cP,
		-cP, -sR*sP, -cR*sP;

	// derivative wrt psi - NED convention
	derDCM_psi   << -sH*cP, -sR*sH*sP-cR*cH, sR*cH-cR*sH*sP,
		cH*cP, sR*cH*sP-cR*sH, cR*cH*sP+sR*sH,
		0, 0, 0;

	// Output
	MatrixXd derDCM_phiThetaPsi_Out(3, 9);
	derDCM_phiThetaPsi_Out << derDCM_phi, derDCM_theta, derDCM_psi;

	return derDCM_phiThetaPsi_Out;

}// end function derDCM_phiThetaPsi
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// TPU : soundings uncertainties
VectorXd Georeferencing::SoundingsUncertainties(VectorXd const& attitude, VectorXd const& attitude_Recep, 
	VectorXd const& SLGF, VectorXd const& Boresight, VectorXd const& a_bi,
	VectorXd const& SigRawData)
{
	/* This function is used to compute the soundings uncertainties, by Total Propagated Uncertainties
	approach using the following geolocation equation: Xn = Pn + Cbin[Cbsbi.rbs + abi]

	Input data: 
	- attitude, attitude_Recep respectively attitude data at transmission and reception time
	- SLGF, representing soundings coordinates wrt the sounder 
	acoustic center in n-frame
	- Boresight, boresight angle
	- a_bi, lever arms,
	- SigRawData, uncertainties on raw data.

	Output data:
	- SoundingsUncertainties_Out
	*/


	//Initialization
	double dphi = Boresight(0), dtheta = Boresight(1), dpsi = Boresight(2);
	double phi = attitude_Recep(0), theta = attitude(1), psi = attitude(2);

	MatrixXd derCbin, derCbin_phi(3,3),  derCbin_theta(3,3), derCbin_psi(3,3);
	MatrixXd derCbsbi, derCbsbi_dphi(3,3),  derCbsbi_dtheta(3,3), derCbsbi_dpsi(3,3);
	VectorXd derRbs_rho(3), derRbs_alpha(3), derRbs_beta(3);

	VectorXd derX_Pn1(3), derX_Pn2(3), derX_Pn3(3);
	VectorXd derX_phi, derX_theta, derX_psi;
	VectorXd  derX_dphi, derX_dtheta, derX_dpsi;
	VectorXd derX_dRho, derX_dBeta, derX_dAlpha;
	VectorXd derX_dax, derX_day, derX_daz;

	double Rho, Beta, Alpha;
	VectorXd r_bS;
	MatrixXd C_bI_LGF, C_LGF_bI, C_bS_bI, C_bI_bS;
	MatrixXd Jac(3, 15);
	VectorXd SoundingsUncertainties_Out(3);

	//// Partial Derivative of X_LGF as function of the Position
	derX_Pn1 << 1, 0, 0;
	derX_Pn2 << 0, 1, 0;
	derX_Pn3 << 0, 0, 1;


	//// Partial Derivative of Xn wrt attitude
	// Cbin and Cnbi
	C_bI_LGF = DCM12(phi,theta,psi);
	C_LGF_bI = C_bI_LGF.transpose();

	// Cbsbi and Cbibs
	C_bS_bI = DCM12(dphi,dtheta,dpsi);
	C_bI_bS = C_bS_bI.transpose();

	// r_bS Calculation
	r_bS = C_bI_bS*C_LGF_bI*SLGF;

	// derivative Cbin wrt attitude
	derCbin = derDCM_phiThetaPsi(phi,theta,psi);
	derCbin_phi = derCbin.middleCols<3>(0) ;
	derCbin_theta = derCbin.middleCols<3>(3);
	derCbin_psi = derCbin.middleCols<3>(6);

	// derivative of Xn wrt attitude
	derX_phi = derCbin_phi*(C_bS_bI*r_bS + a_bi);
	derX_theta = derCbin_theta*(C_bS_bI*r_bS + a_bi);
	derX_psi = derCbin_psi*(C_bS_bI*r_bS + a_bi);


	//// Partial Derivative of X_LGF wrt boresight angles;
	// derivative of Cbsbi wrt boresight angles
	derCbsbi = derDCM_phiThetaPsi(dphi,dtheta,dpsi);
	derCbsbi_dphi = derCbsbi.middleCols<3>(0) ;
	derCbsbi_dtheta = derCbsbi.middleCols<3>(3);
	derCbsbi_dpsi = derCbsbi.middleCols<3>(6);

	// derivative of Xn wrt boresight angles
	derX_dphi = C_bI_LGF*derCbsbi_dphi*r_bS;
	derX_dtheta = C_bI_LGF*derCbsbi_dtheta*r_bS;
	derX_dpsi = C_bI_LGF*derCbsbi_dpsi*r_bS;


	//// Partial Derivative of X_LGF as function of the r_bS
	// r_bS Parametrization
	Rho = r_bS.norm();
	Beta = asin(r_bS(0)/Rho);
	Alpha = asin(r_bS(1)/(Rho*cos(Beta)));
	double cA = cos(Alpha), sA = sin(Alpha), cB = cos(Beta), sB = sin(Beta);

	// derivative of rbs wrt rho, beta and alpha
	derRbs_rho << sB, cB*sA, cB*cA;
	derRbs_beta << Rho*cB, -Rho*sA*sB, -Rho*cA*sB;
	derRbs_alpha << 0, Rho*cA*cB, -Rho*sA*cB;

	// derivative of Xn wrt to rbs
	derX_dRho = C_bI_LGF*C_bS_bI*derRbs_rho;
	derX_dBeta = C_bI_LGF*C_bS_bI*derRbs_beta;
	derX_dAlpha = C_bI_LGF*C_bS_bI*derRbs_alpha;


	//// Partial Derivative of X_LGF as function of the Lever Arms
	derX_dax = C_bI_LGF.col(0);
	derX_day = C_bI_LGF.col(1);
	derX_daz = C_bI_LGF.col(2);

	//// Uncertainties computation : TPU
	// Jacobian matrix
	Jac << derX_Pn1, derX_Pn2, derX_Pn3, derX_phi, derX_theta, derX_psi, derX_dphi, derX_dtheta,
		derX_dpsi,  derX_dRho, derX_dBeta, derX_dAlpha,  derX_dax, derX_day, derX_daz;

	// Input raw data uncertainties
	MatrixXd stdRawData = SigRawData.asDiagonal();
	MatrixXd SIG = stdRawData.array().pow(2);

	// TPU
	MatrixXd VarianceXn;
	VarianceXn = Jac*SIG*Jac.transpose();

	// Output 
	SoundingsUncertainties_Out = (VarianceXn.diagonal().transpose()).array().sqrt();
	return SoundingsUncertainties_Out;

}// end function SoundingsUncertainties
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Merging data
VectorXd Georeferencing::MergingData(VectorXd const& Pn, VectorXd const& attitude, VectorXd const& SLGF, VectorXd const& a_bi)
{
	/* This function based on geolocation equation is used georeference soundings
	by merging data.

	Input data: 
	- Pn, survey reference point position in local navigation frame
	- attitude, roll, pitch and heading
	- SLGF, representing soundings coordinates wrt the sounder 
	acoustic center in n-frame
	- a_bi, lever arms i.e offset from PRP to the sounder acoustic center (transmit array)

	Output data:
	- MergingData_Out = [Xn, Yn, Zn] Georeferenced soundings in navigation frame
	[Xn, Yn, Zn]^T = Pn + Cbin[ Cbsbi.rbs + abi ] = Pn + rn + Cbin.abi
	*/

	// DCM from bi to LGF frame(n)
	double phi = attitude(0), theta = attitude(1), psi = attitude(2);
	MatrixXd C_bI_LGF_NED = DCM12(phi, theta, psi);

	// Output
	VectorXd MergingData_Out = Pn + SLGF + C_bI_LGF_NED*a_bi;
	return MergingData_Out;

} // end function MergingData
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Ray Tracing
VectorXd  Georeferencing::RayTracing(VectorXd const& sinAzCosAzBeta0, MatrixXd const& ProfilCel, double TravelTime)
{
	/* This function is used to correct ray propagation 
	from refraction

	Input data: 
	- sinAzCosAzBeta0,  azimut angle unit vector (intersection transmitted and received beams)
	- ProfilC, sound velocity profiler adapted from draft
	- TravelTime, Acoustic ray one way travel time

	Output data:
	- RayTracing_Out = [rn1, rn2, rn3] , representing soundings coordinates wrt the sounder 
	acoustic center in n-frame
	*/

	// Initialization
	double sinAz = sinAzCosAzBeta0(0), cosAz = sinAzCosAzBeta0(1), Beta0 = sinAzCosAzBeta0(2);
	VectorXd ProfilC = ProfilCel.col(0), ZC = ProfilCel.col(1);
	int N_SSP_values = ProfilC.size();
	VectorXd gn(N_SSP_values-1);

	double Xf = 0.0, Zf = 0.0, AtoS_N = 0.0, AtoS_E = 0.0, AtoS_D = 0.0;
	double Epsi, DT = 0.0, dtt = 0.0, xff = 0.0, zff = 0.0;
	double sinBnm1, sinBn, cosBnm1, cosBn, Rcn, DZ, DR,dtf, dxf, dzf;
	int N = 0;
	VectorXd RayTracing_Out(3);

	//Speed of Sound gradient in each layer
	for (int k=0; k<N_SSP_values-1; k++)
	{
		gn(k) = (ProfilC(k+1)- ProfilC(k))/(ZC(k+1)- ZC(k));
	}

	//constant parameter: Snell's law is applied in the 1st layer
	Epsi = cos(Beta0)/ProfilC(0);

	//Following the gradient celerity values
	while((DT + dtt)<=TravelTime && (N<N_SSP_values-1))
	{
		//update of angles
		sinBnm1 = sqrt(1 - pow(Epsi*ProfilC(N), 2));
		sinBn   = sqrt(1 - pow(Epsi*ProfilC(N+1), 2));

		cosBnm1 = Epsi*ProfilC(N);
		cosBn   = Epsi*ProfilC(N+1);

		if (gn(N)!=0.0)
		{
			// if not null gradient
			//Radius of curvature
			Rcn = 1./(Epsi*gn(N));

			//delta t, delta z and r for the layer N
			dtt = abs( (1./abs(gn(N)))*log( (ProfilC(N+1)/ProfilC(N))*( (1.0 + sinBnm1)/(1.0 + sinBn) ) ) );
			DZ = Rcn*(cosBn - cosBnm1);
			DR = Rcn*(sinBnm1 - sinBn);
		}
		else
		{
			//celerity gradient is zero so constant celerity in this layer
			//delta t, delta z and r for the layer N
			DZ = ZC(N+1) - ZC(N);
			dtt = DZ/(ProfilC(N)*sinBn);
			DR = cosBn*dtt*ProfilC(N);
		}

		//To ensure to work with the N-1 cumulated travel time
		if (DT + dtt <=  TravelTime)
		{
			N = N+1;
			xff = xff + DR;
			zff = zff + DZ;
			DT = DT + dtt;
		}

	}// end loop while

	// Last Layer Propagation
	dtf = TravelTime - DT;
	dxf = ProfilC(N)*dtf*cosBn;
	dzf = ProfilC(N)*dtf*sinBn;

	// Output variable computation
	Xf = xff + dxf;
	Zf = zff + dzf;
	AtoS_N = Xf*sinAz;
	AtoS_E = Xf*cosAz;
	AtoS_D = Zf;

	// Output of function
	RayTracing_Out << AtoS_N, AtoS_E, AtoS_D;
	return RayTracing_Out;

} // end function RayTracing
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Unit vector computation
VectorXd Georeferencing::UnitVectorComputation(VectorXd const& attitude, VectorXd const& attitude_Recp, 
	VectorXd const& Bor, double beta, double alpha)
{
	/* This function is used to compute the unit vector,
	representing the intersection between the transmitted and the received 
	beams. It is considered as an initial vector for refraction correction.
	We use roll at the reception, pitch and heading at the emission.

	Input data: 
	- attitude,  roll, pitch and heading at transmission time
	- attitude_Recp, roll, pitch and heading at reception time
	- Bor, droll, dpitch, dyaw boresight angles
	- beta, emitted steering angle (tilt angle)
	- alpha, received steering angle (beams angle)

	Output data:
	- UnitVector_Out, [sinAz, cosAz, Betan] stand for azimuth and vertical angle
	associated to the unit vector
	*/

	// Initialization
	double phi, theta, psi;
	double dphi, dtheta, dpsi;
	MatrixXd C_bI_LGF_NED(3,3), C_bS_bI_NED(3,3);
	VectorXd Ubs(3);
	VectorXd Un(3);
	double normVn;
	double sinAz, cosAz, Betan;
	VectorXd sinAzcosAzBetan(3);

	// Roll at reception, pitch and heading at emission
	phi = attitude_Recp(0);
	theta = attitude(1);
	psi = attitude(2);

	// Boresight angles
	dphi = Bor(0);
	dtheta = Bor(1);
	dpsi = Bor(2);

	// Launch vector in bS frame
	Ubs << sin(beta), 
		cos(beta)*sin(alpha), 
		cos(beta)*cos(alpha);

	// Frame transformation matrix (bI -> n) - NED convention
	C_bI_LGF_NED = DCM12(phi, theta, psi);

	// Frame transformation matrix (bS -> bI) - NED convention
	C_bS_bI_NED = DCM12(dphi, dtheta, dpsi);

	// Launch vector in n frame
	Un = C_bI_LGF_NED*C_bS_bI_NED*Ubs;

	// norm of Vn - radial part of Un
	normVn = sqrt(pow(Un(0), 2)  + pow(Un(1), 2));

	// Computation of Az and Betan in radian
	sinAz = Un(0)/normVn;
	cosAz = Un(1)/normVn;
	Betan = asin(Un(2));

	// Output
	sinAzcosAzBetan << sinAz, cosAz, Betan;
	return sinAzcosAzBetan;

} // end function UnitVectorComputation
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Global Georeferencing 
MatrixXd Georeferencing::georeferencing(MatrixXd const& RawData, Metadata const& MetaData_Out, MatrixXd const& ProfilC, 
	VectorXd const& SigRawData, string fileNameOut, VectorXd a_bi)
{
	/* This function is used to georeference soundings

	Input data: 
	- RawData, data read and matched from files

	Output data:
	- georeferencing_Out = Raw data + georeferencing
	*/
	
	// Lever arms
	VectorXd True_a_bi = MetaData_Out.CordTx - MetaData_Out.CordGNSS; 
	a_bi(2) = True_a_bi(2);

	// boresight
	VectorXd Boresight = MetaData_Out.PatchBoresight;

	const double Pi = PI;
	const double d2r = Pi/180;

	// Initialization
	int nCol = 22;
	MatrixXd georeferencing_Out(RawData.rows(), nCol);

	// file for writing
	string const nomFichierOut(fileNameOut);
	ofstream FileOut(nomFichierOut.c_str());

	// Loop on each sounding
	int indConserv = 0;
	for(int i = 0; i < RawData.rows(); i++)   
	{
		if (RawData(i,4) > 0.00001)
		{

			//now read the whitespace-separated floats
			double TpsPerBeams(RawData(i,0) - RawData(0,0));         
			double ping(RawData(i,1)),                       Index_Beam(RawData(i,2)),    SSS_long(RawData(i,3));
			double TravelTime(RawData(i,4)),                 Alpha(RawData(i,5)),         Beta(RawData(i,6));
			double Pn_t1(RawData(i,7)),                      Pn_t2(RawData(i,8)),         Pn_t3(RawData(i,9));
			double Roll(RawData(i,10)),                      Pitch(RawData(i,11)),        Yaw(RawData(i,12));
			double Roll_recep(RawData(i,13)),                Pitch_recep(RawData(i,14)),  Yaw_recep(RawData(i,15));

			// Unit vector computation
			VectorXd attitude(3), attitude_Recp(3);
			attitude << Roll, Pitch, Yaw;
			attitude_Recp << Roll_recep, Pitch_recep, Yaw_recep;
			VectorXd sinAzCosAzBetan = UnitVectorComputation(attitude, attitude_Recp, Boresight, Beta, Alpha);

			// Ray Tracing
			VectorXd SLGF = RayTracing(sinAzCosAzBetan, ProfilC,  TravelTime);

			// Data merging
			VectorXd Pn(3);
			Pn << Pn_t1, Pn_t2, Pn_t3;
			VectorXd XYZ_n = MergingData(Pn, attitude, SLGF, a_bi);

			// Uncertainties computation
			VectorXd sigXYZ_n = SoundingsUncertainties(attitude, attitude_Recp, SLGF, Boresight, a_bi, SigRawData);

			// fill data in ENU configuration
			georeferencing_Out.row(indConserv) << TpsPerBeams,  ping, Index_Beam, XYZ_n(1), XYZ_n(0), -XYZ_n(2), sigXYZ_n(1), sigXYZ_n(0), sigXYZ_n(2),
				Roll, Pitch, Yaw, Pn_t2, Pn_t1, -Pn_t3, SSS_long, Roll_recep, Pitch_recep, Yaw_recep,
				Alpha, Beta, TravelTime;
			indConserv++;


			// Writing output file in NED configuration
			FileOut << setprecision (12) << TpsPerBeams << "\t" << ping << "\t" << Index_Beam
				<< "\t" << XYZ_n(0) << "\t" << XYZ_n(1) << "\t" << XYZ_n(2) 
				<< "\t" << sigXYZ_n(0) << "\t" << sigXYZ_n(1) << "\t" << sigXYZ_n(2) 
				<< "\t" << SSS_long << "\t" << Roll << "\t" << Pitch << "\t" << Yaw 
				<< "\t" << Pn_t1 << "\t" << Pn_t2 << "\t" << Pn_t3 
				<< "\t" << Roll_recep << "\t" << Pitch_recep << "\t" << Yaw_recep 
				<< "\t" << Alpha << "\t" << Beta << "\t" << TravelTime << endl;

		}// end if

	}// end for i

	// Output
	georeferencing_Out.conservativeResize(indConserv, nCol);
	return georeferencing_Out;

} // end function georeferencing
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Georeferencing all files
MatrixXd Georeferencing::georeferencingAllFiles(vector<int> & indice, string & folderData,  string & dirString2nd, 
												string & dirString3rd, VectorXd a_bi, int itLAAC, int & nbrLines,
													vector<string> & fileNamesLines, vector<string> & fileNamesLinesOut)
{
	/* This function is used to georeference soundings*/

	// Initialization
	vector<MatrixXd> GeorefOut;

	if (itLAAC == 1)
	{
		// Create an output folder
		string NameProject;
		cout << "Please type the project name (Please each time AVOID SPACE IN NAMES): " << endl;
		cin >> NameProject; cout << endl;

		// parameters
		cout << "Please enter the data folder link: " << endl;
		folderData; 	cin >> folderData; cout << endl;

		// Number of used lines
		cout << "Please enter the number of lines to use: " << endl;
		cin >> nbrLines; cout << endl;

		// Provide current directory
		string curDir =  current_working_directory();

		// Create folder project
		string dirString = curDir + "\\" + NameProject;
		const char* dirChar = (char*)dirString.c_str(); 	    // Name of created directory in char
		mkdir(dirChar); 	                                    // Create directory in the current one

		// Create first part output folder
		string dirString1st = dirString + "/Export1stPart_" + NameProject;
		const char* dirChar1st = (char*)dirString1st.c_str(); 	// Name of created directory in char
		mkdir(dirChar1st); 	                                    // Create directory in the current one

		// Create second part output folder
		dirString2nd = dirString + "\\" + "Export2ndPart_" + NameProject;
		const char* dirChar2nd = (char*)dirString2nd.c_str(); 	// Name of created directory in char
		mkdir(dirChar2nd); 	                                    // Create directory in the current one

		// Create third part output folder
		dirString3rd = dirString + "\\" + "LAAC_Result_" + NameProject;
		const char* dirChar3rd = (char*)dirString3rd.c_str(); 	// Name of created directory in char
		mkdir(dirChar3rd); 	                                    // Create directory in the current one

		// Lines name 
		for(int i =0; i<nbrLines; i++)
		{
			cout << "Please enter the name of line: " << i << endl;
			string fileName_i; cin >> fileName_i; 
			fileNamesLines.push_back(folderData + "\\" + fileName_i);
			fileNamesLinesOut.push_back(dirString1st + "\\" + fileName_i + ".txt");
		}
		cout << endl;

	}// end if itLAAC

	// Mean Pos
	VectorXd MeanPos = Mean_LatLonHeight(fileNamesLines);
	// cout << "MeanPos: " << MeanPos << endl;

	// Reading metadata file
	string fileMetadata(folderData + "/MetaData.txt");          
	Metadata MetaData_Out = readTextFileMetadata(fileMetadata);

	// Reading SVP file
	double draft = MetaData_Out.Draft;                          
	string fileSVP(folderData + "/SVP.txt");
	MatrixXd ProfilC = readTextFileSVP(fileSVP, draft);

	// Uncertainties on raw data
	VectorXd SigRawData = RawDataUncertainties(MetaData_Out); 

	// Loop on each file
	int allTaille = 0;
	for (int ifile = 0; ifile < nbrLines; ifile++)
	{
		cout << "Georeferencing file: " << ifile << endl;

		// CIDCO format reading
		MatrixXd RawData = CidcoFormatFileRead(fileNamesLines[ifile], MeanPos);

		// georef + rawdata
		MatrixXd OutGeoref = georeferencing(RawData, MetaData_Out, ProfilC, SigRawData, fileNamesLinesOut[ifile], a_bi);

		// Save result
		GeorefOut.push_back(OutGeoref);

		// size of data 
		allTaille += OutGeoref.rows();
	}

	// Save data in specific format : all data in the same variable
	int nbrCol = GeorefOut[0].cols(); 	MatrixXd FinalDataOut(allTaille, nbrCol);
	int il = 0;
	for(int i = 0; i<nbrLines; i++)
	{
		FinalDataOut.block(il, 0, GeorefOut[i].rows(), nbrCol) = GeorefOut[i];
		il += GeorefOut[i].rows();
		indice.push_back(il);
	}

	// Output
	cout << endl;
	return FinalDataOut;

}// end function georeferencingAllFiles
// --------------------------------------------------------------------------->>>>>>