/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run LAAC (Lever Arms Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

/*/-------------------------------------------------------------------------------------/*/
// LAAC (Lever Arms Automatic Calibration)
//
// Authors                :  Rabine Keyetieu, Gaël Roué                  
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/11/28
//--------------------------------------------------------------------------------------/*/


// ------------------------------------------------------------------->>>>>>>>>>>>>>>> LIBS:
// Libs
#include <set>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include "LeverArmsEstimation.h"
#include "DataSelection.h"
#include "Georeferencing.h"

// namespace
using namespace std;
// ----------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>


// ------------------------------------------------------------------->>>>>>>>>>>>>>>> MAIN:
int main()
{
	// iNITIAL TIME
	time_t tstart, tend, tstart1, tend1; 
	tstart = time(0);

	// Choose where you want to start
	cout << "<------------ CIDCO: LAAC (Lever Arms Automatic Calibration) -----------------> " << endl;
	cout << " Type 1: if you want to create a new project... " << endl;
	cout << " Type 2: if you want to use an existing project and start from data selection step... " << endl;
	cout << " Type 3: if you want to use an existing project and start from estimation step... " << endl;
	int step; cin >> step; cout << endl << endl;

	// Initialize Lever arms
	cout << "Initial lever arms in metre" << endl;
	cout << "ax: "; double ax; cin >> ax;
	cout << "ay: "; double ay; cin >> ay;
	cout << endl;
	VectorXd a_bi(3); a_bi << ax, ay, 0;


	//#################################### Create a new project
	if (step==1)
	{
		// Initialize to iterate LAAC
		double ConvZone = 1;
		int itLAAC = 0;
		vector<int> indice1; indice1.push_back(0);
		string folderData, dirString2nd, dirString3rd;
		int nbrLines;
		vector<string> fileNamesLines; 
		vector<string> fileNamesLinesOut;

		// While the current estimation and the previous are not close
		// LAAC again
		while (ConvZone > 0.005)
		{

		// Information
		itLAAC++;
		cout << endl << "****************%%%%%%%%%%%%%%%%%%*************** Iteration of LAAC: " << itLAAC << endl;
		cout << "Initial Values of Lever arms (ax, ay, az) in metre:  " << endl << a_bi << endl << endl;


		/////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////-LAAC 1st, 2nd, 3rd PARTS

		//***********************************************************************************//
		////////////////////////////////////////////////////////////////////-1st Georeferencing
		tstart1 = time(0);
		cout << "-----------------GEOREFERENCING MODULE ------------------------------------" << endl;
		// -------------------------->>>>>>>>>>>>>>>> SET UP:
		// set display precision
		cout << fixed;
		cout << setprecision(7);
		// ---------------------------------->>>>>>>>>>>>>>>>

		// -------------------------->>>>>>>>>>>>>>>> 1st- Georeferencing:
		// Class intantiation
		Georeferencing Georef;

		// Georeferencing all files
		MatrixXd Data2_1st = Georef.georeferencingAllFiles(indice1, folderData, dirString2nd, dirString3rd, a_bi, itLAAC,
															nbrLines, fileNamesLines, fileNamesLinesOut);
		// ---------------------------------->>>>>>>>>>>>>>>>
		tend1 = time(0); 
		cout << endl << "Georeferencing took "<< difftime(tend1, tstart1) <<" second(s)."<< endl << endl;

		//***********************************************************************************//
		////////////////////////////////////////////////////////////////////////-2nd Selection
		tstart1 = time(0);
		cout << "----------------- DATA SELECTION MODULE ------------------------------------" << endl;
		// -------------------------->>>>>>>>>>>>>>>> Parameters:
		std::fixed;
		double PlanThreshold = 1.96; //2.58
		string fichierSave("Raw_Data_LeverArms");
		// ---------------------------------->>>>>>>>>>>>>>>>

		// -------------------------->>>>>>>>>>>>>>>> 2nd part selection
		// Class instantiation:
		DataSelection LeverArmsSelection;

		// Data selection results
		MatrixXd SelectedData2 = LeverArmsSelection.DataSelection_Results(Data2_1st, PlanThreshold, indice1, fichierSave,
			dirString2nd);
		// ---------------------------------->>>>>>>>>>>>>>>>
		tend1 = time(0); 
		cout << endl << "Data selection took "<< difftime(tend1, tstart1) <<" second(s)."<< endl << endl;


		//***********************************************************************************//
		////////////////////////////////////////////////////////////////////////-3rd Estimation
		tstart1 = time(0);
		cout << "------------------- ESTIMATION MODULE ---------------------------------------" << endl;
		// Parameters
		double probability = 0.95;
		// ---------------------------------->>>>>>>>>>>>>>>>

		// -------------------------->>>>>>>>>>>>>>>> FOLDER :
		// Data folder
		time_t rawtime;  time (&rawtime); string TimeStr = ctime (&rawtime);
		replace(TimeStr.begin(), TimeStr.end(), ' ', '_'); replace(TimeStr.begin(), TimeStr.end(), '\n', '_'); 
		replace(TimeStr.begin(), TimeStr.end(), ':', '-'); 
		string fileName = TimeStr +  "LAAC_Results";
		// ---------------------------------->>>>>>>>>>>>>>>>

		// ------------------>>>>>>>>>>> 3RD PART ESTIMATION :
		// 3rd Estimation class instantiation 
		cout << "Lever arms Estimation ----------------->:" << endl;
		LeverArmsEstimation LeverArmsEstimate;

		// Results estimation
		VectorXd ResLAACclass = LeverArmsEstimate.LAAC_results(SelectedData2, probability, fileName, dirString3rd, folderData, a_bi);
		// ---------------------------------->>>>>>>>>>>>>>>>
		tend1 = time(0); 
		cout << endl << "Estimation took "<< difftime(tend1, tstart1) <<" second(s)."<< endl << endl;

		// New values of lever arms
		VectorXd NewA_bi(3); NewA_bi << ResLAACclass(0), ResLAACclass(1), 0;

		// Computation of threshold to stop LAAC
		ConvZone = ((NewA_bi - a_bi).norm());

		// Update
		a_bi = NewA_bi;
		cout << "New Values of Lever arms (ax, ay, az) in metre:  " << endl << a_bi << endl << endl;
		cout << "Difference between current and previous results: " << ConvZone << endl;

		// Update the variable indice
		indice1.clear(); indice1.push_back(0);

		}// end while
	}


	//#################################### Project already exist and start to the selection step
	else if(step==2)
	{
		//***********************************************************************************//
		////////////////////////////////////////////////////////////////////-2nd Selection
		tstart1 = time(0);
		cout << "----------------- DATA SELECTION MODULE ------------------------------------" << endl;
		// -------------------------->>>>>>>>>>>>>>>> Parameters:
		std::fixed;
		double PlanThreshold = 1.96;
		string fichierSave("Raw_Data_LeverArms");
		// ---------------------------------->>>>>>>>>>>>>>>>

		// -------------------------->>>>>>>>>>>>>>>> 2nd part selection
		// Class instantiation:
		DataSelection LeverArmsSelection;

		// reading of data
		cout << "DATA READING ----------------->:" << endl;
		vector<int> indice1; indice1.push_back(0);
		string folderData, dirString2ndPart, dirString3rdPart;
		MatrixXd Data2 = LeverArmsSelection.ReadingData_DataSelection(indice1, folderData, dirString2ndPart, dirString3rdPart);
		cout << endl;

		// Data selection results
		MatrixXd SelectedData2 = LeverArmsSelection.DataSelection_Results(Data2, PlanThreshold, indice1, fichierSave,
			dirString2ndPart);
		// ---------------------------------->>>>>>>>>>>>>>>>
		tend1 = time(0); 
		cout << endl << "Data selection took "<< difftime(tend1, tstart1) <<" second(s)."<< endl << endl;


		//***********************************************************************************//
		////////////////////////////////////////////////////////////////////////-3rd Estimation
		tstart1 = time(0);
		cout << "------------------- ESTIMATION MODULE ---------------------------------------" << endl;
		// Parameters
		double probability = 0.95;
		// ---------------------------------->>>>>>>>>>>>>>>>

		// -------------------------->>>>>>>>>>>>>>>> FOLDER :
		// Data folder
		time_t rawtime;  time (&rawtime); string TimeStr = ctime (&rawtime);
		replace(TimeStr.begin(), TimeStr.end(), ' ', '_'); replace(TimeStr.begin(), TimeStr.end(), '\n', '_'); 
		replace(TimeStr.begin(), TimeStr.end(), ':', '-'); 
		string fileName = TimeStr +  "LAAC_Results";
		// ---------------------------------->>>>>>>>>>>>>>>>

		// ------------------>>>>>>>>>>> 3RD PART ESTIMATION :
		// 3rd Estimation class instantiation 
		cout << "Lever arms Estimation ----------------->:" << endl;
		LeverArmsEstimation LeverArmsEstimate;

		// Results estimation
		VectorXd ResLAACclass = LeverArmsEstimate.LAAC_results(SelectedData2, probability, fileName, dirString3rdPart, folderData, a_bi);
		// ---------------------------------->>>>>>>>>>>>>>>>
		tend1 = time(0); 
		cout << endl << "Estimation took "<< difftime(tend1, tstart1) <<" second(s)."<< endl << endl;
	}


	//#################################### Project already exist and start to the estimation step
	else if(step==3)
	{
		//***********************************************************************************//
		////////////////////////////////////////////////////////////////////////-3rd Estimation
		tstart1 = time(0);
		cout << "------------------- ESTIMATION MODULE ---------------------------------------" << endl;
		// Parameters
		double probability = 0.95;
		// ---------------------------------->>>>>>>>>>>>>>>>

		// -------------------------->>>>>>>>>>>>>>>> File name :
		// Data folder
	    time_t rawtime;  time (&rawtime); string TimeStr = ctime (&rawtime);
		replace(TimeStr.begin(), TimeStr.end(), ' ', '_'); replace(TimeStr.begin(), TimeStr.end(), '\n', '_'); 
		replace(TimeStr.begin(), TimeStr.end(), ':', '-'); 
		string fileName = TimeStr +  "LAAC_Results";
		// ---------------------------------->>>>>>>>>>>>>>>>

		// ------------------>>>>>>>>>>> 3RD PART ESTIMATION :
		// 3rd Estimation class instantiation 
		cout << "Lever arms Estimation ----------------->:" << endl;
		LeverArmsEstimation LeverArmsEstimate;

		// Reading raw data from data selection
		cout << "Data reading ----------------->:" << endl;
		string dirString3rdPart;
		string folderData;
		MatrixXd RawData = LeverArmsEstimate.readTextFileRawdata(dirString3rdPart, folderData);

		// Results estimation
		VectorXd ResLAACclass = LeverArmsEstimate.LAAC_results(RawData, probability, fileName, dirString3rdPart, folderData, a_bi);
		// ---------------------------------->>>>>>>>>>>>>>>>
		tend1 = time(0); 
		cout << endl << "Estimation took "<< difftime(tend1, tstart1) <<" second(s)."<< endl << endl;
	}


	//#################################### Bad choice
	else
	{
		cout << "Error change your choice. It should be an integer between 1 and 3" << endl;
	}



	///////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////- END
	tend = time(0); 
	cout << endl << "LAAC took "<< difftime(tend, tstart) <<" second(s)."<< endl;
	system("pause");
	return 0;
}
// ----------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>