/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run MIBAC (Multibeam IMU Boresight Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

#pragma once
/*/-------------------------------------------------------------------------------------/*/
// MIBAC (Multibeam IMU Boresight Automatic Calibration)
//
// Class                  :  .h (BoresightEstimation)
// Authors                :  Rabine Keyetieu, Gaël Roué                 
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/08/12
//--------------------------------------------------------------------------------------/*/

// --------------------------------------------------------------------------->>>>>> Librairies:
// vector libs
#include <vector>
#include "Georeferencing.h"

// shortcut
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;
using std::string;
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>


class BoresightEstimation : public Georeferencing 
{
public:
	// --------------------------------------------------------------------------->>>>>> Methods:
	// Constructor
	BoresightEstimation(void);  

	// Data from selection  module reading
	MatrixXd readTextFileRawdata(string & dirString3rd, string & folderData);

	// Saving index for each patch
	void indexPerPatch(vector<vector<int>> & indexPerPatch_Out, int indPatch, int i, int & indPrevious,
					vector<double> & A_all, vector<double> & B_all, vector<double> & C_all,
					double A, double B, double C);

	// deltaX Least Square adjustment
	VectorXd LS_Adjustement(MatrixXd const& AbW_LS);

	// Least squares adjustment statistic
	VectorXd LS_Adjust_Statistic(MatrixXd const& AbW_LS, VectorXd DeltaEpsi, double probability);

	// Matrix needed for LS adjustment
	MatrixXd Matrix_ALS_bLS_WLS(string folderData, MatrixXd const& RawData, VectorXd Boresight, 
							VectorXd & ABC, int count);

	// MIBAC : Iterative least square adjustment and statistics
	VectorXd MIBAC_results(MatrixXd const& RawData, double probability, string fileName, string dirString3rd, string folderData);

	// Results : Writing results in files
	void Writing_MIBAC_BorEstimation(string dirString3rd, string fileName, VectorXd MIBAC_results_Out, int IterCounter, int numberPatch);
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>


protected:
	// --------------------------------------------------------------------------->>>>>> Attributs:
	string m_MIBAC_Estimation;
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>
};