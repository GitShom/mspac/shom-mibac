/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run MIBAC (Multibeam IMU Boresight Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

#pragma once
/*/-------------------------------------------------------------------------------------/*/
// MIBAC (Multibeam IMU Boresight Automatic Calibration)
//
// Class                  :  .h (Georeferencing)
// Authors                :  Rabine Keyetieu, Gaël Roué                
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/08/12
//--------------------------------------------------------------------------------------/*/


// --------------------------------------------------------------------------->>>>>> Librairies:
// vector libs
#include <vector>

// shortcut
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;
using std::string;
using std::pair;

//Constant
#define PI 3.14159265358979323846;
const double INF = 1.e100;

// Metadata reading: Define struct for output
struct Metadata 
{
	string ModelMBES;
	VectorXd CordTx;
	VectorXd CordGNSS;
	VectorXd PatchBoresight;
	double PosHorAcc, PosVerAcc;
	double PitchRollAcc, HeadingAcc;
	double Draft;
};

// Structure for matching data
struct Cell_DataReading
{
	VectorXd TimeMBES;

	VectorXd TimePos;
	VectorXd latitude;
	VectorXd longitude;
	VectorXd height;

	VectorXd TimePitchRoll;
	VectorXd pitch;
	VectorXd roll;

	VectorXd TimeHeading;
	VectorXd heading;

	VectorXd Ping2Delete;

	MatrixXd MBES_Data;
};
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>


class Georeferencing
{
public:
	// --------------------------------------------------------------------------->>>>>> Methods:
	// Constructor
	Georeferencing(void);  

	// Get current directory
	string current_working_directory();

	// Heading file reading
	MatrixXd readTextFileHeading(string fileName);

	// Pitch Roll file reading
	MatrixXd readTextFilePitchRoll(string fileName);

	// Position file reading
	MatrixXd readTextFilePosition(string fileName);

	// MBES data reading
	MatrixXd Reading_MBES_Data(string fileNameMBES);

	// DCM: C_bi_n
	MatrixXd DCM12(double phi, double theta, double psi); 

	// derDCM
	MatrixXd derDCM_phiThetaPsi(double phi, double theta, double psi);  

	// Soundings uncertainties
	VectorXd SoundingsUncertainties(VectorXd const& attitude, VectorXd const& attitude_Recep, 
	VectorXd const& SLGF, VectorXd const& Boresight, VectorXd const& a_bi,
	VectorXd const& SigRawData);

	// Merging data
	VectorXd MergingData(VectorXd const& Pn, VectorXd const& attitude, VectorXd const& SLGF, VectorXd const& a_bi);

	// Ray Tracing
	VectorXd  RayTracing(VectorXd const& sinAzCosAzBeta0, MatrixXd const& ProfilCel, double TravelTime);

	// Unit vector computation
	VectorXd UnitVectorComputation(VectorXd const& attitude, VectorXd const& attitude_Recp, 
		VectorXd const& Bor, double beta, double alpha);

	// SVP reading 
	MatrixXd readTextFileSVP(string fileName, double draft);

	// Metadata reading
	Metadata readTextFileMetadata(string fileName);

	// Assembling raw data uncertainties
	VectorXd RawDataUncertainties(Metadata const& MetaData_Out);

	// Global Georeferencing 
	MatrixXd georeferencing(MatrixXd const& RawData, Metadata const& MetaData_Out, MatrixXd const& ProfilC, 
							VectorXd const& SigRawData, string fileNameOut);

	// Georeferencing all files
	MatrixXd georeferencingAllFiles(vector<int> & indice, string & folderData, string & dirString2nd, string & dirString3rd);

	// Average of longitude, latitude and ellipsoidal height of all the files for local frame
	VectorXd Mean_LatLonHeight(vector<string> const& fileNames);

	// Keep only data collected before the end of "val"
	MatrixXd find_sup(VectorXd const& Time2modify, VectorXd const& data2modify, 
		              double val, vector<int>& ind, int taille);

	// Keep only data collected after the beginning of "val"
	MatrixXd find_inf(VectorXd const& Time2modify, VectorXd const& data2modify, 
		              double val, vector<int>& ind, int taille);

	// Linear interpolation one point
	double interpolate_linear(double x, vector<pair<double, double>> & table);

	// linear interpolation vector
	VectorXd interpl(VectorXd const& X_input, VectorXd const& Y_input, VectorXd const& X_wanted);

	// Remove a row from MatrixXd
	void removeRow(MatrixXd& matrix, long int rowToRemove);

	// Matching all raw data
	Cell_DataReading MatchingByInterpolation(Cell_DataReading const& Input);

	// Centered positioning data
	MatrixXd CenterGNSSData(VectorXd const& lat, VectorXd const& lon,
	VectorXd const& alt, double lat0, double lon0, double alt0);

	// Cidco format file reading
	MatrixXd CidcoFormatFileRead(string folderData, VectorXd const& MeanPos);
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>


protected:
	// --------------------------------------------------------------------------->>>>>> Attributs:
	string m_MIBAC_Georeferencing;
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>
};