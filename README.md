# Shom-MIBAC

MSPAC Shom solution, developped by CIDCO, ENSTA Bretagne and Shom. 

In order for everyone to experiment and test these algorithms, these results are
freely distributed. The study reports under licence ouverte/Open license;
as well as the source codes under CeCILL-C license (GPL compatible license).

The Shom disclaims any responsibility for the operational transfer as well as 
the maintenance of these softwares.