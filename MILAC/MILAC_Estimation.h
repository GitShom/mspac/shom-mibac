/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run MIBAC (Multibeam IMU Boresight Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

#pragma once
/*/-------------------------------------------------------------------------------------/*/
// MILAC (Multibeam IMU Latency Automatic Calibration)
//
// Class                  :  .h (MILAC_Estimation)
// Authors                :  Rabine Keyetieu, Gaël Roué                 
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/12/21
//--------------------------------------------------------------------------------------/*/


// --------------------------------------------------------------------------->>>>>> Librairies:
// vector libs
#include <vector>

// shortcut
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;
using std::string;
using std::pair;

//Constant
#define PI 3.14159265358979323846;
const double INF = 1.e100;

// Metadata reading: Define struct for output
struct Metadata 
{
	string ModelMBES;
	VectorXd CordTx;
	VectorXd CordGNSS;
	VectorXd PatchBoresight;
	VectorXd EmitterMountAngl;
	VectorXd ReceiverMountAngl;
	double PosHorAcc, PosVerAcc;
	double PitchRollAcc, HeadingAcc;
	double Draft;
};


// Structure for matching data
struct Cell_DataReading
{
	VectorXd TimeMBES;
	VectorXd TimePos;
	VectorXd latitude;
	VectorXd longitude;
	VectorXd height;
	VectorXd TimePitchRoll;
	VectorXd pitch;
	VectorXd roll;
	VectorXd TimeHeading;
	VectorXd heading;
	VectorXd Ping2Delete;
	MatrixXd MBES_Data;
	MatrixXd rawW_b_nb;
    MatrixXd rawW_b_nbPT;
};

//pragma
# pragma comment (lib, "libmx.lib")
# pragma comment (lib, "libeng.lib")
# pragma comment (lib, "libmex.lib")
# pragma comment (lib, "libmat.lib")
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>


class MILAC_Estimation
{
public:
	// --------------------------------------------------------------------------->>>>>> Methods:
	// Constructor
	MILAC_Estimation(void);  

	// Get current directory
	string current_working_directory();

	// Heading file reading
	MatrixXd readTextFileHeading(string fileName);

	// Pitch Roll file reading
	MatrixXd readTextFilePitchRoll(string fileName);

	// Position file reading
	MatrixXd readTextFilePosition(string fileName);

	// MBES data reading
	MatrixXd Reading_MBES_Data(string fileNameMBES);

	// SVP reading 
	MatrixXd readTextFileSVP(string fileName, double draft);

	// Metadata reading
	Metadata readTextFileMetadata(string fileName);

	// Assembling raw data uncertainties
	VectorXd RawDataUncertainties(Metadata const& MetaData_Out);

	// Average of longitude, latitude and ellipsoidal height of all the files for local frame
	VectorXd Mean_LatLonHeight(vector<string> const& fileNames);

	// Keep only data collected before the end of "val"
	MatrixXd find_sup(VectorXd const& Time2modify, VectorXd const& data2modify, 
		              double val, vector<int>& ind, int taille);

	// Keep only data collected after the beginning of "val"
	MatrixXd find_inf(VectorXd const& Time2modify, VectorXd const& data2modify, 
		              double val, vector<int>& ind, int taille);

	// Linear interpolation one point
	double interpolate_linear(double x, vector<pair<double, double>> & table);

	// linear interpolation vector
	VectorXd interpl(VectorXd const& X_input, VectorXd const& Y_input, VectorXd const& X_wanted);

	// Remove a row from MatrixXd
	void removeRow(MatrixXd& matrix, long int rowToRemove);

	// Remove a row from VectorXd
	void removeRow(VectorXd& Inivector, long int rowToRemove);

	// Matching all raw data
	Cell_DataReading MatchingByInterpolation(Cell_DataReading const& Input);

	// Centered positioning data
	MatrixXd CenterGNSSData(VectorXd const& lat, VectorXd const& lon,
	VectorXd const& alt, double lat0, double lon0, double alt0);

	// Angular rates and accelerations
	MatrixXd AngularVelAcc(VectorXd const& deltaT, VectorXd const& Roll, VectorXd const& Pitch,
	                   VectorXd const& Yaw, double sigR, double sigQ);

	// Plot of selected attitude data
	int PlotSelectedData(VectorXd const& Temps, VectorXd const& roll, VectorXd const& pitch,
					  vector<double> const& TpsSel, vector<double> const& RollSel, vector<double> const& PitchSel);

	// Extraction og index corresponding to high values for attitude data
	VectorXd Pic1(VectorXd const& Att, int n);

	// Extraction og index corresponding to high values for attitude data
	VectorXd Pic2(VectorXd const& Roll, int n);

	// Index of pings associated to superior and inferior pics
	VectorXd Pic2Delete(VectorXd const& Att, int n);

	// Computation of coefficients associated to roll and pitch data
	VectorXd Roll_Pitch_Proportion(VectorXd const& Roll, VectorXd const& Pitch);

	// Extract index correspondings to high values of attitude
	VectorXd RemovePics(VectorXd const& Roll, double segment);

	// Selection of pings excluding pics values
	VectorXd Attitude_Select(VectorXd const& Roll, double Q, double segment);

	// Automatic Selection of data according to roll and pitch attitude data
	VectorXd RobusteDataSelection(VectorXd const& Roll, VectorXd const& Pitch);

	// Selection of data per percentage
	VectorXd SelectPourcent_Valeur(MatrixXd const& w_b_nb, MatrixXd const& w_b_nb_PT,
	                               VectorXd const& Roll, VectorXd const& Pitch);

	// Cidco format file reading
	MatrixXd CidcoFormatFileRead(string folderData, VectorXd const& MeanPos, double dt2Estimate, int ApproachAuto);

	// C_bi_n
	MatrixXd DCM12(double phi, double theta, double psi);

	// derivative DCM wrt phi, theta, psi : dCbin_(phi, theta, psi)
	MatrixXd MILAC_Estimation::derDCM_phiThetaPsi(double phi, double theta, double psi);

	// Unit vector computation
	VectorXd UnitVectorComputation(double phi, double theta, double psi, 
								   VectorXd const& Bor, double beta, double alpha, 
								   double O1, double O2, double O3, double dt);

	// Ray Tracing
	VectorXd  RayTracing(VectorXd const& sinAzCosAzBeta0, MatrixXd const& ProfilCel, double TravelTime);

	// Merging data
	VectorXd MergingData(VectorXd const& Pn, double phi, double theta, double psi,
	                    double dt, double O1, double O2, double O3, 
						VectorXd const& SLGF, VectorXd const& a_bi);

	// TPU : soundings uncertainties
	VectorXd SoundingsUncertainties(double phi, double theta, double psi, 
						   VectorXd const& SLGF, VectorXd const& Boresight, VectorXd const& a_bi,
						   VectorXd const& SigRawData,
						   double O1, double O2, double O3, double dt);

	// Matrix needed for LS adjustment
	MatrixXd Matrix_ALS_bLS_WLS(string folderData, MatrixXd const& RawData, 
	                            double A, double B, double C, double D, double E, double F, double dt, double so2);

	// deltaX Least Square adjustment
	VectorXd LS_Adjustement(MatrixXd const& AbW_LS);

	// Least squares adjustment statistic
	VectorXd LS_Adjust_Statistic(MatrixXd const& AbW_LS, VectorXd DeltaEpsi, double probability);

	// Cidco format file reading
	VectorXd MILAC_Results(double probability, double dt2Estimate, int ApproachAuto, 
						   double & A, double & B, double & C, double & D, double & E, double & F, double & dt,
						   string & NameProject, string & folderData, string & dirString, vector<string> & fileNamesLines,
						   VectorXd & MeanPos, MatrixXd & RawData, int EstCounter);

	// writing MILAC results in files
	void Writing_MILAC_LatEstimation(string dirString, double dt, VectorXd LSStat, 
		                  int IterCounter, int nbrPt);
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>


protected:
	// --------------------------------------------------------------------------->>>>>> Attributs:
	string m_MILAC;
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>
};