/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run MIBAC (Multibeam IMU Boresight Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

/*/-------------------------------------------------------------------------------------/*/
// MILAC (Multibeam IMU Latency Automatic Calibration)
//
// Class                  :  .cpp (MILAC_Estimation)
// Authors                :  Rabine Keyetieu, Ga�l Rou�                 
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/12/21
//--------------------------------------------------------------------------------------/*/


// --------------------------------------------------------------------------->>>>>>>>>>>>>>>> LIBS:
// Class
#include <limits>
#include <direct.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <math.h> 
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <iterator>
#include <set>
#include <ctime>
#include <Eigen/Dense>
#include "spline.h"
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/inverse_gaussian.hpp>
#include <boost/math/distributions/students_t.hpp>
#include <windows.h>
#include <Engine.h>
#include "MILAC_Estimation.h"


// Using name space
using namespace std;

// using particular methods
using boost::math::chi_squared;
using boost::math::quantile;
using boost::math::complement;
using boost::math::normal;
using namespace boost::math;
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>


// --------------------------------------------------------------------------->>>>>> Methods definition
// --------------------------------------------------------------------------->>>>>>
// Conctructor
MILAC_Estimation::MILAC_Estimation(void)
{
	m_MILAC = "MILAC";
}// end constructor
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Get current directory
string MILAC_Estimation::current_working_directory()
{
	/* This function is used to get current directory
	*/

	char* cwd = _getcwd( 0, 0 ) ; // **** microsoft specific ****
	string working_directory(cwd) ;
	free(cwd) ;
	return working_directory ;

}// end function current_working_directory
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Heading file reading
MatrixXd MILAC_Estimation::readTextFileHeading(string fileName)
{
	/* This function is used to read Heading file

	Input data:
	- filename, heading file name

	Output data:
	- HeadingData, heading data
	*/

	// Initialization
	MatrixXd HeadingData(1000,2);
	int i=0;

	// Open file
	ifstream file;
	file.open(fileName);

	if (file)
	{
		// File reading
		for(string line; std::getline(file, line); )
		{
			// Set the precision
			setprecision (25);  

			istringstream in(line);    
			double tps, heading;

			in >> tps >> heading;
			HeadingData(i,0)=tps;
			HeadingData(i,1)=heading;
			i++;

			// Increase data size
			if (i%1000 == 0)
			{
				HeadingData.conservativeResize(i + 1000, 2);
			}

		}// end for
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output
	HeadingData.conservativeResize(i,2);
	return HeadingData;

}// end function readTextFileHeading
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Pitch Roll file reading
MatrixXd MILAC_Estimation::readTextFilePitchRoll(string fileName)
{
	/* This function is used to read pitch roll file

	Input data:
	- filename, pitch/roll file name

	Output data:
	- PitchRollData, Pitch/Roll data
	*/

	// Initialization
	MatrixXd PitchRollData(1000,3);
	int i=0;

	// Open file
	ifstream file;
	file.open(fileName);

	if (file)
	{
		// File reading
		for(string line; std::getline(file, line); )
		{
			// Set the precision
			setprecision (25);  

			istringstream in(line);    
			double tps, pitch, roll;

			in >> tps >> pitch >> roll;
			PitchRollData(i,0)=tps;
			PitchRollData(i,1)=pitch;
			PitchRollData(i,2)=roll;
			i++;

			// Increase data size
			if (i%1000 == 0)
			{
				PitchRollData.conservativeResize(i + 1000, 3);
			}

		}// end for
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output
	PitchRollData.conservativeResize(i,3);
	return PitchRollData;

}// end function readTextFilePitchRoll
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Position file reading
MatrixXd MILAC_Estimation::readTextFilePosition(string fileName)
{
	/* This function is used to read position file

	Input data:
	- filename, position file name

	Output data:
	- PositionData, Position data
	*/

	// Initialization
	MatrixXd PositionData(1000,4);
	int i=0;

	// Open file
	ifstream file;
	file.open(fileName);

	if (file)
	{
		// File reading
		for(string line; std::getline(file, line); )
		{
			// Set the precision
			setprecision (25);  

			istringstream in(line);    
			//unsigned long long tps;
			double tps, lat, lon, elH;

			in >> tps >> lat >> lon >> elH;
			PositionData(i,0)=tps;
			PositionData(i,1)=lat;
			PositionData(i,2)=lon;
			PositionData(i,3)=elH;
			i++;

			// Increase data size
			if (i%1000 == 0)
			{
				PositionData.conservativeResize(i + 1000, 4);
			}

		}// end for
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output
	PositionData.conservativeResize(i,4);
	return PositionData;

}// end function readTextFilePosition
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// MBES data reading
MatrixXd MILAC_Estimation::Reading_MBES_Data(string fileNameMBES)
{
	/* This function is used to read MBES file

	Input data:
	- filename, MBES file name

	Output data:
	- MBESData, MBES data
	*/

	// Initalization
	MatrixXd MBESData;

	// Open files
	ifstream file;
	file.open(fileNameMBES);

	if (file)
	{

		// Extract the maximum of columns (maximum of soundings)
		VectorXd First_3_Col(1000);
		int i=0;
		for(string line; std::getline(file, line); )
		{
			// Set the precision
			setprecision (25);  

			double tps, SSS, NbrBeams;
			istringstream in(line);
			in >> tps >> SSS >> NbrBeams;
			First_3_Col(i) = NbrBeams;
			i++;

			if (i%1000 == 0)
			{
				First_3_Col.conservativeResize(i+1000);
			}
		}
		First_3_Col.conservativeResize(i);

		// Initialization of MBES data
		int MaxNumberBeams = static_cast<int>(First_3_Col.maxCoeff());
		int NumberOfColumns = MaxNumberBeams*3 + 3;
		long NumberOfPings = First_3_Col.rows();
		MBESData = MatrixXd::Constant(NumberOfPings, NumberOfColumns, -10000000);

		// Reading of data line by line
		file.close();
		ifstream file2(fileNameMBES);
		i=0;
		for(string line; std::getline(file2, line); )
		{
			istringstream in(line);
			for (int j = 0; j < (First_3_Col(i)*3+3); j++) 
			{
				double x;
				in >> x;
				MBESData(i,j)= x;
			}
			i++;
		}
	}
	else
	{
		cout << "File reading error" << endl;
	}

	// Output
	return MBESData;

}// end function Reading_MBES_Data
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// SVP reading 
MatrixXd MILAC_Estimation::readTextFileSVP(string fileName, double draft)
{

	/* This function is used to read SVP file and adapt
	with draft

	Input data: 
	- fileName, SVP file name 

	Output data:
	- ProfilC_Out, Sound velocity and depth
	*/

	// Initialization
	MatrixXd ProfilC_Out;
	MatrixXd ProfilC(1000,2);

	// Open file
	ifstream file;
	string line;
	file.open(fileName);

	if (file)
	{
		//// Reading
		//read stream line by line
		int i = 0;
		for(string line; getline(file, line); )   
		{
			//make a stream for the line itself
			istringstream in(line);				  

			//now read the whitespace-separated floats
			double cProfil, zProfil;
			in >> zProfil >> cProfil;					      

			//fill the profil vector
			ProfilC(i,0)=cProfil;      
			ProfilC(i,1)=-zProfil;
			i++;
		}
		// keep just non empty elements
		ProfilC.conservativeResize(i,2);


		//// Adaptation with draft
		// Find index where depth is lower than the draft
		VectorXd ind(ProfilC.rows());
		int compteur = 0;
		for (int i=0 ; i<ProfilC.rows() ; i++)
		{
			if (ProfilC(i,1) <= draft)
			{
				ind(compteur) = i;
				compteur++;
			}
		}
		ind.conservativeResize(compteur);

		// add draft if the case in SVP data
		if (ind.rows()!=0)
		{
			// compute sound velcotity assocuated to draft
			const int last(ind(ind.rows()-1));
			double gi = (ProfilC(last+1,0) - ProfilC(last,0)) / (ProfilC(last+1,1) - ProfilC(last,1));
			double cDraft = ProfilC(last,0) + gi*(draft - ProfilC(last,1));

			// New profil with draft
			MatrixXd ProfilCnew(ProfilC.rows() - last, 2);
			ProfilCnew << cDraft, draft, ProfilC.bottomRows(ProfilC.rows()-(last+1));
			ProfilC_Out = ProfilCnew;
		}
		else
		{
			ProfilC_Out = ProfilC;
		}
	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	// Output 
	ProfilC_Out.col(1) = ProfilC_Out.col(1).array() - ProfilC_Out(0,1);
	return ProfilC_Out;

} // end function readTextFileSVP
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Metadata reading
Metadata MILAC_Estimation::readTextFileMetadata(string fileName)
{
	/* This function is used to read Metadata file in NED
	convention

	Input data: 
	- fileName, Metadata file name 

	Output data:
	- Metadata_Out, 
	struct Metadata [CordTx, CordGNSS, PosHorAcc, PosVerAcc, PitchRollAcc, HeadingAcc, Draft, Boresight, NameDevice]
	*/

	// initialization
	Metadata Metadata_Out;
	VectorXd CordTx(3), CordGNSS(3), Boresight(3), EmitterMountAngl(3), ReceiverMountAngl(3);
	string NameDevice;

	double Patch_Roll, Patch_Pitch, Patch_Heading;
	double PitchRollAcc, HeadingAcc;
	double PosHorAcc, PosVerAcc;
	double MBES_X, MBES_Y, MBES_Z;
	double AntX, AntY, AntZ;
	double Draft;

	double Eroll =0, Epitch=0, Eyaw=0, Rroll=0, Rpitch=0, Ryaw=0, Mroll=0, Mpitch=0, Myaw=0;

	const double Pi = PI;
	const double d2r = Pi/180;

	// Open file
	ifstream file;
	string line;
	file.open(fileName);

	if (file)
	{
		/// Reading
		//read stream line by line
		for(string line; getline(file, line); )  
		{
			//make a stream for the line itself
			istringstream in(line);  

			string type;
			in >> type;

			if (type=="MultibeamModel")
			{
				in >> NameDevice;
			}
			else if (type=="AntPositionOffsetX")
			{
				in >> AntX;
			}
			else if (type=="AntPositionOffsetY")
			{
				in >> AntY;
			}
			else if (type=="AntPositionOffsetZ")
			{
				in >> AntZ;
			}
			else if (type=="MBEOffsetX")
			{
				in >> MBES_X;
			}
			else if (type=="MBEOffsetY")
			{
				in >> MBES_Y;
			}
			else if (type=="MBEOffsetZ")
			{
				in >> MBES_Z;
			}
			else if (type=="MBEDraft")
			{
				in >> Draft;
			}
			else if (type=="PositionAccuracy")
			{
				in >> PosHorAcc;
				PosVerAcc = PosHorAcc*1.5;
			}
			else if (type=="PitchRollAccuracy")
			{
				in >> PitchRollAcc;
			}
			else if (type=="HeadingAccuracy")
			{
				in >> HeadingAcc;
			}
			else if (type=="RollAlignment")
			{
				in >> Patch_Roll;
			}
			else if (type=="PitchAlignment")
			{
				in >> Patch_Pitch;
			}
			else if (type=="HeadingAlignment")
			{
				in >> Patch_Heading;
			}
			else if (type=="MBEOffsetR")
			{
				in >> Eroll;
			}
			else if (type=="MBEOffsetP")
			{
				in >> Epitch;
			}
			else if (type=="MBEOffsetH")
			{
				in >> Eyaw;
			}
			else if (type=="MBEOffset2R")
			{
				in >> Rroll;
			}
			else if (type=="MBEOffset2P")
			{
				in >> Rpitch;
			}
			else if (type=="MBEOffset2H")
			{
				in >> Ryaw;
			}
			else if (type=="MotionSensorR")
			{
				in >> Mroll;
			}
			else if (type=="MotionSensorP")
			{
				in >> Mpitch;
			}
			else if (type=="MotionSensorH")
			{
				in >> Myaw;
			}

		} // end For

		// Transmitter, GNSS and boresight
		CordTx << MBES_Y, MBES_X, -MBES_Z;
		CordGNSS << AntY, AntX, -AntZ;
		Boresight << Patch_Roll*d2r, Patch_Pitch*d2r, Patch_Heading*d2r;

		// Transmitter, Receiver relative to bI (IMU) frame
		EmitterMountAngl  << (Eroll - Mroll)*d2r, (Epitch - Mpitch)*d2r, (Eyaw - Myaw)*d2r;
		ReceiverMountAngl << (Rroll - Mroll)*d2r, (Rpitch - Mpitch)*d2r, (Ryaw - Myaw)*d2r;

		// Output
		Metadata_Out.CordTx = CordTx;
		Metadata_Out.CordGNSS = CordGNSS;
		Metadata_Out.PatchBoresight = Boresight;

		Metadata_Out.EmitterMountAngl = EmitterMountAngl;
		Metadata_Out.ReceiverMountAngl = ReceiverMountAngl;

		Metadata_Out.PitchRollAcc = PitchRollAcc*d2r;
		Metadata_Out.HeadingAcc = HeadingAcc*d2r;

		Metadata_Out.PosHorAcc = PosHorAcc;
		Metadata_Out.PosVerAcc = PosVerAcc;

		Metadata_Out.Draft = Draft;
		Metadata_Out.ModelMBES = NameDevice;

	}
	else
	{
		cout << "File reading error !" << endl; 
	}

	return Metadata_Out;

} // end function readTextFileMetadata
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Assembling raw data uncertainties
VectorXd MILAC_Estimation::RawDataUncertainties(Metadata const& MetaData_Out)
{
	/* This function is used to cluster georeferencing soundings uncertainties

	Input data: 
	- MetaData_Out, soundings meta data

	Output data:
	- RawDataUncertainties_Out, raw data uncertainties
	*/

	// Initialization
	VectorXd RawDataUncertainties_Out(15);

	const double Pi = PI;
	const double d2r = Pi/180;

	// uncertainties
	RawDataUncertainties_Out << MetaData_Out.PosHorAcc, MetaData_Out.PosHorAcc, MetaData_Out.PosVerAcc,  // Pn (Pn1, Pn2, Pn3)
		MetaData_Out.PitchRollAcc, MetaData_Out.PitchRollAcc, MetaData_Out.HeadingAcc,                   // Attitude (phi, theta, psi)
		0.01*d2r, 0.01*d2r, 0.01*d2r,                                                                    // Boresight (dphi, dtheta, dpsi)
		0.01, 0.01*d2r, 0.01*d2r,                                                                        // rbs (rho, beta, alpha)
		0.01, 0.01, 0.01;                                                                                // Lever arms (ax, ay, az)

	// Output
	return RawDataUncertainties_Out;

}// end function RawDataUncertainties
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Average of longitude, latitude and ellipsoidal height of all the files for local frame
VectorXd MILAC_Estimation::Mean_LatLonHeight(vector<string> const& fileNames)
{
	/* This function is used to compute average position

	Input data:
	- fileNames, position files name

	Output data:
	- Pos0, Average Position data
	*/

	// Initialization
	int NberOfFiles = fileNames.size();
	VectorXd LatTot(1000);
	VectorXd LonTot(1000);
	VectorXd HeightTot(1000);
	VectorXd Pos0(3);
	int c = 0;

	// Loop on each files
	for (int i=0; i<NberOfFiles; i++)
	{
		// file open
		string tmp = fileNames[i] + "\\AntPosition.txt";
		ifstream file;
		file.open(tmp);

		if (file)
		{

			for(string line; std::getline(file, line); )
			{
				double tps, lat, lon, elH;
				istringstream in(line);
				in >> tps >> lat >> lon >> elH;
				LonTot(c)= lon;
				LatTot(c)= lat;
				HeightTot(c)= elH;
				c++;

				if (c%1000 == 0)
				{
					LonTot.conservativeResize(c+1000);
					LatTot.conservativeResize(c+1000);
					HeightTot.conservativeResize(c+1000);
				}

			} // end for line
			file.close();

		}// end if (file)
		else
		{
			cout << "File reading error !" << endl;
		}

	}// end for i

	LonTot.conservativeResize(c);
	LatTot.conservativeResize(c);
	HeightTot.conservativeResize(c);

	// Average computation: Pos0 is [Long0, Lat0, Height0]
	double Pi = PI; double d2r = Pi/180;
	Pos0(0) = LonTot.mean()*d2r;
	Pos0(1) = LatTot.mean()*d2r;
	Pos0(2) = HeightTot.mean();

	// Output
	return Pos0;

} // end function Mean_LatLonHeight
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Keep only data collected before the end of "val"
MatrixXd MILAC_Estimation::find_sup(VectorXd const& Time2modify, VectorXd const& data2modify, double val, vector<int>& ind, int taille)
{
	/* This function is used keeping only data collected before a definied time (val)

	Input data:
	- Time2modify, initial time of data to modify
	- data2modify, data collected
	- val, referene time 
	- ind, index of soundings satisfying the condition,
	- taille, to synchronize data index when deleting

	Output data:
	- TimeData, new time and data deleted from soundings not satisfying the condition
	*/

	// Initialization
	VectorXd NewTime(Time2modify.rows());
	VectorXd NewData(Time2modify.rows());
	int c=0;

	// Loop on each value of pitch roll
	for (int i=0; i<Time2modify.rows(); i++)
	{
		if (Time2modify(i) <= val)
		{
			NewTime(c) = Time2modify(i);
			NewData(c) = data2modify(i);
			c++;
		}

		else
		{
			ind.push_back(i+taille);
		}

	}// end for

	// resize data
	NewTime.conservativeResize(c);
	NewData.conservativeResize(c);

	// Output: New values
	MatrixXd TimeData(NewData.rows(), 2);
	TimeData.col(0) = NewTime;
	TimeData.col(1) = NewData;
	return TimeData;

}// end function find_sup
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Keep only data collected after the beginning of "val"
MatrixXd MILAC_Estimation::find_inf(VectorXd const& Time2modify, VectorXd const& data2modify, double val, vector<int>& ind, int taille)
{
	/* This function is used keeping only data collected after a definied time (val)

	Input data:
	- Time2modify, initial time of data to modify
	- data2modify, data collected
	- val, referene time 
	- ind, index of soundings satisfying the condition,
	- taille, to synchronize data index when deleting

	Output data:
	- TimeData, new time and data deleted from soundings not satisfying the condition
	*/

	// Initialization
	VectorXd NewTime(Time2modify.rows());
	VectorXd NewData(Time2modify.rows());
	int c=0;

	// Loop on each value of pitch roll
	for (int i=0; i<Time2modify.rows(); i++)
	{
		if (Time2modify(i) >= val)
		{
			NewTime(c) = Time2modify(i);
			NewData(c) = data2modify(i);
			c++;
		}

		else
		{
			ind.push_back(i+taille);
		}

	}// end for

	// resize data
	NewTime.conservativeResize(c);
	NewData.conservativeResize(c);

	// Output: New values
	MatrixXd TimeData(NewData.rows(), 2);
	TimeData.col(0) = NewTime;
	TimeData.col(1) = NewData;
	return TimeData;

}// end function find_inf
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Linear interpolation one point
double MILAC_Estimation::interpolate_linear(double x, vector<pair<double, double>> & table)
{
	/* This function is used for interpolation

	Input data:
	- x, value from which we need the interpolation
	- table, reference data for interpolation

	Output data:
	- interpolated value
	*/

	// Assumes that "table" is sorted by .first
	// Check if x is out of bound
	if (x > table.back().first) return INF;
	if (x < table[0].first) return -INF;

	vector<pair<double, double> >::iterator it, it2;

	// INFINITY is defined in math.h in the glibc implementation
	it = lower_bound(table.begin(), table.end(), make_pair(x, -INF));

	// Corner case
	if (it == table.begin()) return it->second;
	it2 = it;
	--it2;
	return it2->second + (it->second - it2->second)*(x - it2->first)/(it->first - it2->first);

}// end function interpolate_linear 
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// linear interpolation vector
VectorXd MILAC_Estimation::interpl(VectorXd const& X_input, VectorXd const& Y_input, VectorXd const& X_wanted)
{
	/* This function is used for interpolation

	Input data:
	- x, value from which we need the interpolation
	- X_input, Y_input, reference data for interpolation
	- X_wanted, data we need interpolation

	Output data:
	- Output, interpolated value vector
	*/

	// Initialization
	vector<pair<double, double> > table;
	VectorXd Output(X_wanted.rows());

	for (int i=0; i<X_input.rows(); i++)
	{
		table.push_back(make_pair(X_input(i),Y_input(i)));
	}

	// interpolation point by point
	for (int i=0; i<X_wanted.rows(); i++)
	{
		Output(i) = interpolate_linear(X_wanted(i), table);
	}

	// Output
	return Output;

}// end function interpl
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Remove a row from MatrixXd
void MILAC_Estimation::removeRow(MatrixXd& matrix, long int rowToRemove)
{
	/* This function is used to remove a row from a MAtrixXd

	Input data:
	- matrix, a matrix we want to remove row
	- rowToRemove, row index we want to remove
	*/

	// Intialization
	long int numRows = matrix.rows()-1;
	long int numCols = matrix.cols();

	// MatrixXd
	if( rowToRemove < numRows)
		matrix.block(rowToRemove,0,numRows-rowToRemove,numCols) = matrix.block(rowToRemove+1,0,numRows-rowToRemove,numCols);

	// if last item just resize
	matrix.conservativeResize(numRows,numCols);

}// end function removeRow
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Remove a row from VectorXd
void MILAC_Estimation::removeRow(VectorXd& Inivector, long int rowToRemove)
{
	/* This function is used to remove a row from a VectorXd

	Input data:
	- Inivector, a vector we want to remove row
	- rowToRemove, row index we want to remove
	*/

	// Intialization
	long int numRows = Inivector.rows()-1;

	// VectorXd
	if( rowToRemove < numRows)
		Inivector.segment(rowToRemove, numRows-rowToRemove) = Inivector.segment(rowToRemove+1, numRows-rowToRemove);

	// if last item just resize
	Inivector.conservativeResize(numRows);

}// end function removeRow VectorXd
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Matching all raw data
Cell_DataReading MILAC_Estimation::MatchingByInterpolation(Cell_DataReading const& Input)
{
	/* This funcion is used to match data (synchronize)

	Input data:
	- Cell_DataReading, constituted of data to synchronize

	Output data:
	- Output, synchronize data
	*/

	//  Data
	// MBES time (Reference time)
	VectorXd TimeMBES2 = Input.TimeMBES;

	// Position
	VectorXd TimePos = Input.TimePos;
	VectorXd latitude = Input.latitude;
	VectorXd longitude = Input.longitude;
	VectorXd height = Input.height;

	// attitude
	VectorXd TimePitchRoll = Input.TimePitchRoll;
	VectorXd pitch = Input.pitch;
	VectorXd roll = Input.roll;
	VectorXd TimeHeading = Input.TimeHeading;
	VectorXd heading = Input.heading;

	//angular velocity
    MatrixXd w_b_nb = Input.rawW_b_nb;
    VectorXd w_b_nb1 = w_b_nb.col(0);
    VectorXd w_b_nb2 = w_b_nb.col(1);
    VectorXd w_b_nb3 = w_b_nb.col(2);

    //angular Acceleration
    MatrixXd w_b_nbPt = Input.rawW_b_nbPT;
    VectorXd w_b_nbPt1 = w_b_nbPt.col(0);
    VectorXd w_b_nbPt2 = w_b_nbPt.col(1);
    VectorXd w_b_nbPt3 = w_b_nbPt.col(2);

	// MBES Data
	MatrixXd MBESData = Input.MBES_Data;

	// Keep MBES ping which start after the beginning and end before auxiliary
	// data (position and attitude) to ensure the interpolation
	VectorXd TimeMBES = TimeMBES2.head(TimeMBES2.rows()-1);
	vector<int> ind(0);
	int taille(0);
	double tmp = max(TimePos(0),TimePitchRoll(0));
	MatrixXd TimeMbesModif = find_inf(TimeMBES, TimeMBES, tmp, ind, taille);
	TimeMBES = TimeMbesModif.col(0);

	taille = ind.size();
	tmp = min(TimePos(TimePos.rows()-1),TimePitchRoll(TimePitchRoll.rows()-1));
	TimeMbesModif = find_sup(TimeMBES, TimeMBES, tmp, ind, taille);
	TimeMBES = TimeMbesModif.col(0);

	// Matching position data
	VectorXd latitudeMatch = interpl(TimePos, latitude, TimeMBES);
	VectorXd longitudeMatch = interpl(TimePos, longitude, TimeMBES);
	VectorXd heightMatch = interpl(TimePos, height, TimeMBES);

	// Matching attitude data
	VectorXd pitchMatch = interpl(TimePitchRoll, pitch, TimeMBES);
	VectorXd rollMatch = interpl(TimePitchRoll, roll, TimeMBES);
	VectorXd headingMatch = interpl(TimePitchRoll, heading, TimeMBES);

    // Angular velocity
    VectorXd w_b_nb_Roll_interp = interpl(TimePitchRoll, w_b_nb1, TimeMBES);
    VectorXd w_b_nb_Pitch_interp = interpl(TimePitchRoll, w_b_nb2, TimeMBES);
    VectorXd w_b_nb_Yaw_interp = interpl(TimePitchRoll, w_b_nb3, TimeMBES);

    MatrixXd w_b_nbMatch(w_b_nb_Roll_interp.rows(),3);  
	w_b_nbMatch << w_b_nb_Roll_interp, w_b_nb_Pitch_interp, w_b_nb_Yaw_interp;

    // Angular Acceleration
    VectorXd w_b_nbPt_Roll_interp = interpl(TimePitchRoll, w_b_nbPt1, TimeMBES);
    VectorXd w_b_nbPt_Pitch_interp = interpl(TimePitchRoll, w_b_nbPt2, TimeMBES);
    VectorXd w_b_nbPt_Yaw_interp = interpl(TimePitchRoll, w_b_nbPt3, TimeMBES);

    MatrixXd w_b_nbPTMatch(w_b_nbPt_Roll_interp.rows(),3); 
	w_b_nbPTMatch << w_b_nbPt_Roll_interp, w_b_nbPt_Pitch_interp, w_b_nbPt_Yaw_interp;

	// index to delete
	VectorXd ind2(ind.size());
	for (int i=0;i<ind.size();i++) 
	{ 
		ind2(i) = ind[i]; 
	}

	for (int i=0;i<ind.size();i++) 
	{ 
		removeRow( MBESData, ind2(i) );
		ind2.array() = ind2.array() - 1;
	}
	removeRow(MBESData, MBESData.rows()-1);

	// Output
	Cell_DataReading Output;
	Output.latitude = latitudeMatch;
	Output.longitude = longitudeMatch;
	Output.height = heightMatch;
	Output.pitch = pitchMatch;
	Output.roll = rollMatch;
	Output.heading = headingMatch;
	Output.TimeMBES = TimeMBES;
	Output.Ping2Delete = ind2;
	Output.MBES_Data = MBESData;
	Output.rawW_b_nb = w_b_nbMatch;
    Output.rawW_b_nbPT = w_b_nbPTMatch;
	return Output;

}// end function MatchingByInterpolation
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Centered positioning data
MatrixXd MILAC_Estimation::CenterGNSSData(VectorXd const& lat, VectorXd const& lon,
	VectorXd const& alt, double lat0, double lon0, double alt0)
{
	/* This function is used to compute centered position in navigation frame

	Input data:
	- lat, lon, alt, latitude, longitude and ellipsoidal height
	- lat0, lon0, alt0, average position data

	Output data:
	- Pn_t123C, centered position data in navigation frame
	*/

	// WGS84 ellipsoid Parameters
	double a = 6378137;
	double e = 0.081819190842622;

	// Positioning data
	// Transformation of lon, lat, alt to TRF
	VectorXd N, PTRF1, PTRF2, PTRF3;
	N = ((1- (e*lat.array().sin()).pow(2)).sqrt()); N = a*( N.array().inverse() );
	PTRF1 = (N.array() + alt.array())*(lat.array().cos())*(lon.array().cos());
	PTRF2 = (N.array() + alt.array())*(lat.array().cos())*(lon.array().sin());
	PTRF3 = (N.array()*(1-pow(e,2)) + alt.array())*lat.array().sin();

	// Transformation of GNSS data from TRF frame to Navigation frame (n)
	VectorXd Pn_t1, Pn_t2, Pn_t3;
	Pn_t1 = cos(lat0)*PTRF3 - sin(lat0)*sin(lon0)*PTRF2 - sin(lat0)*cos(lon0)*PTRF1;
	Pn_t2 = cos(lon0)*PTRF2 - sin(lon0)*PTRF1;
	Pn_t3 = -sin(lat0)*PTRF3 - cos(lat0)*sin(lon0)*PTRF2 - cos(lat0)*cos(lon0)*PTRF1;

	//Local frame center
	//Transformation of lon, lat, alt to TRF
	double N0, PTRF10, PTRF20, PTRF30;
	N0 = a/(sqrt(1 - pow(e*sin(lat0), 2)));
	PTRF10 = (N0+alt0)*cos(lat0)*cos(lon0);
	PTRF20 = (N0+alt0)*cos(lat0)*sin(lon0);
	PTRF30 = (N0*(1-pow(e,2)) + alt0)*sin(lat0);

	// Transformation of GNSS data from TRF frame to Navigation frame (n)
	double Pn_t10, Pn_t20, Pn_t30;
	Pn_t10 = cos(lat0)*PTRF30 - sin(lat0)*sin(lon0)*PTRF20 - sin(lat0)*cos(lon0)*PTRF10;
	Pn_t20 = cos(lon0)*PTRF20 - sin(lon0)*PTRF10;
	Pn_t30= -sin(lat0)*PTRF30 - cos(lat0)*sin(lon0)*PTRF20 - cos(lat0)*cos(lon0)*PTRF10;

	// Centered GNSS data
	VectorXd Pn_t1C, Pn_t2C, Pn_t3C;
	Pn_t1C = Pn_t1.array() - Pn_t10;
	Pn_t2C = Pn_t2.array() - Pn_t20;
	Pn_t3C = Pn_t3.array() - Pn_t30;

	// Output
	MatrixXd Pn_t123C(lat.rows(), 3);
	Pn_t123C << Pn_t1C, Pn_t2C, Pn_t3C;

	return Pn_t123C;

}// end function CenterGNSSData
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Angular velcotity and accelerations
MatrixXd MILAC_Estimation::AngularVelAcc(VectorXd const& deltaT, VectorXd const& Roll, VectorXd const& Pitch,
	                   VectorXd const& Yaw, double sigR, double sigQ)
{
	/*This function is used to compute angular velocities and accelerations

	Input data:
	- Roll, Pitch, Yaw attitude data
	- DeltaT, time
	- SigR variance factor associated to observations
	- SigQ, variance factor associated to state equation

	Output data:
	- AngularVelAcc, angular velocities and accelerations
	*/


    //Length of data
    long n = deltaT.rows();

    // Initialisation of Xk and P
    VectorXd Xk(9);
    Xk << Roll(0), Pitch(0), Yaw(0), 0, 0, 0, 0, 0, 0;
    double varI = pow(0.001, 2);
    double dtk = deltaT(1) - deltaT(0);
    MatrixXd P(9,9); P.setIdentity(9,9);
    P(0,0) = varI; P(1,1) = varI; P(2,2) = varI;
    P(3,3) = 4*varI/dtk; P(4,4) = 4*varI/dtk; P(5,5) = 4*varI/dtk;
    P(6,6) = 4*varI/(dtk*dtk); P(7,7) = 4*varI/(dtk*dtk); P(8,8) = 4*varI/(dtk*dtk);

    // Constant parameters of kalman filter
    // Unknows : Xk = [Attitude (roll,  pitch,  yaw),  derivative of attitude AND derivative second of attitude]
    //           Xk = [phik, thetak, psik, phi_ptk, theta_ptk, psi_ptk, phi_pt2k, theta_pt2k, psi_pt2k  ]
    int m = 9;

    //Observation Y is attitude data;

    //Matrix H
    MatrixXd H(3,9); H.setIdentity(3,9);
    H(0,0)=1; H(1,1)=1; H(2,2)=1;

    // Covariance matrix R from v : noise of observation,
    MatrixXd R(3,3); R.setIdentity(3,3);
    R(0,0) = sigR*sigR; R(1,1) = sigR*sigR; R(2,2) = sigR*sigR;

    // Initialisation of values of P, W_b_nb, SkewW_b_nb and Xk
    // Xk initialisation
    VectorXd PHI(n), THETA(n), PSI(n), PHI_PT(n), THETA_PT(n);
	VectorXd PSI_PT(n), PHI_2PT(n), THETA_2PT(n), PSI_2PT(n);

    PHI(0)      = Xk(0); THETA(0)    = Xk(1); PSI(0)      = Xk(2);
    PHI_PT(0)   = Xk(3); THETA_PT(0) = Xk(4); PSI_PT(0)   = Xk(5);
    PHI_2PT(0)  = Xk(6); THETA_2PT(0)= Xk(7); PSI_2PT(0)  = Xk(8);

    // W_b_nb & w_b_nb_PT initialisation
    MatrixXd w_b_nb(3,n);
    VectorXd Norm_w_b_nb(n);
    MatrixXd w_b_nb_PT(3,n);
    VectorXd Norm_w_b_nb_PT(n);

    MatrixXd matw(3,3);
    matw << 1. , 0. , -sin(THETA(0)),
    0. , cos(PHI(0)) , sin(PHI(0))*cos(THETA(0)),
    0. , -sin(PHI(0)) , cos(PHI(0))*cos(THETA(0));

    VectorXd tmp(3); tmp << PHI_PT(0),THETA_PT(0),PSI_PT(0);
    w_b_nb.col(0) = matw * tmp;
    Norm_w_b_nb(0) = (w_b_nb.col(0)).norm();

    MatrixXd matw_PT(3,3);
    matw_PT << 0. , 0., -THETA_PT(0)*cos(THETA(0)),
    0, -PHI_PT(0)*sin(PHI(0)),PHI_PT(0)*cos(PHI(0))*cos(THETA(0)) - THETA_PT(0)*sin(PHI(0))*sin(THETA(0)),
    0,-PHI_PT(0)*cos(PHI(0)),-PHI_PT(0)*sin(PHI(0))*cos(THETA(0)) - THETA_PT(0)*cos(PHI(0))*sin(THETA(0));

    VectorXd tmpPT(3);  tmpPT  << PHI_PT(0),  THETA_PT(0),  PSI_PT(0);
    VectorXd tmp2PT(3); tmp2PT << PHI_2PT(0), THETA_2PT(0), PSI_2PT(0);
    w_b_nb_PT.col(0) = matw * tmp2PT + matw_PT * tmpPT;
    Norm_w_b_nb_PT(0) = (w_b_nb_PT.col(0)).norm();

    MatrixXd zero(3,3); zero.setZero(3, 3);
    MatrixXd eye(3,3); eye.setIdentity(3,3);
    MatrixXd eyem(m,m); eyem.setIdentity(m,m);

    // Algorithm of Kalman filter
    // n : length of time
    // m : length of unknows

    for (int i=1; i<n; i++)

    {
        // time increment
        dtk = deltaT(i) - deltaT(i-1);

        // Matrix A
        MatrixXd A(9,9);
        A << eye, dtk*eye, 0.5*pow(dtk,2)*eye,
        zero,eye,dtk*eye,
        zero,zero,eye;

        // Covariance matrix Q from W : noise of state
        MatrixXd Q(9,9);
        Q << sigQ * eye * pow((1./6)*pow(dtk, 3),2), eye * sigQ * (1./12)*pow(dtk, 5), sigQ * eye * (1./6)*pow(dtk, 4),
        sigQ * eye * (1./12)*pow(dtk, 5), sigQ * eye * pow(0.5*pow(dtk,2),2), sigQ *  eye *(1./2)*pow(dtk, 3),
        sigQ * eye * (1./6)*pow(dtk, 4), sigQ * eye * (1./2)*pow(dtk, 3), sigQ * eye *pow(dtk, 2);

        // PREDICTION
        // first step
        VectorXd Xk_1 = A * Xk;

        // second step : % by assuming that Gk is the identity (6,6) matrix
        MatrixXd P_1 = A * P * A.adjoint() + Q;
        //// UPDATE
        MatrixXd K1 = P_1 * H.transpose();
        MatrixXd K2 = H * P_1 * H.transpose() + R;
        MatrixXd invK2 = K2.inverse();
        MatrixXd K = K1 * invK2;

        // fourth step
        VectorXd Yk(3); Yk << Roll(i), Pitch(i), Yaw(i); 
        Xk = Xk_1 + K * (Yk - H * Xk_1);

        //// fifth step
        P = (eyem - K * H) * P_1 * (eyem - K * H).transpose() + K*R*(K.transpose());

        // new values
        PHI(i)       = Xk(0);
        THETA(i)    = Xk(1);
        PSI(i)      = Xk(2);
        PHI_PT(i)    = Xk(3);
        THETA_PT(i)  = Xk(4);
        PSI_PT(i)    = Xk(5);
        PHI_2PT(i)   = Xk(6);
        THETA_2PT(i) = Xk(7);
        PSI_2PT(i)   = Xk(8);

        // computation of w_b_nb
        MatrixXd matw(3,3);
        matw << 1. , 0. , -sin(THETA(i)),
        0. , cos(PHI(i)) , sin(PHI(i))*cos(THETA(i)),
        0. , -sin(PHI(i)) , cos(PHI(i))*cos(THETA(i));

        VectorXd tmp(3);
        tmp << PHI_PT(i),THETA_PT(i),PSI_PT(i);
        w_b_nb.col(i) = matw * tmp;
        Norm_w_b_nb(i) = (w_b_nb.col(i)).norm();

        MatrixXd matw_PT(3,3);
        matw_PT << 0. , 0., -THETA_PT(i)*cos(THETA(i)),
        0, -PHI_PT(i)*sin(PHI(i)),PHI_PT(i)*cos(PHI(i))*cos(THETA(i)) - THETA_PT(i)*sin(PHI(i))*sin(THETA(i)),
        0,-PHI_PT(i)*cos(PHI(i)),-PHI_PT(i)*sin(PHI(i))*cos(THETA(i)) - THETA_PT(i)*cos(PHI(i))*sin(THETA(i));
        VectorXd tmpPT(3);
        tmpPT << PHI_PT(i), THETA_PT(i), PSI_PT(i);
        VectorXd tmp2PT(3);
        tmp2PT << PHI_2PT(i), THETA_2PT(i), PSI_2PT(i);
        w_b_nb_PT.col(i) = matw * tmp2PT + matw_PT * tmpPT;
        Norm_w_b_nb_PT(i) = (w_b_nb_PT.col(i)).norm();
    }

	// Disp min, max and mean values
	cout << "min of roll rate (rad/s): "  <<  PHI_PT.minCoeff() << endl;
	cout << "max of roll rate (rad/s): "  <<  PHI_PT.maxCoeff() << endl;

    //output data
    MatrixXd AngVelAcc(n, 6);
    AngVelAcc.middleCols(0, 3) = w_b_nb.transpose();
    AngVelAcc.middleCols(3, 3) = w_b_nb_PT.transpose();
    return AngVelAcc;

}// end function AngularVelAcc
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Plot of selected attitude data
int MILAC_Estimation::PlotSelectedData(VectorXd const& Temps, VectorXd const& roll, VectorXd const& pitch,
					  vector<double> const& TpsSel, vector<double> const& RollSel, vector<double> const& PitchSel)
{
	/*This function is used to plot selected data among attitude data

	Input data:
	- roll, pitch respectively roll and pitch attitude data
	- selection, index of selected ping
	*/

	// Engine pointer
	Engine *ep;
	mxArray *phi = NULL, *the = NULL, *tps = NULL;
	mxArray *phiSel = NULL, *theSel = NULL, *tpsSel = NULL;

	// Open of matlab
	if (!(ep = engOpen(""))) {
		fprintf(stderr, "\nCan't start MATLAB engine\n");
		return EXIT_FAILURE;
	}

	// Initialization
	vector <double> TpsAtt, RollPhi, PitchThe;
	double Pi = PI;
	double r2d = 180/Pi;

	// Extraction of all data
	for (int i=0; i<roll.rows();i++)
	{
		// Initialization to have type vector
		RollPhi.push_back(roll(i)*r2d);
		PitchThe.push_back(pitch(i)*r2d);
		TpsAtt.push_back(Temps(i));

		// copy into engine type variable
		tps = mxCreateDoubleMatrix(1,TpsAtt.size(), mxREAL);
		memcpy(mxGetPr(tps), TpsAtt.data(), sizeof(double)*TpsAtt.size());
		engPutVariable(ep, "tps", tps);

		phi = mxCreateDoubleMatrix(1,RollPhi.size(), mxREAL);
		memcpy(mxGetPr(phi), RollPhi.data(), sizeof(double)*RollPhi.size());
		engPutVariable(ep, "phi", phi);

		the = mxCreateDoubleMatrix(1,PitchThe.size(), mxREAL);
		memcpy(mxGetPr(the), PitchThe.data(), sizeof(double)*PitchThe.size());
		engPutVariable(ep, "the", the);
	}

	// copy into engine type variable
	tpsSel = mxCreateDoubleMatrix(1,TpsSel.size(), mxREAL);
	memcpy(mxGetPr(tpsSel), TpsSel.data(), sizeof(double)*TpsSel.size());
	engPutVariable(ep, "tpsSel", tpsSel);

	phiSel = mxCreateDoubleMatrix(1,RollSel.size(), mxREAL);
	memcpy(mxGetPr(phiSel), RollSel.data(), sizeof(double)*RollSel.size());
	engPutVariable(ep, "phiSel", phiSel);

	theSel = mxCreateDoubleMatrix(1,PitchSel.size(), mxREAL);
	memcpy(mxGetPr(theSel), PitchSel.data(), sizeof(double)*PitchSel.size());
	engPutVariable(ep, "theSel", theSel);
	
	// Roll plot
	engEvalString(ep, "figure (1)");
	engEvalString(ep, "subplot(211)");
	engEvalString(ep, "plot(tps-tps(1), phi, tpsSel-tps(1), phiSel, '*', 'LineWidth', 3), hold on, ;");
	engEvalString(ep, "title('Roll attitude data');");
	engEvalString(ep, "xlabel('Time [s]');");
	engEvalString(ep, "ylabel('Roll [�]');");
	engEvalString(ep, "legend(Attitude data, Pings selected);");

	// Pitch plot
	engEvalString(ep, "subplot(212)");
	engEvalString(ep, "plot(tps-tps(1), the, tpsSel-tps(1), theSel, '*', 'LineWidth', 3), hold on, ;");
	engEvalString(ep, "title('Pitch attitude data');");
	engEvalString(ep, "xlabel('Time [s]');");
	engEvalString(ep, "ylabel('Pitch [�]');");
	engEvalString(ep, "legend(Attitude data, Pings selected);");

}// end function PlotSelectedData
// --------------------------------------------------------------------------->>>>>>



//<<<<<<<------------------------Start of set of functions for automatic selection for Latency---------------------------->>>>>>
// --------------------------------------------------------------------------->>>>>>
// Extraction og index corresponding to high values for attitude data
VectorXd MILAC_Estimation::Pic1(VectorXd const& Att, int n)
{
	/*This function is used to extract index associated to high values of attitude data
	The scanning is performed from the first to the last element (superior)

	Input data:
	- Att, attitude data
	- n, length of attitude data

	Output data:
	- indpic, index of found pics
	*/

	// Initialization
    VectorXd Diff(n-1), indPic(n);

    // Difference of attitude 
    for (int i=0; i<n-1; i++)
    {
        Diff(i) = Att(i+1) - Att(i);
    }

    for (int i=1; i<n-2; i++)
    {
        if ( ( Diff(i) == 0 ) )
        {
            Diff(i) = Diff(i-1);
        }
    }

    // Index of pics
    int ind = 0;
    for (int i=0; i<n-2; i++)
    {
        if ( Diff(i+1)*Diff(i)  < 0 )
        {
            indPic(ind) = i+1;
            ind++;
        }
    }
    indPic.conservativeResize(ind);

    //ouput Data:
	VectorXd indpic(ind + 2);
    indpic <<0, indPic, n-1;
    return indpic;

}// end function Pic1
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Extraction og index corresponding to high values for attitude data
VectorXd MILAC_Estimation::Pic2(VectorXd const& Att, int n)
{
	/*This function is used to extract index associated to high values of attitude data
	The scanning is performed from the last to the first element (inferior)

	Input data:
	- Att, attitude data
	- n, length of attitude data

	Output data:
	- indpic, index of found pics
	*/

	// Initialization
    VectorXd Diff(n-1), indPic(n);

    // Difference of attitude
    for (int i=0; i<n-1; i++)
    {
        Diff(i) = Att(i+1) - Att(i);
    }

    for (int i=n-3; i>0; i--)
    {
        if ( ( Diff(i) == 0 ) )
        {
            Diff(i) = Diff(i+1);
        }
    }

    // Index of pics
    int ind = 0;
    for (int i=0; i<n-2; i++)
    {
		if  ( Diff(i+1)*Diff(i) < 0)
        {
            indPic(ind) = i+1;
            ind++;
        }
    }
    indPic.conservativeResize(ind);

    //ouput Data:
	VectorXd indpic(ind +2);
    indpic <<0, indPic, n-1;
    return indpic;

}// end function Pic2
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Index of pings associated to superior and inferior pics
VectorXd MILAC_Estimation::Pic2Delete(VectorXd const& Att, int n)
{
	/*This function is used to extract attitude data corresponding to pics (they will be deleted)
	In fact we de not want to select pics values which do not correspond to high angular rate and
	low angular accelerations.

	Input data:
	- Att, attitude data
	- n, length of attitude data

	Output data:
	- indpic, index of found pics
	*/

	// Extraction of pics
    VectorXd ind1 = Pic1(Att, n); //borne sup�rieur des pics
    VectorXd ind2 = Pic2(Att, n); //borne inf�rieure des pics

    VectorXd ind2delete(n);
    int ind = 0;
    for (int i=0; i<ind2.rows(); i++)
    {
        for (int j=ind2(i); j <=ind1(i); j++)
        {
            ind2delete(ind) = j;
            ind++;
        }
    }
    ind2delete.conservativeResize(ind);
    return ind2delete;

}// end function Pic2Delete
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Computation of coefficients associated to roll and pitch data
VectorXd MILAC_Estimation::Roll_Pitch_Proportion(VectorXd const& Roll, VectorXd const& Pitch)
{
	/*This function is used to compute coefficents corresponding to roll and pitch for weighting
	of data selection

	Input data:
	- Roll, Pitch, attitude data

	Output data:
	- Q, Weighting Coefficients for roll and pitch
	*/

    //length of Data
    int n = Roll.rows();

    //indice des pic (uniquement le borne sup�rieure on pouvait aussi prendre la borne sup�rieure)
    VectorXd indPicRoll, indPicPitch;
    indPicRoll = Pic1(Roll, n);
    indPicPitch = Pic1(Pitch, n);

    //size of indPic:
    int NpicRoll = indPicRoll.rows(); // nombre de pic pour le roulis
    int NpicPitch = indPicPitch.rows(); // nombre de pic pour le tangage

    //variation moyenne du roulis
    VectorXd diffRollPic(NpicRoll-1), diffPitchPic(NpicPitch-1);
    for (int i=0; i<NpicRoll-1; i++)
    {
        diffRollPic(i) = Roll(indPicRoll(i+1)) - Roll(indPicRoll(i));
    }
    double meanRoll = diffRollPic.array().abs().mean();

    //variation moyenne du pitch
    for (int i=0; i<NpicPitch-1; i++)
    {
        diffPitchPic(i) = Pitch(indPicPitch(i+1)) - Pitch(indPicPitch(i));
    }
    double meanPitch = diffPitchPic.array().abs().mean();

    //calcul des proportion: More Q is bigger less the number of selected points
	cout << "Enter the factor for automatic selection (0 < Qf < 1): ";
	double Qfactor; cin >> Qfactor;

    double QRoll, QPitch;
    if(meanRoll >= meanPitch)
    {
		QRoll = Qfactor;
		if ((meanPitch/meanRoll) > 0.5) 
		{
			double eps = 1 + 5*meanPitch/meanRoll;
			QPitch = Qfactor + (1-Qfactor)/eps;
		}
		else 
		{
			double eps = 1 + 2*meanPitch/meanRoll;
			QPitch = Qfactor + (1-Qfactor)/eps;
		}
    }
    else
    {
        QPitch = Qfactor;
		if ((meanRoll/meanPitch) > 0.5) 
		{
			double eps = 1 + 5*meanRoll/meanPitch;
			QRoll = Qfactor + (1-Qfactor)/eps;
		}
		else 
		{
			double eps = 1 + 2*meanRoll/meanPitch;
			QRoll = Qfactor + (1-Qfactor)/eps;
		}  
    }

    //output Data:
    VectorXd Q(2);
    Q << QRoll, QPitch;
    return Q;

}// end function Roll_Pitch_Proportion
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Extract index correspondings to high values of attitude
VectorXd MILAC_Estimation::RemovePics(VectorXd const& Roll, double segment)
{
	/*This function is used to extract index of high values of attitude

	Input data:
	- Roll, attitude data

	Output data:
	- selection, index of extracted data
	*/

    //length of Data:
    int n = Roll.rows();

    //indice des pic
    VectorXd indpic = Pic1(Roll, n);

    //Nombre de pings:
    int Npic = indpic.rows() - 1;

    //indice des bornes inf�rieures et sup�rieures pour les pics..
    VectorXd indpic1(Npic), indpic2(Npic);
    indpic1 << indpic.segment(0, Npic);
    indpic2 << indpic.segment(1, Npic);

    //Amplitude des pic
    VectorXd Rollpic1(Npic), Rollpic2(Npic);
    for (int i=0; i<Npic; i++)
    {
        Rollpic1(i) = Roll(indpic1(i));
        Rollpic2(i) = Roll(indpic2(i));
    }

    //Vecteur des amplitudes cr�te � cr�te:
    VectorXd DiffRoll = Rollpic2 - Rollpic1;

    // threshld
    VectorXd Rollfort1 = Rollpic1.array() + (1./segment)*DiffRoll.array();
    VectorXd Rollfort2 = Rollpic2.array() - (1./segment)*DiffRoll.array();

    // For each pic remove data at 1/segment proportion
    VectorXd Selection(n);
    int Nping = 0;
    for (int ii=0; ii<Npic; ii++)
    {
       for(int i=indpic1(ii); i<indpic2(ii); i++)
       {
           if ( (Roll(i) > Rollfort2(ii)) && (Roll(i) > Rollfort1(ii)) ) 
		   {
			  Selection(Nping) = i;
              Nping = Nping+1;
		   }
           else if ( (Roll(i) < Rollfort2(ii)) && (Roll(i) < Rollfort1(ii)) ) 
		   {
			  Selection(Nping) = i;
              Nping = Nping+1;
		   }
       }// end second for
    }// end first for

    //output data:
    Selection.conservativeResize(Nping);
    return Selection;

}// end function RemovePics
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Selection of pings excluding pics values
VectorXd MILAC_Estimation::Attitude_Select(VectorXd const& Roll, double Q, double segment)
{
	/*This function is used to select data for the estimation (pics values are excluded)

	Input data:
	- Roll, attitude data
	- Q, Weighting Coefficients for roll and pitch

	Output data:
	- selection, index of selected data
	*/

    //length of Data:
    int n = Roll.rows();

    //indice des pic
    VectorXd indpic = Pic1(Roll, n);

    //Nombre de pings:
    int Npic = indpic.rows() - 1;

    //indice des bornes inf�rieures et sup�rieures pour les pics..
    VectorXd indpic1(Npic), indpic2(Npic);
    indpic1 << indpic.segment(0, Npic);
    indpic2 << indpic.segment(1, Npic);

    //Amplitude des pic
    VectorXd Rollpic, Rollpic1(Npic), Rollpic2(Npic);
    for (int i=0; i<Npic; i++)
    {
        Rollpic1(i) = Roll(indpic1(i));
        Rollpic2(i) = Roll(indpic2(i));
    }

    //Vecteur des amplitudes cr�te � cr�te:
    VectorXd Roll2 = Rollpic2 - Rollpic1;

    //choix des pic ayant un fort circle (>Q% du circle max):
    double SRoll = Q*Roll2.array().abs().maxCoeff();
    int Npicfort = 0;

    VectorXd indfort1(Npic), indfort2(Npic), Rollfort(Npic), Rollfort1(Npic), Rollfort2(Npic);
    for (int i=0; i<Npic; i++)
    {
        if( abs(Roll2(i)) >= SRoll )
        {
            indfort1(Npicfort) = indpic1(i);
            indfort2(Npicfort) = indpic2(i);

            Rollfort(Npicfort) = Roll2(i);
            Rollfort1(Npicfort) = Rollpic1(i);
            Rollfort2(Npicfort) = Rollpic2(i);

            Npicfort ++;
        }
    }

    //conservative Resize
    indfort1.conservativeResize(Npicfort);
    indfort2.conservativeResize(Npicfort);

    Rollfort.conservativeResize(Npicfort);
    Rollfort1.conservativeResize(Npicfort);
    Rollfort2.conservativeResize(Npicfort);

    // threshld
    Rollfort1 = Rollfort1.array() + (1./segment)*Rollfort.array();
    Rollfort2 = Rollfort2.array() - (1./segment)*Rollfort.array();

    //Selection des pings ayant une forte vitese angulaire et une faible acc�l�ration angulaire
    VectorXd Selection(n);
    int Nping = 0;
    for (int ii=0; ii<Npicfort; ii++)
    {
       for(int i=indfort1(ii); i<indfort2(ii); i++)
       {
           if ( (Roll(i) <= Rollfort2(ii)) && (Roll(i) >= Rollfort1(ii)) )
           {
              Selection(Nping) = i;
              Nping = Nping+1;
           }

           if ( (Roll(i) >= Rollfort2(ii)) && (Roll(i) <= Rollfort1(ii)) )
           {
              Selection(Nping) = i;
              Nping = Nping+1;
           }
       }// end second for
    }// end first for

    //output data:
    Selection.conservativeResize(Nping);
    return Selection;

}// end function Attitude_Select
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Automatic Selection of data according to roll and pitch attitude data
VectorXd  MILAC_Estimation::RobusteDataSelection(VectorXd const& Roll, VectorXd const& Pitch)
{
	/*This function is used to select data considering automatic selection approach based only on roll
	and pitch data.

	Input data:
	- Roll, Pitch, attitude data

	Output data:
	- selection, index of selected data
	*/

    //length of data:
    int n = Roll.rows();

    //Calcul des Proportion
    VectorXd Q = Roll_Pitch_Proportion(Roll, Pitch);
    double QRoll = Q(0);
    double QPitch = Q(1);

    //Extraction des sondes Selectionn�es
	double Segment = 10;
    VectorXd indRoll = Attitude_Select(Roll, QRoll, Segment);
    VectorXd indPitch = Attitude_Select(Pitch, QPitch, Segment);

    // Union
    VectorXd indSelect(indRoll.rows() + indPitch.rows());
    indSelect << indRoll, indPitch;

    //suppr�ssion des pics
	//------> First approach: Delete only beams corresponding to pics
	// VectorXd indRoll2Delete = Pic2Delete(Roll, n);
    // VectorXd indPitch2Delete = Pic2Delete(Pitch, n);
	//------> Second approach: Delete beams corresponding to pics at 1/segment proportion
	VectorXd indRoll2Delete = RemovePics(Roll, Segment);
    VectorXd indPitch2Delete = RemovePics(Pitch, Segment);
	// Merge of index to delete
	VectorXd ind2Delete(indRoll2Delete.rows() + indPitch2Delete.rows());
    ind2Delete << indRoll2Delete, indPitch2Delete;

	//Transformation des eigen en tableau dynamik (vector).
    vector<double> tabSelect;
    for (int i=0; i<indSelect.rows();i++ )
    {
        tabSelect.push_back(indSelect(i));
    }

    vector<double> tab2Delete;
    for (int i=0; i<ind2Delete.rows();i++ )
    {
		tab2Delete.push_back(ind2Delete(i));
    }

    //set_difference
	vector<int> SelectMinusDelete;
	set<int> s(tab2Delete.begin(), tab2Delete.end());
	copy_if(tabSelect.begin(), tabSelect.end(), back_inserter(SelectMinusDelete),
                [&s](int elem) { return s.find(elem) == s.end(); });


    //reconversion des tableau en matrix Eigen
    VectorXd selection( SelectMinusDelete.size() );
    for(int j=0; j<SelectMinusDelete.size(); j++)
    {
        selection(j) = SelectMinusDelete[j];
    }

	// Output
    return selection;

}// end function RobusteDataSelection
// --------------------------------------------------------------------------->>>>>>
//<<<<<<<---------------------------End of Suite of functions for automatic selection for Latency------------------------>>>>>>



// --------------------------------------------------------------------------->>>>>>
// Selection of data per percentage
VectorXd MILAC_Estimation::SelectPourcent_Valeur(MatrixXd const& w_b_nb, MatrixXd const& w_b_nb_PT,
	                                              VectorXd const& Roll, VectorXd const& Pitch)
{
	/*This function is used to select data considering percentage associated to
	angular rate and angular accelerations computed from roll, pitch and heading

	Input data:
	- w_b_nb angular rate
	- w_b_nb_PT, angular acceleration

	Output data:
	- selection, index of selected data
	*/

    // Angular rate and acceleration computation of norm:
    VectorXd Norm_w_b_nb =    (w_b_nb.col(0).array().pow(2) + 
		                       w_b_nb.col(1).array().pow(2) + 
		                       w_b_nb.col(2).array().pow(2)).sqrt();
    VectorXd Norm_w_b_nb_PT = (w_b_nb_PT.col(0).array().pow(2) + 
		                       w_b_nb_PT.col(1).array().pow(2) + 
							   w_b_nb_PT.col(2).array().pow(2)).sqrt();

    // Defined percentage: less are a and b, less are the number of selected points
	// If we increase "a" and "b" we will get much more points
	cout << "Enter coefficient for angular rate (a < 1): "; double a; cin >> a; 
	cout << "Enter coefficient for angular accelerations (b < 1): "; double b; cin >> b;

    // Velocity threshold computed from previous defined percentage
    double eps1 = Norm_w_b_nb.maxCoeff() - a*(Norm_w_b_nb.maxCoeff() - Norm_w_b_nb.minCoeff());

    // Acceleration threshold computed from previous defined percentage
    double eps2 = Norm_w_b_nb_PT.minCoeff() + b*(Norm_w_b_nb_PT.maxCoeff() - Norm_w_b_nb_PT.minCoeff());

    // Then selection of high angular rate and low angular acceleration data
    VectorXd ind2Select(w_b_nb.rows());
    int kk =0;
    for (int i=0; i<w_b_nb.rows(); i++)
    {
        if ( Norm_w_b_nb(i) > eps1  &&  Norm_w_b_nb_PT(i) < eps2 )
        {
            ind2Select(kk) = i;
            kk++ ;
        }

    }
    ind2Select.conservativeResize(kk);

	//suppr�ssion des pics
	//------> First approach: Delete only beams corresponding to pics
	// VectorXd indRoll2Delete = Pic2Delete(Roll, n);
    // VectorXd indPitch2Delete = Pic2Delete(Pitch, n);
	//------> Second approach: Delete beams corresponding to pics at 1/segment proportion
	double Segment = 10;
	VectorXd indRoll2Delete = RemovePics(Roll, Segment);
    VectorXd indPitch2Delete = RemovePics(Pitch, Segment);
	// Merge of index to delete
	VectorXd ind2Delete(indRoll2Delete.rows() + indPitch2Delete.rows());
    ind2Delete << indRoll2Delete,indPitch2Delete;

	//Transformation des eigen en tableau dynamik (vector).
    vector<double> tabSelect;
    for (int i=0; i<ind2Select.rows();i++ )
    {
        tabSelect.push_back(ind2Select(i));
    }
    vector<double> tab2Delete;
    for (int i=0; i<ind2Delete.rows();i++ )
    {
		tab2Delete.push_back(ind2Delete(i));
    }

    //set_difference
	vector<int> SelectMinusDelete;
	set<int> s(tab2Delete.begin(), tab2Delete.end());
	copy_if(tabSelect.begin(), tabSelect.end(), back_inserter(SelectMinusDelete),
                [&s](int elem) { return s.find(elem) == s.end(); });

    // Output data: reconversion des tableau en matrix Eigen
    VectorXd selection( SelectMinusDelete.size() );
    for(int j=0; j<SelectMinusDelete.size(); j++)
    {
        selection(j) = SelectMinusDelete[j];
    }
    return selection;

}// end function SelectPourcent_Valeur
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Cidco format file reading
MatrixXd MILAC_Estimation::CidcoFormatFileRead(string folderData, VectorXd const& MeanPos, double dt2Estimate, int ApproachAuto)
{
	/*This function is used to read raw data from survey system 
	(formatted in CIDCO configuration)

	Input data:
	- folderData, location of data
	- MeanPos, average position data

	Output data:
	- CidcoFormatData, raw data
	*/
	//---------------------------------> files name
	string fileNameAntPos(folderData + "/AntPosition.txt"); 
	string fileNamePitchRoll(folderData + "/PitchRoll.txt");
	string fileNameHeading(folderData + "/Heading.txt"); 
	string fileNameMBES(folderData + "/Multibeam.txt");

	// Degree To Radian and Radian To Degree
	double Pi = PI; double d2r = Pi/180, r2d = 180/Pi;

	//---------------------------------> Reading of different files
	// GNSS data
	MatrixXd AntPosData = readTextFilePosition(fileNameAntPos);
	VectorXd TimePos = AntPosData.col(0);
	VectorXd latitude = AntPosData.col(1) * d2r;
	VectorXd longitude = AntPosData.col(2) * d2r;
	VectorXd height = AntPosData.col(3);

	// Pitch roll data
	MatrixXd PitchRollData = readTextFilePitchRoll(fileNamePitchRoll);
	VectorXd TimePitchRoll = PitchRollData.col(0);
	VectorXd pitch = PitchRollData.col(1) * d2r;
	VectorXd roll = PitchRollData.col(2) * d2r;

	// Heading data
	MatrixXd HeadingData = readTextFileHeading(fileNameHeading);
	VectorXd TimeHeading  = HeadingData.col(0);
	VectorXd heading = HeadingData.col(1);
	for (int h=1; h<heading.rows(); h++)
	{
		if (heading(h)-heading(h-1) < -180.)  {heading(h) = heading(h) + 360. ;}
		else if (heading(h)-heading(h-1) > 180.)  { heading(h) = heading(h) - 360. ;}
	}
	heading = heading * d2r ;

	// MBES time is our reference time
	MatrixXd MBESData = Reading_MBES_Data(fileNameMBES);
	VectorXd TimeMBES = MBESData.col(0);


	// ---------------------------------> Synchronization of attitude data
	// Synchronization of attitude data
	vector<int> indSyn; int tailleSyn = 0;
	MatrixXd TimeRoll = find_inf(TimePitchRoll, roll, TimeHeading(0), indSyn, tailleSyn);
	MatrixXd TimePitch = find_inf(TimePitchRoll, pitch, TimeHeading(0), indSyn, tailleSyn);
	MatrixXd TimeRoll2 = find_sup(TimeRoll.col(0), TimeRoll.col(1), TimeHeading(TimeHeading.rows()-1), indSyn, tailleSyn);
	MatrixXd TimePitch2 = find_sup(TimeRoll.col(0), TimePitch.col(1), TimeHeading(TimeHeading.rows()-1), indSyn, tailleSyn);
	roll = TimeRoll2.col(1);
	pitch = TimePitch2.col(1);
	TimePitchRoll = TimeRoll2.col(0);
	heading = interpl(TimeHeading, heading, TimePitchRoll);


	// ---------------------------------> Add latency attitude data
	// Time with fake latency
    VectorXd TimePR_Latency = TimePitchRoll.array() + dt2Estimate;

	// Spline
    roll = tk::Interp_spline(TimePitchRoll, roll, TimePR_Latency);
    pitch = tk::Interp_spline(TimePitchRoll, pitch, TimePR_Latency);
    heading = tk::Interp_spline(TimePitchRoll, heading, TimePR_Latency);
	// Linear
	//roll = interpl(TimePitchRoll, roll, TimePR_Latency);
	//pitch = interpl(TimePitchRoll, pitch, TimePR_Latency);
	//heading = interpl(TimePitchRoll, heading, TimePR_Latency);

	// Remove bad data induced by fake latency
	VectorXd TimePitchRoll_2 = TimePitchRoll;
	if (dt2Estimate > 0)
	{
		int IndLast = TimePitchRoll.rows()-1;
		while( TimePitchRoll_2(IndLast) + dt2Estimate > TimePitchRoll_2(TimePitchRoll_2.rows()-1) )
		{
			removeRow(roll, roll.rows()-1);
			removeRow(pitch, pitch.rows()-1);
			removeRow(heading, heading.rows()-1);
			removeRow(TimePitchRoll, TimePitchRoll.rows()-1);
			IndLast--;
		}
	}
	else if(dt2Estimate < 0)
	{
		int IndFirst = 0;
		while( TimePitchRoll_2(IndFirst) + dt2Estimate < TimePitchRoll_2(0))
		{
			removeRow(roll, 0);
			removeRow(pitch, 0);
			removeRow(heading, 0);
			removeRow(TimePitchRoll, 0);
			IndFirst++;
		}
	}


	// ---------------------------------> Computation of angular velocities and accelerations
	double sigR = 1e-20;
    double sigQ = 1e-3;
    MatrixXd AngRateAngAcc = AngularVelAcc(TimePitchRoll, roll, pitch, heading, sigR, sigQ);
    MatrixXd rawW_b_nb(TimePitchRoll.rows(), 3), rawW_b_nbPT(TimePitchRoll.rows(), 3);
    rawW_b_nb = AngRateAngAcc.middleCols(0, 3);
    rawW_b_nbPT = AngRateAngAcc.middleCols(3, 3); 


	// ---------------------------------> Synchronisation of global data
	// Matching of data by interpolation
	// allow to delete MBES data that do not coincide with others data, so not to have bad values when interpolating
	Cell_DataReading Data2Interpolate;
	Data2Interpolate.TimeMBES = TimeMBES;
	Data2Interpolate.TimePos = TimePos;
	Data2Interpolate.latitude = latitude;
	Data2Interpolate.longitude = longitude;
	Data2Interpolate.height = height;
	Data2Interpolate.TimePitchRoll = TimePitchRoll;
	Data2Interpolate.pitch = pitch;
	Data2Interpolate.roll = roll;
	Data2Interpolate.TimeHeading = TimeHeading;
	Data2Interpolate.heading = heading;
	Data2Interpolate.MBES_Data = MBESData;
	Data2Interpolate.rawW_b_nb = rawW_b_nb;
    Data2Interpolate.rawW_b_nbPT = rawW_b_nbPT;

	Cell_DataReading InterpolatedData = MatchingByInterpolation(Data2Interpolate);

	// matched Attitude data
	VectorXd pitchMatch = InterpolatedData.pitch;
	VectorXd rollMatch = InterpolatedData.roll;
	VectorXd headingMatch = InterpolatedData.heading;
	MatrixXd attitude(headingMatch.rows(),3);
	attitude << rollMatch, pitchMatch, headingMatch;

	// Angular velocities and accelerations
    MatrixXd w_b_nbMatch = InterpolatedData.rawW_b_nb;
    MatrixXd w_b_nbPTMatch = InterpolatedData.rawW_b_nbPT;

	//  matched position data
	VectorXd latitudeMatch = InterpolatedData.latitude;
	VectorXd longitudeMatch = InterpolatedData.longitude;
	VectorXd heightMatch = InterpolatedData.height;

	// Transformation from geographic coordinates(lon, lat, h) to local navigation frame NED (Pnt1, Pnt2, Pnt3)
	MatrixXd PGeo = CenterGNSSData(latitudeMatch, longitudeMatch, heightMatch, MeanPos(1), MeanPos(0), MeanPos(2));

	// MBES ref time and Ping to delete
	MBESData = InterpolatedData.MBES_Data;
	VectorXd TimeMBES_ref = InterpolatedData.TimeMBES;
	VectorXd Ping2Delete = InterpolatedData.Ping2Delete; 

	//Ping 
	VectorXd Ping(TimeMBES_ref.rows());
	for (int i=0; i<TimeMBES_ref.rows(); i++)
	{
		Ping(i)=i+1;
	}

	// Adapt data to data per beams
	VectorXd SSS = MBESData.col(1);
	VectorXd NberBeam = MBESData.col(2);

	long nbrElt = NberBeam.sum();

	VectorXd Beta(nbrElt,1);
	VectorXd Alpha(nbrElt,1);
	VectorXd TwoTravelTime(nbrElt,1);

	VectorXd Time_long(nbrElt,1);
	VectorXd Ping_long(nbrElt,1);
	VectorXd Index_Beam(nbrElt,1);
	VectorXd SSS_long(nbrElt,1);
	MatrixXd attitude_long(nbrElt,3);
	MatrixXd w_b_nb_long(nbrElt,3);
    MatrixXd w_b_nbPT_long(nbrElt,3);
	MatrixXd PGeo_long(nbrElt,3);

	long indMin;
	long indMax;
	int c = 0;

	for(int i=0; i<MBESData.rows(); i++)
	{
		indMin = NberBeam.topRows(i).sum() ;
		indMax = NberBeam.topRows(i+1).sum() - 1;

		for (long j=0;j<(indMax-indMin+1);j++)
		{
			// Extract MBES data
			TwoTravelTime(c) = MBESData(i,3*j+3);
			Alpha(c) = MBESData(i,3*j+4)*d2r;
			Beta(c) = MBESData(i,3*j+5)*d2r;
			c++;
		}

		int counterBeams = 1;
		for (long j=indMin;j<=indMax;j++)
		{
			// adapt attitude data
			attitude_long(j,0) = attitude(i,0);
			attitude_long(j,1) = attitude(i,1);
			attitude_long(j,2) = attitude(i,2);

			// angular rate
			w_b_nb_long(j,0) = w_b_nbMatch(i, 0);
            w_b_nb_long(j,1) = w_b_nbMatch(i, 1);
            w_b_nb_long(j,2) = w_b_nbMatch(i, 2);

            // angular acceleration
			w_b_nbPT_long(j,0) = w_b_nbPTMatch(i, 0);
            w_b_nbPT_long(j,1) = w_b_nbPTMatch(i, 1);
            w_b_nbPT_long(j,2) = w_b_nbPTMatch(i, 2);

			// adapt position data
			PGeo_long(j,0) = PGeo(i,0);
			PGeo_long(j,1) = PGeo(i,1);
			PGeo_long(j,2) = PGeo(i,2);

			// adapt other data
			Ping_long(j) = Ping(i);
			SSS_long(j) = SSS(i);
			Time_long(j) = TimeMBES_ref(i);
			Index_Beam(j)= counterBeams;
			counterBeams++;   
		}

	}// end for i


	// time corresponding to receive beams
	VectorXd TimeBasis = Time_long + TwoTravelTime;

	// Delete beams index whose time is greater for ensuring interpolation
	vector<int> ind2Remove;
	VectorXd TimeBasisInt = Time_long.array() + TwoTravelTime.array();
	MatrixXd TimeBasisNew = find_sup(TimeBasisInt, TimeBasisInt, TimePitchRoll(TimePitchRoll.rows()-1), ind2Remove, tailleSyn);
	TimeBasis = TimeBasisNew.col(0);

	//attitude data at received time:
	//-----------------> Linear:
	// Linear: attitude
	VectorXd Attitude_Roll_interp = interpl(TimePitchRoll, roll, TimeBasis);
	VectorXd Attitude_Pitch_interp = interpl(TimePitchRoll, pitch, TimeBasis);
	VectorXd Attitude_Yaw_interp = interpl(TimePitchRoll, heading, TimeBasis);
	// Linear: angular rate
	VectorXd AngRate_Roll_interp = interpl(TimePitchRoll, rawW_b_nb.col(0), TimeBasis);
	VectorXd AngRate_Pitch_interp = interpl(TimePitchRoll, rawW_b_nb.col(1), TimeBasis);
	VectorXd AngRate_Yaw_interp = interpl(TimePitchRoll, rawW_b_nb.col(2), TimeBasis);
	//-----------------> Spline:
	// Spline: attitude
	//VectorXd Attitude_Roll_interp = tk::Interp_spline(TimePitchRoll,roll, TimeBasis);
	//VectorXd Attitude_Pitch_interp = tk::Interp_spline(TimePitchRoll,pitch, TimeBasis);
    //VectorXd Attitude_Yaw_interp = tk::Interp_spline(TimePitchRoll,heading, TimeBasis);
	//// Spline: angular rate
	//VectorXd AngRate_Roll_interp = tk::Interp_spline(TimePitchRoll,rawW_b_nb.col(0), TimeBasis);
    //VectorXd AngRate_Pitch_interp = tk::Interp_spline(TimePitchRoll,rawW_b_nb.col(1), TimeBasis);
    //VectorXd AngRate_Yaw_interp = tk::Interp_spline(TimePitchRoll,rawW_b_nb.col(2), TimeBasis);

	// Storage
	MatrixXd AttitudeRecep(Attitude_Roll_interp.rows(),3), WbnbRecep(Attitude_Roll_interp.rows(),3);
	AttitudeRecep << Attitude_Roll_interp, Attitude_Pitch_interp, Attitude_Yaw_interp;
	WbnbRecep << AngRate_Roll_interp, AngRate_Pitch_interp, AngRate_Yaw_interp;

	// intermediate data
	MatrixXd CidcoFormatData_int(nbrElt, 7);
	CidcoFormatData_int << Time_long, Ping_long, Index_Beam, SSS_long, 
		TwoTravelTime/2.0, Alpha, Beta;

	// Remove data when appropriate
	if (ind2Remove.size() > 0)
	{
		VectorXd ind2Rem(ind2Remove.size());
		for (int i=0;i<ind2Remove.size();i++) 
		{ 
			ind2Rem(i) = ind2Remove[i]; 
		}

		for(int ii=0; ii<ind2Remove.size(); ii++)
		{
			removeRow( PGeo_long, ind2Rem(ii) );
			removeRow( attitude_long, ind2Rem(ii) );
			removeRow( w_b_nb_long, ind2Rem(ii) );
			removeRow( w_b_nbPT_long, ind2Rem(ii) );
			removeRow( CidcoFormatData_int, ind2Rem(ii) );
			ind2Rem.array() = ind2Rem.array() - 1;
		}
	}


	// ---------------------------------> Data Selection
	VectorXd selection;
	if (ApproachAuto == 1)
	{
		// Automatic approach
		selection = RobusteDataSelection(attitude_long.col(0), attitude_long.col(1));
	}
	else
	{
		// Percentage approach
		selection = SelectPourcent_Valeur(w_b_nb_long, w_b_nbPT_long, attitude_long.col(0), attitude_long.col(1));
	}

	// All data without selection
	MatrixXd CidcoFormatData(CidcoFormatData_int.rows(), 22);
	CidcoFormatData << CidcoFormatData_int, PGeo_long, attitude_long, AttitudeRecep, w_b_nb_long, WbnbRecep;

	// ---------------------------------> Extraction of selected data
	MatrixXd CidcoFormatData_Out(selection.rows(), 22);
	vector<double> TpsSel, phiSel, theSel;
	int kkk=0;
    for (int kk = 0; kk<selection.rows(); kk++)
    {
		//Delete beams with small travel time (spikes)
        if ( CidcoFormatData_int( selection(kk), 4 ) > 0.00001 )
        {
			// Extraction of attitude data
			TpsSel.push_back(CidcoFormatData(selection(kk), 0));
			phiSel.push_back(attitude_long(selection(kk), 0)*r2d);
			theSel.push_back(attitude_long(selection(kk), 1)*r2d);

			// Extraction of all data
			CidcoFormatData_Out.block(kkk, 0, 1, CidcoFormatData_Out.cols()) = CidcoFormatData.block(selection(kk), 0, 1, CidcoFormatData.cols());
            kkk++;
        }
    }
	CidcoFormatData_Out.conservativeResize(kkk, CidcoFormatData.cols());

	// Plot of attitude and selected data
	int Plot = PlotSelectedData(TimePitchRoll, roll, pitch, TpsSel, phiSel, theSel);


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Test: Write data 
	//string const nomFichierOut("Reading_And_Selection.txt");
	//ofstream FileOut(nomFichierOut.c_str());

	//// header
	//FileOut << "Time_long(s)  Ping_long  Index_Beam  SSS_long(m/s)  OneTravelTime(s)  Alpha(rad)  Beta(rad)  "
	//	    << "P_N  P_E  P_D  roll(rad)  pitch(rad)  yaw(rad)  roll_r(rad)  pitch_r(rad)  yaw_r(rad)  " 
	//        << "omega_roll(rad)  omega_pitch(rad)  omega_yaw(rad)  omega_roll_r(rad)  omega_pitch_r(rad)  omega_yaw_r(rad)  " << endl;

	//// synchronized data
	//for (int il =0; il < CidcoFormatData_Out.rows(); il++)
	//{
	//	FileOut << std::fixed << setprecision (12)  << CidcoFormatData_Out(il, 0)   << "\t"  << CidcoFormatData_Out(il, 1) 
	//	<< "\t" << CidcoFormatData_Out(il, 2)  << "\t"  << CidcoFormatData_Out(il, 3)   << "\t"  << CidcoFormatData_Out(il, 4) 
	//	<< "\t" << CidcoFormatData_Out(il, 5)  << "\t"  << CidcoFormatData_Out(il, 6)   << "\t"  << CidcoFormatData_Out(il, 7) 
	//	<< "\t" << CidcoFormatData_Out(il, 8)  << "\t"  << CidcoFormatData_Out(il, 9)   << "\t"  << CidcoFormatData_Out(il, 10) 
	//	<< "\t" << CidcoFormatData_Out(il, 11) << "\t"  << CidcoFormatData_Out(il, 12)  << "\t"  << CidcoFormatData_Out(il, 13) 
	//	<< "\t" << CidcoFormatData_Out(il, 14) << "\t"  << CidcoFormatData_Out(il, 15)  << "\t"  << CidcoFormatData_Out(il, 16) 
	//	<< "\t" << CidcoFormatData_Out(il, 17) << "\t"  << CidcoFormatData_Out(il, 18)  << "\t"  << CidcoFormatData_Out(il, 19) 
	//	<< "\t" << CidcoFormatData_Out(il, 20) << "\t"  << CidcoFormatData_Out(il, 21)  << endl;
	//}
	//FileOut.close();
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	// Output
	return CidcoFormatData_Out;

}// end function CidcoFormatFileRead
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// C_bi_n
MatrixXd MILAC_Estimation::DCM12(double phi, double theta, double psi)
{
	/* This function is used to compute the DirectionCosine Matrix (DCM)
	from frame 1 to frame 2 (NED convention).

	Input data: 
	- phi, theta, psi, attitude angle of 1 wrt 2

	Output data:
	- DCM12_Out
	*/

	// Initialisation
	MatrixXd R1(3,3), R2(3,3), R3(3,3);
	MatrixXd C_bI_LGF_NED(3,3);

	// Rotation matrix Rx, Ry, Rz
	R1 << 1, 0, 0,
		0, cos(phi), -sin(phi),
		0, sin(phi), cos(phi);

	R2 << cos(theta), 0, sin(theta),
		0, 1, 0,
		-sin(theta), 0, cos(theta);

	R3 << cos(psi), -sin(psi), 0,
		sin(psi), cos(psi), 0,
		0, 0, 1;

	// Frame transformation matrix (1 -> 2) - NED convention
	C_bI_LGF_NED = R3*R2*R1;

	// Output
	return C_bI_LGF_NED;

} // end function DCM12
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// derivative DCM wrt phi, theta, psi : dCbin_(phi, theta, psi)
MatrixXd MILAC_Estimation::derDCM_phiThetaPsi(double phi, double theta, double psi)
{
	/* This function is used to compute the derivative of the DirectionCosine Matrix (DCM)
	from frame 1 to frame 2 (NED convention) wrt phi, theta, psi.

	Input data: 
	- phi, theta, psi, attitude angle of 1 wrt 2

	Output data:
	- derDCM_phiThetaPsi_Out
	*/

	// Initialization
	double sR = sin(phi);
	double cR = cos(phi);
	double sP = sin(theta);
	double cP = cos(theta);
	double sH = sin(psi);
	double cH = cos(psi);

	MatrixXd derDCM_phi(3,3), derDCM_theta(3,3), derDCM_psi(3,3);

	// derivative wrt phi - NED convention
	derDCM_phi   <<    0, cR*cH*sP+sR*sH, -sR*cH*sP+cR*sH,
		0, cR*sH*sP-sR*cH, -sR*sH*sP-cR*cH,
		0, cR*cP, -sR*cP;

	// derivative wrt theta - NED convention
	derDCM_theta << -cH*sP, sR*cH*cP, cR*cH*cP,
		-sH*sP, sR*sH*cP, cR*sH*cP,
		-cP, -sR*sP, -cR*sP;

	// derivative wrt psi - NED convention
	derDCM_psi   << -sH*cP, -sR*sH*sP-cR*cH, sR*cH-cR*sH*sP,
		cH*cP, sR*cH*sP-cR*sH, cR*cH*sP+sR*sH,
		0, 0, 0;

	// Output
	MatrixXd derDCM_phiThetaPsi_Out(3, 9);
	derDCM_phiThetaPsi_Out << derDCM_phi, derDCM_theta, derDCM_psi;

	return derDCM_phiThetaPsi_Out;

}// end function derDCM_phiThetaPsi
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Unit vector computation
VectorXd MILAC_Estimation::UnitVectorComputation(double phi, double theta, double psi, 
												 VectorXd const& Bor, double beta, double alpha, 
												 double O1, double O2, double O3, double dt)
{
	/* This function is used to compute the unit vector,
	representing the intersection between the transmitted and the received 
	beams. It is considered as an initial vector for refraction correction.
	We use roll at the reception, pitch and heading at the emission.

	Input data: 
	- attitude,  roll, pitch and heading at transmission time
	- attitude_Recp, roll, pitch and heading at reception time
	- Bor, droll, dpitch, dyaw boresight angles
	- beta, emitted steering angle (tilt angle)
	- alpha, received steering angle (beams angle)
	- dt, MBES/IMU Latency

	Output data:
	- UnitVector_Out, [sinAz, cosAz, Betan] stand for azimuth and vertical angle (depression angle)
	associated to the unit vector
	*/

	// Initialization
	double dphi, dtheta, dpsi;
	MatrixXd C_bI_LGF_NED(3,3), C_bS_bI_NED(3,3);
	VectorXd Ubs(3);
	VectorXd Un(3), Un1(3), Un2(3);
	double normVn;
	double sinAz, cosAz, Betan;
	VectorXd sinAzcosAzBetan(3);

	// Boresight angles
	dphi = Bor(0);
	dtheta = Bor(1);
	dpsi = Bor(2);

	// Launch vector in bS frame
	Ubs << sin(beta), 
		cos(beta)*sin(alpha), 
		cos(beta)*cos(alpha);

	// Frame transformation matrix (bI -> n) - NED convention
	C_bI_LGF_NED = DCM12(phi, theta, psi);

	// Frame transformation matrix (bS -> bI) - NED convention
	C_bS_bI_NED = DCM12(dphi, dtheta, dpsi);

	// Un1 in n frame: Cbin.CbsbI.Ubs
	Un1 = C_bI_LGF_NED*C_bS_bI_NED*Ubs;

	//calcul omega_nbI_bI;
    MatrixXd Omega(3,3);
    Omega <<0, -O3, O2,
             O3, 0, -O1,
            -O2, O1, 0;

    // Un2 = dt .*C_bi_n .*omega_nbI_bI .*C_bs_bi .*U_bs
    Un2 = dt*C_bI_LGF_NED*Omega*C_bS_bI_NED*Ubs;

	// Launch vector in n-frame
	Un = Un1 - Un2;

	// norm of Vn - radial part of Un
	normVn = sqrt(pow(Un(0), 2)  + pow(Un(1), 2));

	// Computation of Az and Betan in radian
	sinAz = Un(0)/normVn;
	cosAz = Un(1)/normVn;

	// Remove nan fom Betan
	if(Un(2) > 1) {Un(2) = 1;}
	Betan = asin(Un(2));

	// Output
	sinAzcosAzBetan << sinAz, cosAz, Betan;
	return sinAzcosAzBetan;

} // end function UnitVectorComputation
// --------------------------------------------------------------------------->>>>>>



// --------------------------------------------------------------------------->>>>>>
// Ray Tracing
VectorXd  MILAC_Estimation::RayTracing(VectorXd const& sinAzCosAzBeta0, MatrixXd const& ProfilCel, double TravelTime)
{
	/* This function is used to correct ray propagation 
	from refraction

	Input data: 
	- sinAzCosAzBeta0,  azimut angle unit vector (intersection transmitted and received beams)
	- ProfilC, sound velocity profiler adapted from draft
	- TravelTime, Acoustic ray one way travel time

	Output data:
	- RayTracing_Out = [rn1, rn2, rn3] , representing soundings coordinates wrt the sounder 
	acoustic center in n-frame
	*/

	// Initialization
	double sinAz = sinAzCosAzBeta0(0), cosAz = sinAzCosAzBeta0(1), Beta0 = sinAzCosAzBeta0(2);
	VectorXd ProfilC = ProfilCel.col(0), ZC = ProfilCel.col(1);
	int N_SSP_values = ProfilC.size();
	VectorXd gn(N_SSP_values-1);

	double Xf = 0.0, Zf = 0.0, AtoS_N = 0.0, AtoS_E = 0.0, AtoS_D = 0.0;
	double Epsi, DT = 0.0, dtt = 0.0, xff = 0.0, zff = 0.0;
	double sinBnm1, sinBn, cosBnm1, cosBn, Rcn, DZ, DR,dtf, dxf, dzf;
	int N = 0;
	VectorXd RayTracing_Out(3);

	//Speed of Sound gradient in each layer
	for (int k=0; k<N_SSP_values-1; k++)
	{
		gn(k) = (ProfilC(k+1)- ProfilC(k))/(ZC(k+1)- ZC(k));
	}

	//constant parameter: Snell's law is applied in the 1st layer
	Epsi = cos(Beta0)/ProfilC(0);

	//Following the gradient celerity values
	while((DT + dtt)<=TravelTime && (N<N_SSP_values-1))
	{
		//update of angles
		sinBnm1 = sqrt(1 - pow(Epsi*ProfilC(N), 2));
		sinBn   = sqrt(1 - pow(Epsi*ProfilC(N+1), 2));

		cosBnm1 = Epsi*ProfilC(N);
		cosBn   = Epsi*ProfilC(N+1);

		if (gn(N)!=0.0)
		{
			// if not null gradient
			//Radius of curvature
			Rcn = 1./(Epsi*gn(N));

			//delta t, delta z and r for the layer N
			dtt = abs( (1./abs(gn(N)))*log( (ProfilC(N+1)/ProfilC(N))*( (1.0 + sinBnm1)/(1.0 + sinBn) ) ) );
			DZ = Rcn*(cosBn - cosBnm1);
			DR = Rcn*(sinBnm1 - sinBn);
		}
		else
		{
			//celerity gradient is zero so constant celerity in this layer
			//delta t, delta z and r for the layer N
			DZ = ZC(N+1) - ZC(N);
			dtt = DZ/(ProfilC(N)*sinBn);
			DR = cosBn*dtt*ProfilC(N);
		}

		//To ensure to work with the N-1 cumulated travel time
		if (DT + dtt <=  TravelTime)
		{
			N = N+1;
			xff = xff + DR;
			zff = zff + DZ;
			DT = DT + dtt;
		}

	}// end loop while

	// Last Layer Propagation
	dtf = TravelTime - DT;
	dxf = ProfilC(N)*dtf*cosBn;
	dzf = ProfilC(N)*dtf*sinBn;

	// Output variable computation
	Xf = xff + dxf;
	Zf = zff + dzf;
	AtoS_N = Xf*sinAz;
	AtoS_E = Xf*cosAz;
	AtoS_D = Zf;

	// Output of function
	RayTracing_Out << AtoS_N, AtoS_E, AtoS_D;
	return RayTracing_Out;

} // end function RayTracing
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Merging data
VectorXd MILAC_Estimation::MergingData(VectorXd const& Pn, double phi, double theta, double psi,
	                                   double dt, double O1, double O2, double O3, 
									   VectorXd const& SLGF, VectorXd const& a_bi)
{
	/* This function based on geolocation equation is used georeference soundings
	by merging data.

	Input data: 
	- Pn, survey reference point position in local navigation frame
	- attitude, roll, pitch and heading at emission
	- SLGF, representing soundings coordinates wrt the sounder 
	acoustic center in n-frame
	- a_bi, lever arms i.e offset from PRP to the sounder acoustic center (transmit array)
	- dt, MBES/IMU latency
	- O1,2,3 : Angular rate at emission

	Output data:
	- MergingData_Out = [Xn, Yn, Zn] Georeferenced soundings in navigation frame
	[Xn, Yn, Zn]^T = Pn + Cbin[Id - Omega*dt][ Cbsbi.rbs + abi ] = Pn + rn + Cbin[Id - Omega*dt].abi
	*/

	// DCM from bi to LGF frame(n)
	MatrixXd C_bI_LGF_NED = DCM12(phi, theta, psi);

	// Cbin[Id - Omega*dt]
	MatrixXd Id3 = MatrixXd::Identity(3,3);
	MatrixXd Omega(3, 3);  Omega << 0, -O3, O2,
								    O3, 0, -O1,
							       -O2, O1, 0;
	MatrixXd dtOmega = Id3.array() - dt*Omega.array();
    MatrixXd C_bI_LGF_dt_NED = C_bI_LGF_NED*dtOmega;

	// Output: Pn + rn + Cbin[Id - Omega*dt].abi
	VectorXd MergingData_Out = Pn + SLGF + C_bI_LGF_NED*a_bi;
	return MergingData_Out;

} // end function MergingData
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// TPU : soundings uncertainties
VectorXd MILAC_Estimation::SoundingsUncertainties(double phi, double theta, double psi, 
						   VectorXd const& SLGF, VectorXd const& Boresight, VectorXd const& a_bi,
						   VectorXd const& SigRawData,
						   double O1, double O2, double O3, double dt)
{
	/* This function is used to compute the soundings uncertainties, by Total Propagated Uncertainties
	approach using the following geolocation equation: Xn = Pn + Cbin[Cbsbi.rbs + abi]

	Input data: 
	- attitude,  attitude data at transmission
	- SLGF, representing soundings coordinates wrt the sounder 
	acoustic center in n-frame
	- Boresight, boresight angle
	- a_bi, lever arms,
	- SigRawData, uncertainties on raw data.

	Output data:
	- SoundingsUncertainties_Out
	*/


	//Initialization
	double dphi = Boresight(0), dtheta = Boresight(1), dpsi = Boresight(2);

	MatrixXd derCbin, derCbin_phi(3,3),  derCbin_theta(3,3), derCbin_psi(3,3);
	MatrixXd derCbsbi, derCbsbi_dphi(3,3),  derCbsbi_dtheta(3,3), derCbsbi_dpsi(3,3);
	VectorXd derRbs_rho(3), derRbs_alpha(3), derRbs_beta(3);

	VectorXd derX_Pn1(3), derX_Pn2(3), derX_Pn3(3);
	VectorXd derX_phi, derX_theta, derX_psi;
	VectorXd  derX_dphi, derX_dtheta, derX_dpsi;
	VectorXd derX_dRho, derX_dBeta, derX_dAlpha;
	VectorXd derX_dax, derX_day, derX_daz;

	double Rho, Beta, Alpha;
	VectorXd r_bS, r_bS1, r_bS2;
	MatrixXd C_bI_LGF, C_LGF_bI, C_bS_bI, C_bI_bS;
	MatrixXd Jac(3, 15);
	VectorXd SoundingsUncertainties_Out(3);

	//// Partial Derivative of X_LGF as function of the Position
	derX_Pn1 << 1, 0, 0;
	derX_Pn2 << 0, 1, 0;
	derX_Pn3 << 0, 0, 1;


	//// Partial Derivative of Xn wrt attitude
	// Cbin and Cnbi
	C_bI_LGF = DCM12(phi,theta,psi);
	C_LGF_bI = C_bI_LGF.transpose();

	// Cbsbi and Cbibs
	C_bS_bI = DCM12(dphi,dtheta,dpsi);
	C_bI_bS = C_bS_bI.transpose();

	// Skew symmetric matrix and identity matrix
	MatrixXd Id3 = MatrixXd::Identity(3,3);
	MatrixXd Omega(3, 3);  Omega << 0, -O3, O2,
								    O3, 0, -O1,
							       -O2, O1, 0;
	// r_bS Calculation
	r_bS1 = C_bI_bS*C_LGF_bI*SLGF;
	r_bS2 = dt*C_bI_bS*Omega.transpose()*C_LGF_bI*SLGF;
	r_bS = r_bS1 - r_bS2;


	// derivative Cbin wrt attitude
	derCbin = derDCM_phiThetaPsi(phi,theta,psi);
	derCbin_phi = derCbin.middleCols<3>(0) ;
	derCbin_theta = derCbin.middleCols<3>(3);
	derCbin_psi = derCbin.middleCols<3>(6);

	// derivative of Xn wrt attitude
	derX_phi = derCbin_phi*(C_bS_bI*r_bS + a_bi) - dt*derCbin_phi*Omega*(C_bS_bI*r_bS + a_bi);
	derX_theta = derCbin_theta*(C_bS_bI*r_bS + a_bi) - dt*derCbin_theta*Omega*(C_bS_bI*r_bS + a_bi);
	derX_psi = derCbin_psi*(C_bS_bI*r_bS + a_bi) - dt*derCbin_psi*Omega*(C_bS_bI*r_bS + a_bi);


	//// Partial Derivative of X_LGF wrt boresight angles;
	// derivative of Cbsbi wrt boresight angles
	derCbsbi = derDCM_phiThetaPsi(dphi,dtheta,dpsi);
	derCbsbi_dphi = derCbsbi.middleCols<3>(0) ;
	derCbsbi_dtheta = derCbsbi.middleCols<3>(3);
	derCbsbi_dpsi = derCbsbi.middleCols<3>(6);

	// derivative of Xn wrt boresight angles
	derX_dphi = C_bI_LGF*derCbsbi_dphi*r_bS - dt*C_bI_LGF*Omega*derCbsbi_dphi*r_bS;
	derX_dtheta = C_bI_LGF*derCbsbi_dtheta*r_bS - dt*C_bI_LGF*Omega*derCbsbi_dtheta*r_bS;
	derX_dpsi = C_bI_LGF*derCbsbi_dpsi*r_bS - dt*C_bI_LGF*Omega*derCbsbi_dpsi*r_bS;


	//// Partial Derivative of X_LGF as function of the r_bS
	// r_bS Parametrization
	Rho = r_bS.norm();
	Beta = asin(r_bS(0)/Rho);
	Alpha = asin(r_bS(1)/(Rho*cos(Beta)));
	double cA = cos(Alpha), sA = sin(Alpha), cB = cos(Beta), sB = sin(Beta);

	// derivative of rbs wrt rho, beta and alpha
	derRbs_rho << sB, cB*sA, cB*cA;
	derRbs_beta << Rho*cB, -Rho*sA*sB, -Rho*cA*sB;
	derRbs_alpha << 0, Rho*cA*cB, -Rho*sA*cB;

	// derivative of Xn wrt to rbs
	derX_dRho = (C_bI_LGF*C_bS_bI - dt*C_bI_LGF*Omega*C_bS_bI)*derRbs_rho;
	derX_dBeta = (C_bI_LGF*C_bS_bI - dt*C_bI_LGF*Omega*C_bS_bI)*derRbs_beta;
	derX_dAlpha = (C_bI_LGF*C_bS_bI - dt*C_bI_LGF*Omega*C_bS_bI)*derRbs_alpha;


	//// Partial Derivative of X_LGF as function of the Lever Arms
	derX_dax = C_bI_LGF.col(0) - dt*(C_bI_LGF*Omega).col(0);
	derX_day = C_bI_LGF.col(1) - dt*(C_bI_LGF*Omega).col(1);
	derX_daz = C_bI_LGF.col(2) - dt*(C_bI_LGF*Omega).col(2);


	//// Uncertainties computation : TPU
	// Jacobian matrix
	Jac << derX_Pn1, derX_Pn2, derX_Pn3, derX_phi, derX_theta, derX_psi, derX_dphi, derX_dtheta,
		derX_dpsi,  derX_dRho, derX_dBeta, derX_dAlpha,  derX_dax, derX_day, derX_daz;

	// Input raw data uncertainties
	MatrixXd stdRawData = SigRawData.asDiagonal();
	MatrixXd SIG = stdRawData.array().pow(2);

	// TPU
	MatrixXd VarianceXn;
	VarianceXn = Jac*SIG*Jac.transpose();

	// Output 
	SoundingsUncertainties_Out = (VarianceXn.diagonal().transpose()).array().sqrt();
	return SoundingsUncertainties_Out;

}// end function SoundingsUncertainties
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Matrix needed for LS adjustment
MatrixXd MILAC_Estimation::Matrix_ALS_bLS_WLS(string folderData, MatrixXd const& RawData, 
	                                         double A, double B, double C, double D, double E, double F, double dt, double so2)
{
	/* This function is used to construct matrix needed for least squares estimation (1st option)

	Input data: 
	- FolderData, data folder
	- RawData, raw data:
	[Time_long, Ping_long, Index_Beam, SSS_long, TwoTravelTime/2.0, Alpha, 
	Beta, PGeo_long, attitude_long, AttitudeRecep, w_b_nb_long, w_b_nbRecep_long]

	Output data:
	- Matrix_ALS_bLS_WLS_Out, ALS, bLS and WLS matrices
	*/

	// Reading metadata file
	string fileMetadata(folderData + "/MetaData.txt");
	Metadata MetaData_Out = readTextFileMetadata(fileMetadata);

	// Reading SVP file
	double draft = MetaData_Out.Draft;
	string fileSVP(folderData + "/SVP.txt");
	MatrixXd ProfilC = readTextFileSVP(fileSVP, draft);

	// Lever arms and boresight angles
	VectorXd a_bi = MetaData_Out.CordTx - MetaData_Out.CordGNSS;
	VectorXd Boresight = MetaData_Out.PatchBoresight;

	// Uncertainties on raw data
	VectorXd SigRawData = RawDataUncertainties(MetaData_Out);

	// Initialization
	int N_soundings = RawData.rows();
	VectorXd Xn(N_soundings), Yn(N_soundings), Zn(N_soundings);
	VectorXd sigXn(N_soundings), sigYn(N_soundings), sigZn(N_soundings);
	VectorXd derXn_ddt(N_soundings), derYn_ddt(N_soundings), derZn_ddt(N_soundings);
	double dphi = Boresight(0), dtheta = Boresight(1), dpsi = Boresight(2);

	// Loop on each sounding : Georeferencing and Xn derivative wrt latency
	for(int j=0; j<N_soundings; j++)
	{
		//------> Georeferencing:
		// Launh vector computation
		double phi = RawData(j, 13), the = RawData(j, 11), psi = RawData(j, 12); // Roll at reception and pitch anh heading at emission
		double O1 = RawData(j, 19), O2 = RawData(j, 17), O3 = RawData(j, 18);    // Roll at reception and pitch anh heading at emission
		double beta = RawData(j, 6), alpha = RawData(j, 5);
		VectorXd sinAzCosAzBetan =  UnitVectorComputation(phi, the, psi, 
														  Boresight,  beta,  alpha, 
														  O1, O2, O3, dt);
		// Ray Tracing
		double TravelTime = RawData(j, 4);
		VectorXd SLGF = RayTracing(sinAzCosAzBetan, ProfilC,  TravelTime);

		// Data merging: Xn
		phi = RawData(j, 10); O1 =  RawData(j, 16);
		VectorXd Pn(3); Pn << RawData(j, 7), RawData(j, 8), RawData(j, 9);
		VectorXd XYZn = MergingData(Pn, phi, the, psi, dt, O1, O2, O3, SLGF, a_bi);
		Xn(j) = XYZn(0);
		Yn(j) = XYZn(1);
		Zn(j) = XYZn(2);

		//------> Uncertainties on Xn: SigXn
		VectorXd sigXYZn = SoundingsUncertainties(phi, the, psi, SLGF, Boresight, a_bi, SigRawData, O1, O2, O3, dt);
		sigXn(j) = sigXYZn(0);
		sigYn(j) = sigXYZn(1);
		sigZn(j) = sigXYZn(2);

		//------> Derivative of Xn wrt dt:
		// C_LGF_bI, C_bI_bS
		MatrixXd C_bI_LGF = DCM12(phi, the, psi);
		MatrixXd C_LGF_bI = C_bI_LGF.transpose();
		MatrixXd C_bS_bI = DCM12(dphi,dtheta,dpsi);
		MatrixXd C_bI_bS = C_bS_bI.transpose();

		// Skew symmetric matrix
		MatrixXd Omega(3, 3);  Omega << 0, -O3, O2,
										O3, 0, -O1,
									   -O2, O1, 0;
		// r_bS
		VectorXd r_bS1 = C_bI_bS*C_LGF_bI*SLGF;
		VectorXd r_bS2 = dt*C_bI_bS*Omega.transpose()*C_LGF_bI*SLGF;
		VectorXd r_bS = r_bS1 - r_bS2;

		//derXn_dt
		VectorXd derXn_dt = -C_bI_LGF*Omega*(C_bS_bI*r_bS + a_bi);
		derXn_ddt(j) = derXn_dt(0);
		derYn_ddt(j) = derXn_dt(1);
		derZn_ddt(j) = derXn_dt(2);
	}

	// NB:
	// Z - AX^2 - BY^2 - CXY - DX -EY -F = f(chi)
    // -f(chi0) = bLS = AX^2 + BY^2 + CXY + DX + EY + F - Z 

	//------> derf_ddt:
	VectorXd derf_ddt = derZn_ddt.array() - 2*A*Xn.array()*derXn_ddt.array() - 2*B*Yn.array()*derYn_ddt.array() -
						C*Xn.array()*derYn_ddt.array() - C*Yn.array()*derXn_ddt.array() -
						D*derXn_ddt.array() - E*derYn_ddt.array();

	//------> ALS:
	int N_unknows = 7;
	MatrixXd ALS = MatrixXd::Zero(N_soundings, N_unknows);
	ALS.col(0) = derf_ddt;                                    // derf_ddt
	ALS.col(1) = - Xn.array()*Xn.array();                     // derf_dA
	ALS.col(2) = - Yn.array()*Yn.array();                     // derf_dB
	ALS.col(3) = - Xn.array()*Yn.array();                     // derf_dC
	ALS.col(4) = - Xn;                                        // derf_dD
	ALS.col(5) = - Yn;                                        // derf_dE
	ALS.col(6) = -1*MatrixXd::Ones(N_soundings,1).array();    // derf_dF


	//------> bLS:
	VectorXd bLS = A*Xn.array()*Xn.array() + B*Yn.array()*Yn.array() + 
		  C*Xn.array()*Yn.array() + D*Xn.array() + E*Yn.array() + F - Zn.array();

	//------> diagWLS:
	VectorXd Sig2_b = (2*A*Xn.array()).pow(2)*sigXn.array().pow(2) + (2*B*Yn.array()).pow(2)*sigYn.array().pow(2) +
					  (C*Xn.array()).pow(2)*sigYn.array().pow(2) + (C*Yn.array()).pow(2)*sigXn.array().pow(2) +
					  pow(D, 2)*sigXn.array().pow(2) + pow(E, 2)*sigYn.array().pow(2) + sigZn.array().pow(2);
	VectorXd diagWLS = (1/so2)*Sig2_b.array().inverse();


	//------>  Output
	MatrixXd Matrix_ALS_bLS_WLS_Out(N_soundings, N_unknows+2);
	Matrix_ALS_bLS_WLS_Out << ALS, bLS, diagWLS;
	return Matrix_ALS_bLS_WLS_Out;

}// end function Matrix_ALS_bLS_WLS
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// deltaX Least Square adjustment
VectorXd MILAC_Estimation::LS_Adjustement(MatrixXd const& AbW_LS)
{
	/* This function is used to compute Least square solution as follows:
	deltaX = inv(A^t.W.A)*A^t.W.b

	Input data:
	- AbW_LS, ALS, bLS and WLS matrix

	Output data:
	- LS_Adjustement_Out, Least square solution
	*/

	// Data extraction 
	int N_unknows = (AbW_LS.cols() - 2);
	MatrixXd ALS = AbW_LS.block(0, 0, AbW_LS.rows(), N_unknows);
	VectorXd bLS = AbW_LS.block(0, AbW_LS.cols()-2, AbW_LS.rows(), 1);
	VectorXd diagWeightMat = AbW_LS.block(0, AbW_LS.cols()-1, AbW_LS.rows(), 1);

	// MatWeight = diagWeightMat*ones(1, N_unknows);
	MatrixXd MatWeight = diagWeightMat*(MatrixXd::Ones(1, N_unknows));  

	// AALS = ( (ALS.*MatWeight)' )*ALS;
	MatrixXd AALS_int = (ALS.array()*MatWeight.array()).transpose();                     
	MatrixXd AALS = AALS_int*ALS;                                       

	// bbLS = (ALS')*(diagWeightMat.*bLS);
	MatrixXd bbLS_int = ALS.array().transpose();
	VectorXd bbLS_int2 = diagWeightMat.array()*bLS.array();
	VectorXd bbLS = bbLS_int*bbLS_int2; 

	// Output: deltaEpsi = AALS\bbLS;
	VectorXd LS_Adjustement_Out = AALS.colPivHouseholderQr().solve(bbLS);
	return LS_Adjustement_Out;

}// end function LS_Adjustment
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Least squares adjustment statistic
VectorXd MILAC_Estimation::LS_Adjust_Statistic(MatrixXd const& AbW_LS, VectorXd DeltaEpsi, double probability)
{
	/* This function is used to compute least squares adjustment statistic

	Input data:
	- AbW_LS, ALS, bLS and WLS matrix
	- DeltaEpsi, Least squares solution
	- Probability, statistic probability 

	NB:
	- Calculate confidence intervals for a value.
	- For example if we set the confidence limit to
	0.95, we know that if we repeat the sampling
	100 times, then we expect that the true value
	will be between out limits on 95 occations.

	- Note: this is not the same as saying a 95%
	confidence interval means that there is a 95%
	probability that the interval contains the true value.

	- The interval computed from a given sample either
	contains the true value or it does not.

	Output data:
	- LS_Adjust_Statistic_Out:
	* so2, Unit variance factor
	* MNR, Maximal Normalized Residual, 
	* Zint, �Internal reliability 
	* stdRoll, stdPitch, stdYaw, respectively boresight angle estimation precision 
	* chi2Test, Test chi 2 result, 1 if success and 0 if not.
	*/

	// Initialization
	double so2, MNR, Zint, chi2Test;
	double stddt;
	MatrixXd CovXest_a;
	VectorXd diagCovObs_a; 
	VectorXd LS_Adjust_Statistic_Out(5);

	// ALS, bLS and diagWeightMat extraction
	int N_unknows = (AbW_LS.cols() - 2);
	MatrixXd ALS = AbW_LS.block(0, 0, AbW_LS.rows(), N_unknows);
	VectorXd bLS = AbW_LS.block(0, AbW_LS.cols()-2, AbW_LS.rows(), 1);
	VectorXd diagWeightMat = AbW_LS.block(0, AbW_LS.cols()-1, AbW_LS.rows(), 1);

	//// Residual computation
	VectorXd residual = bLS - ALS*DeltaEpsi;

	//// degree of freedom
	int df = ALS.rows() - ALS.cols();

	//// Unbiased estimator of the variance factor sigma^2
	MatrixXd residualT = residual.array().transpose();
	MatrixXd diagResidual = diagWeightMat.array()*residual.array();
	VectorXd so2Vect = (1.0/df)*residualT*diagResidual;
	so2 = so2Vect(0);

	// A priori variances associated to estimated parameters: AALS = ( (ALS.*MatWeight)' )*ALS : [invCovXest = AALS];
	MatrixXd MatWeight = diagWeightMat*(MatrixXd::Ones(1, N_unknows));  
	MatrixXd AALS_int = (ALS.array()*MatWeight.array()).transpose();                     
	MatrixXd AALS = AALS_int*ALS;                  
	MatrixXd CovXest = AALS.inverse();

	// Variances associated to observations
	VectorXd diagCovObs = diagWeightMat.array().inverse(); 

	// for chi2 test:
	double alpha = (1 - probability);
	chi_squared ChiDist(df);
	double g2 = quantile(complement(ChiDist, alpha/2));
	double g1 = quantile(ChiDist, alpha/2);  

	// Gamma for normalized residual Initialization 
	double Gamma; 

	// If successfull chi2 test  
	if (df*so2/g2 <= 1 && 1 <= df*so2/g1)
	{
		chi2Test = 1.0;

		// Gamma for normalized residual (student's distribution)
		students_t StudentDist(df);
		Gamma = quantile(complement(StudentDist, alpha / 2));

		// Covariance matrices a posteriori computation
		CovXest_a = so2*CovXest;
		diagCovObs_a = so2*diagCovObs;
	}

	// If chi2 test failed
	else
	{
		chi2Test = 0.0;

		// Gamma for normalized residual (normal distribution)
		int MeanNorm = 0;
		int StdNorm = 1;
		normal s(MeanNorm, StdNorm);
		Gamma = quantile(complement(s,  alpha/2));

		// Covariance matrices a posteriori computation
		CovXest_a = CovXest;
		diagCovObs_a = diagCovObs; 
	}

	// Residual covariance matrix
	// NB : J2 = inv(AALS)*ALS' = CovXest*ALS'
	MatrixXd J2 = CovXest*(ALS.transpose()); 

	// NB: diag(ALS*CovXest_a*ALS') = wr
	VectorXd diagACovA = (ALS*J2).diagonal();

	// CovResidual = CovObs_a - ALS*CovXest*ALS'
	VectorXd diagCovResidual =  diagCovObs_a - diagACovA;

	// Maximal Normalized residual computation: MNR = max( residual ./ sqrt(diag(CovResidual)) );
	VectorXd SqrtDiagCovResidual = diagCovResidual.array().sqrt();
	VectorXd MNRVect = residual.array()/SqrtDiagCovResidual.array();
	MNR = 	MNRVect.array().abs().maxCoeff();




	// Normalized residual wirting //////////////////////////////////////////////////
	ofstream FileOut;
	string const nomFichierOut("MNR.txt");
	FileOut.open(nomFichierOut.c_str());
	FileOut << 0 << "\t" << Gamma << endl;
	for (int i = 0; i<MNRVect.rows(); i++)
	{
		FileOut << i+1 << "\t" << MNRVect(i) << endl;
	}


	// Internal reliability computation : Z, matrix (One column))
	// Zint = sqrt(diag(CovResidual)./diagCovObs_a);
	VectorXd ZintVect = (diagCovResidual.array()/diagCovObs_a.array()).sqrt();
	Zint = ZintVect.minCoeff()*100;

	// standard deviation associated to roll, pitch and yaw estimation
	stddt =  sqrt(CovXest_a(0, 0));         

	// Output
	LS_Adjust_Statistic_Out << stddt, so2, Zint, MNR, chi2Test;
	return LS_Adjust_Statistic_Out;

} // end function LS_adjustment statistic
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// Cidco format file reading
VectorXd MILAC_Estimation::MILAC_Results(double probability, double dt2Estimate, int ApproachAuto, 
										 double & A, double & B, double & C, double & D, double & E, double & F, double & dt,
										 string & NameProject, string & folderData, string & dirString, vector<string> & fileNamesLines,
										 VectorXd & MeanPos, MatrixXd & RawData, int EstCounter)
{
	// timer
	DWORD temps_debut, temps_execution;

	// Compile this part only for the first serie of adjustment
	if (EstCounter==1)
	{
		//------------------------------------------>> Extraction of data
		// Create an output folder
		cout << "Please type the project name (Please each time AVOID SPACE IN NAMES): " << endl;
		cin >> NameProject; cout << endl;

		// parameters
		cout << "Please enter the data folder link: " << endl;
		cin >> folderData; cout << endl;

		// Provide current directory
		string curDir =  current_working_directory();

		// Create folder project
		dirString = curDir + "\\" + NameProject;
		const char* dirChar = (char*)dirString.c_str(); 	    // Name of created directory in char
		mkdir(dirChar); 	                                    // Create directory in the current one

		// Lines name 
		cout << "Please enter the name of line: "<< endl;
		string fileName_i; cin >> fileName_i;
		fileNamesLines.push_back(folderData + "\\" + fileName_i);
		cout << endl;

		// Mean Pos
		MeanPos = Mean_LatLonHeight(fileNamesLines);
		// cout << "MeanPos: " << MeanPos << endl;

		//-----> Raw data Reading
		temps_debut = GetTickCount();
		cout << "Reading and selection of data..." << endl;
		RawData = CidcoFormatFileRead(fileNamesLines[0], MeanPos, dt2Estimate, ApproachAuto);

		// end timer reading of data
		temps_execution = GetTickCount() - temps_debut;
		cout << "Reading and selection of data took "<< temps_execution/1000. <<" second(s)."<< endl << endl;
	}


	//------------------------------------------>> Least squares process
	// Epsilon for stopping the Least squares and counter max
	double eps = 0.001;
	int IterCounterMax = 50;

	// Initialization: loop counter, Norm initialization
	cout << "Least Squares..." << endl;
	temps_debut = GetTickCount();
	int IterCounter = 0;
	double normDelta = 1;
	double so2 = 1;
	int chi2Test = 0;

	// Initialization LS matrices and LS solution
	MatrixXd AbW_LS;
	VectorXd Delta;
	VectorXd LSStat;

	// loop on chi2 test
	while(chi2Test ==0)
	{
		cout << "New set of terations:---->" << endl;

		// Loop: Iterative least squares
		// Stop condition : the LS adjustment norm less than "eps" and the counter reaches "IterCounterMax" (if divergence)
		while (normDelta >= eps && IterCounter < IterCounterMax)
		{
			//-----> Construction of LS matrices
			AbW_LS = Matrix_ALS_bLS_WLS(folderData, RawData, A, B, C, D, E, F, dt, so2);

			//-----> Least square solution
			Delta  =  LS_Adjustement(AbW_LS);

			//-----> Update Latency and surface parameters
			dt = dt + Delta(0);
			A = A + Delta(1); B = B + Delta(2);
			C = C + Delta(3); D = D + Delta(4);
			E = E + Delta(5); F = F + Delta(6);

			// update norm and counter
			normDelta = Delta.norm();
			IterCounter ++;

			//-----> iteration counter
			cout << "Iteration: " << IterCounter<< ".  Latency (s): " << dt << endl;
		}

		//-----> Statistical least square
		LSStat = LS_Adjust_Statistic(AbW_LS, Delta, probability);

		// Update for the new set of iterations
		so2 = LSStat(1);
		chi2Test = LSStat(4);
		IterCounter = 0;
		normDelta = 1;

	}// end first while on chi2Test
	cout << endl;

	//-----> end timer Least Squares
	temps_execution = GetTickCount() - temps_debut;
	cout << "Least Squares process "<< temps_execution/1000. <<" second(s)."<< endl << endl;

	//-----> Save results
	Writing_MILAC_LatEstimation(dirString, dt, LSStat,  IterCounter, AbW_LS.rows());

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Test: Write data 
	//string const nomFichierOut("ALS_bLS_WLS.txt");
	//ofstream FileOut(nomFichierOut.c_str());

	//// header
	//FileOut << "derf_ddt  derf_A  derf_B  derf_C  derf_D  derf_E  derf_F  bLS  diagWLS" << endl;

	//// synchronized data
	//for (int il =0; il < AbW_LS.rows(); il++)
	//{
	//	FileOut << std::fixed << setprecision (12)  
	//	<< AbW_LS(il, 0)   << "\t"  << AbW_LS(il, 1) 
	//	<< "\t" << AbW_LS(il, 2)  << "\t"  << AbW_LS(il, 3)   << "\t"  << AbW_LS(il, 4) 
	//	<< "\t" << AbW_LS(il, 5)  << "\t"  << AbW_LS(il, 6)   << "\t"  << AbW_LS(il, 7) 
	//	<< "\t" << AbW_LS(il, 8)  << endl;
	//}
	//FileOut.close();

	//cout << "A: " << A << " B: " << B << " C: " << C << " D: " << D << " E: " << E << " F: " << F << endl;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//-----> Output
	VectorXd MIBAC_results_Out(6);
	MIBAC_results_Out << dt, LSStat;
	return MIBAC_results_Out;

}// end function MILAC_Results
// --------------------------------------------------------------------------->>>>>>


// --------------------------------------------------------------------------->>>>>>
// writing MILAC results in files
void MILAC_Estimation::Writing_MILAC_LatEstimation(string dirString, double dt, VectorXd LSStat, int IterCounter, int nbrPt)
{
	/* This function is used to write latency estimation results

	Input data:
	- dt, estimated latency
	- LSStat, Containting results statistics
	- IterCounter, number of iteration for converging
	*/
	
	// Name of file out
	time_t rawtime;  time (&rawtime); string TimeStr = ctime (&rawtime);
	replace(TimeStr.begin(), TimeStr.end(), ' ', '_'); replace(TimeStr.begin(), TimeStr.end(), '\n', '_'); 
	replace(TimeStr.begin(), TimeStr.end(), ':', '-'); 
	string fileName = TimeStr +  "MILAC_Results";

	// const variable
	const double r2d = 180/PI;
	string FileName = dirString + "\\" + fileName + ".txt";

	// writing
	ofstream FileOut(FileName.c_str());
	FileOut << "MILAC RESULTS WITH ASSOCIATED STANDARD DEVIATION ----->>>:"      << "\n"  
		<< "===================================================================" << "\n" << "\n"

		<< "Number of iterations :" << "\t" << IterCounter                       << "\n"
		<< "Number of used points:" << "\t" << nbrPt                             << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "IMU/Sounder Latency Estimation in second:"  << "\t" << dt			 << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "STD latency Estimation in second:" << "\t" << LSStat(0)              << "\n"
		<< "===================================================================" << "\n" << "\n"

		<< "Unit variance factor:"					<< "\t" << LSStat(1)              << "\n"
		<< "Internal reliability:"					<< "\t" << LSStat(2)				 << "\n"
		<< "Maximal normalized residual:"			<< "\t" << LSStat(3)				 << "\n"
		<< "Chi2 test (1 is True and 0 is False):"	<< "\t" << LSStat(4)    << "\n"
		<< "===================================================================" << endl;

}// end function writing MILAC results
// --------------------------------------------------------------------------->>>>>>